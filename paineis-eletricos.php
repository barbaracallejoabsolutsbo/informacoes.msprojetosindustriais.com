<?php
    $title       = "Painéis Elétricos";
    $description = "Quando precisar de painéis elétricos não procure em outro lugar sem antes consultar as incríveis opções que você só encontra na MS Projetos Industriais. ";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Se você procura pela maior variedade de painéis elétricos de diversos modelos com preço justo e alta qualidade encontrou o lugar certo. A MS Projetos Industriais é uma empresa que projeta, fabrica, comercializa e instala painéis elétricos em todo o território nacional. São mais de 15 anos atuando nos ramos de engenharia civil e automação industrial. Os painéis elétricos são importantes equipamentos dos sistemas elétricos tendo em vista que são responsáveis por receber a eletricidade, converter de forma segura e redistribuir para pequenos circuitos individuais ou pontos de alimentação. A instalação desse tipo de equipamento deve ser realizada por um profissional qualificado com registro ativo no órgão regulamentador (CREA). A missão da nossa empresa é criar soluções em engenharia elétrica que garantam excelência, apoiadas na competência e no aprimoramento tecnológico buscando sempre agir de forma ética e sustentável. Quando precisar de painéis elétricos não procure em outro lugar sem antes consultar as incríveis opções que você só encontra na MS Projetos Industriais. Estando há muito tempo no mercado, somos uma das empresas mais recomendadas do segmento e já realizamos diversos atendimentos de muito destaque propondo soluções incríveis e assertivas para solucionar os problemas elétricos de nossos clientes. </p>
<h2>Os melhores painéis elétricos do mercado estão aqui</h2>
<p>Para projetar e produzir os painéis elétricos contamos com uma equipe técnica altamente qualificada que passou por instruções nas melhores instituições de ensino do país dentro de nosso segmento. Trabalhamos com a visão de elevar nossa importância nos próximos anos, nos tornando destaque dentre as maiores fornecedoras de serviços de engenharia e montagens industriais no cenário nacional. Atendemos grandes nomes da indústria que podem ser consultados através da aba “portfolio” disponível em nosso site. Nossa empresa atua tanto prestando serviço para o setor privado quanto para o setor público com qualidade, ética, respeito e compromisso.</p>
<h3>Saiba mais sobre os painéis elétricos da MS Projetos Industriais</h3>
<p>Para saber mais sobre os painéis elétricos ou quaisquer outros produtos, ou serviços disponíveis em nosso catálogo entre em contato e seja atendido por um especialista para te auxiliar da melhor maneira possível. É importante que a manutenção desse equipamento seja feita periodicamente para prevenir falhas e acidentes que podem ir de pequenos choques até grandes incêndios em caso de falha grave por falta de manutenção do equipamento. Tudo que você precisa sobre engenharia elétrica e automação industrial em um só lugar.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>