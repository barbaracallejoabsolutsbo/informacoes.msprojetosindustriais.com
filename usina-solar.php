<?php
    $title       = "Usina Solar";
    $description = "Saiba como funciona uma usina solar e suas vantagens. A MS Projetos Industriais disponibiliza serviços no segmento de geração fotovoltaica.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>A <strong>usina solar</strong> é um local central de grande geração de energia que utiliza milhares de placas fotovoltaicas para geração de eletricidade. O efeito fotovoltaico é o que garante a transformação da energia solar em energia elétrica.</p>
<p>Formada por diversas placas de energia solar, a <strong>usina solar </strong>é atualmente uma das soluções sustentáveis que vem ganhando destaque no Brasil e no mundo.</p>
<h2><strong>Usina solar</strong> e suas vantagens</h2>
<p>A placa de energia solar é um equipamento capaz de capturar os raios solares e transformá-los em energia elétrica. O efeito fotovoltaico é o que garante a transformação dessa energia. A instalação de um sistema fotovoltaico <strong>em usina solar</strong> pode ser feita por uma empresa especializada.</p>
<p>Como geralmente a <strong>usina solar</strong> é instalada em áreas distantes e isoladas, as instalações interligadas aos quadros para <strong>usina solar</strong> enviam a energia aos centros urbanos através de linhas de transmissão.</p>
<p>Com sede em Contagem, Minas Gerais, a MS Projetos Industriais atua em todo o território nacional. Nosso segmento de geração fotovoltaica possui equipe especializada em comercialização e implantação de energia, construção, manutenção, e outros serviços relacionados à <strong>usina solar</strong>, como fabricação e montagem de quadros e placas solares.</p>
<p>No Brasil, as usinas solares estão em amplo crescimento, e atualmente Minas Gerais possui uma dessas usinas em seu território. As vantagens são as seguintes:</p>
<ul>
<li>         Possibilidade de instalação em diversos lugares;</li>
<li>         Não emitem gases poluentes;</li>
<li>         Apresentam baixo custo de manutenção;</li>
<li>         Ocupam pouco espaço;</li>
<li>         Silenciosa;</li>
<li>         Renovável, entre outras.</li>
</ul>
<p>O custo de manutenção de placas solares é o mais barato do mercado de energia. Para que as placas sejam conservadas, basta a lavagem a cada seis meses. Entre essas vantagens, a vida útil dos componentes é um benefício, já que uma placa fotovoltaica pode durar até 40 anos.</p>
<h3>Conheça a MS Projetos Industriais e a MS Solar</h3>
<p>Em relação a MS Solar, executamos o melhor projeto viável em implantação, geração, rentabilidade e pós venda. Nossos serviços incluem: prospecção de terras, estudo de viabilidade técnica financeira, dimensionamento do sistema solar, fornecimento de solução completa, acompanhamento pós-venda, análise de dados após implantação, comercialização de energia, contrato e manutenção de energia solar, e muito mais.</p>
<p>Entre em contato com nossa equipe e conheça todas as facilidades disponíveis nesse segmento. Trabalhamos para garantir soluções e serviços à sua empresa e indústria, levando segurança e responsabilidade. Serviços em <strong>usina solar</strong> se tornam ainda mais fáceis com a MS.</p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>