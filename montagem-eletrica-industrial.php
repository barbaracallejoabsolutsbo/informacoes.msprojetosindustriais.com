<?php
    $title       = "Montagem Elétrica Industrial";
    $description = "Durante a montagem elétrica industrial, disponibilizamos um profissional com as ferramentas certas para solucionar seus problemas de montagem.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Se você precisa de um serviço de montagem elétrica industrial e procura uma empresa de alta qualidade e credibilidade para solicitar esse serviço encontrou o lugar ideal. A MS Projetos Industriais está há mais de 15 anos no mercado atuando com engenharia elétrica e automação industrial. Prestamos a montagem elétrica industrial, e diversos outros serviços como manutenção de quadros e painéis elétricos, montagem de quadros e painéis elétricos, inspeção e laudo com parecer técnico, entre outros. Para realizar a montagem elétrica industrial, disponibilizamos profissionais altamente capacitados que foram instruídos nas melhores instituições de ensino dentro de nosso segmento. A missão da nossa empresa é criar soluções em engenharia elétrica que garantam excelência, apoiadas na competência e no aprimoramento tecnológico buscando sempre agir de forma ética e sustentável. Durante a montagem elétrica industrial, disponibilizamos um profissional com as ferramentas certas para solucionar seus problemas de montagem. Solicite esse e outros serviços com a MS Projetos Industriais para ser atendido por uma das referências nacionais dentro do segmento. Atendemos grandes nomes da indústria que podem ser consultados através da aba “portfolio” disponível em nosso site.</p>
<h2>Tudo que você precisa sobre montagem elétrica industrial em um só lugar</h2>
<p>A montagem elétrica industrial é muito importante de ser realizada corretamente para não comprometer a segurança do ambiente. Diversos acidentes podem acontecer envolvendo rede elétrica como curto circuito, incêndios, sobrecarga entre outros. Com a montagem correta do equipamento e manutenções constantes no mesmo você se assegura de que a probabilidade de um evento desses ocorrer seja muito baixa. Além disso, você garante a integridade física dos profissionais que atuam dentro do seu ambiente. Atuamos seguindo todas as normas técnicas e ambientais compreendendo que uma de nossas principais funções é atuar de forma sustentável preservando o ambiente para as futuras gerações. </p>
<h3>Saiba mais sobre a montagem elétrica industrial da MS Projetos Industriais</h3>
<p>Para saber mais sobre nosso serviço de montagem elétrica industrial ou quaisquer outros disponibilizados em nosso catálogo entre em contato e seja atendido por um especialista para te auxiliar da melhor maneira possível. Não coloque seus equipamentos e ambiente de trabalho em risco e conte com os serviços da MS Projetos Industriais para realizar as montagens elétricas que você precisa. São quase duas décadas atuando com qualidade, transparência, respeito, sustentabilidade e trabalho em equipe.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>