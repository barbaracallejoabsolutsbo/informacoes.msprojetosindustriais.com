<?php
    $title       = "Montagem de Quadro de Distribuição";
    $description = "Aproveite essa grande oportunidade de solicitar a montagem de quadro de distribuição com uma empresa altamente competente para garantir a segurança de seus.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Se você precisa realizar uma montagem de quadro de distribuição e procurar por uma empresa qualificada e recomendada para prestar esse serviço encontrou o lugar certo. Seja bem-vindo a MS Projetos Industriais, somos uma empresa que está há mais de 15 anos no mercado prestando serviços dentro da engenharia elétrica e automação industrial. Para realizar a montagem de quadro de distribuição, disponibilizamos profissionais altamente qualificados para realizar o seu atendimento com segurança e qualidade. São quase duas décadas prestando serviço para empresas privadas e para o setor público com ética, transparência, compromisso e qualidade. Para a segurança dos usuários é essencial que a montagem de quadro de distribuição seja realizada corretamente tendo em vista que esse equipamento é alimentado eletricamente com diversas tensões e correntes que podem ocasionar acidentes. Aproveite essa grande oportunidade de solicitar a montagem de quadro de distribuição com uma empresa altamente competente para garantir a segurança de seus equipamentos com a distribuição correta da energia para os circuitos individuais que os alimentam. Trabalhamos com a visão de elevar nossa importância nos próximos anos, nos tornando destaque dentre as maiores fornecedoras de serviços de engenharia e montagens industriais no cenário nacional.</p>
<h2>O melhor serviço de montagem de quadro de distribuição está aqui</h2>
<p>Durante a visita de nossos profissionais para realizar a montagem de quadro de distribuição, prestamos toda a assistência necessária para que o quadro de distribuição seja colocado em funcionamento corretamente. Realizar a montagem de maneira profissional é indispensável tendo em vista que o conhecimento necessário para realizar esse processo tem que ser extremamente afiado. Atuamos seguindo todas as normas técnicas e ambientais compreendendo que uma de nossas principais funções é atuar de forma sustentável preservando o ambiente para as futuras gerações. </p>
<h3>Saiba mais sobre a montagem de quadro de distribuição da MS Projetos Industriais</h3>
<p>Para saber mais sobre nosso serviço de montagem de quadro de distribuição ou quaisquer outros oferecidos por nossa empresa entre em contato e seja atendido por um especialista para te auxiliar da melhor maneira possível. Realizamos todo o atendimento técnico necessário e se você precisar existem diversos outros serviços que prestamos para indústrias como manutenção em subestação, termografia elétrica, emissão de laudo (NR-10, NR-12, aterramento, ICMS), fabricação de painéis elétricos e muito mais. Tudo que você precisa sobre engenharia elétrica e automação industrial você encontra aqui na MS Projetos Industriais. </p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>