<?php
    $title       = "empresa de Energia Fotovoltaica Lagoa Santa";
    $description = "Nossa empresa de energia fotovoltaica Lagoa Santa, MS Solar, faz desde o projeto, até a regulamentação com a sua concessionária de luz da região.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>No país inteiro, a energia solar vem ganhando espaço e cresce gradativamente no mercado de energia elétrica. Diversas empresas trabalham fornecendo energia de fonte solar, renovável, e algumas outras, projetam e oferecem sistemas completos para uso residencial, comercial e industrial. A MS Projetos é uma<strong> empresa de energia fotovoltaica Lagoa Santa </strong>que oferece tudo o que você precisa para ter um sistema regulamentado particular.</p>
<p>Nossa <strong>empresa de energia fotovoltaica Lagoa Santa </strong>oferece serviços como: instalação, comissionamento e suporte para startups de energia solar; estudo de viabilidade técnica financeira; prospecção de terras; dimensionamento do sistema solar; soluções completas e especificações técnicas para residenciais, indústrias e empresas em geral; e muito mais.</p>
<p>Tenha redução de até 95% da sua conta de energia elétrica investindo em painéis solares com nossa <strong>empresa de energia fotovoltaica Lagoa Santa. </strong>Nossa equipe especializada no segmento de energia solar fotovoltaica possui sede em Minas Gerais, Contagem, atuando no país inteiro. Desde a prospecção de imóveis até a implantação e comercialização da energia, desenvolvemos projetos com viabilidade em termos completos.</p>
<p>Implemente seu sistema de energia solar com nossa <strong>empresa de energia fotovoltaica Lagoa Santa.</strong> Com retornos do investimento de 3 a 7 anos após a instalação, dependendo do sistema, os painéis solares garantem redução drástica na conta de energia elétrica vinda das concessionárias de energia elétrica. Regulamentado, todo o processo feito com a MS Projetos é profissionalizado e conta com supervisão e desenvolvimento por um Engenheiro Eletricista especializado e certificado.</p>
<h2><strong>Empresa de energia fotovoltaica Lagoa Santa e o funcionamento da energia solar</strong></h2>
<p>Nossa<strong> empresa de energia fotovoltaica Lagoa Santa </strong>faz desde o projeto, regulamentação com a concessionária de luz, até a instalação e configuração dos sistemas de captação de energia solar. Funcionando mesmo nos dias nublados, os painéis captam a luz proveniente do sol, convertendo esta energia em um inversor para energia elétrica, para ser utilizado no sistema elétrico residencial, comercial ou industrial. Fale conosco para mais informações e serviços.</p>
<h3><strong>Empresa de energia fotovoltaica Lagoa Santa que realiza manutenção</strong></h3>
<p>A MS Projetos,<strong> empresa de energia fotovoltaica Lagoa Santa</strong>, oferece todo suporte em manutenção de sistemas de captação de energia solar que foram danificados ou perderam eficiência por algum motivo. A manutenção periódica se resume na limpeza das placas, que em tempos chuvosos acontece naturalmente. Em casos mais específicos é necessário diagnóstico profissional de um Engenheiro Eletricista especializado.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>