<?php
    $title       = "Usina Fotovoltaica";
    $description = "Conheça como funciona a usina fotovoltaica e suas vantagens. A MS Projetos Industriais disponibiliza serviços no segmento de geração fotovoltaica.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>A <strong>usina fotovoltaica </strong>trabalha com placas solares, normalmente compostas por silício, que irão transformar a energia solar em eletricidade. E como a energia é convertida em eletricidade? O efeito fotovoltaico é o que garante a transformação dessa energia. A instalação de um sistema fotovoltaico é rápida pode ser feita por uma empresa especializada.</p>
<p>O silício é um material renovável e precisa ser utilizado para a conversão de raios solares em energia, por isso, na <strong>usina fotovoltaica,</strong> além dos raios solares, é preciso que haja placas revestidas desse material para a geração de energia.</p>
<h2>Saiba mais sobre a <strong>usina fotovoltaica</strong> e seu funcionamento</h2>
<p>Como geralmente a <strong>usina fotovoltaica</strong> é instalada em áreas distantes e isoladas, as instalações interligadas aos quadros para usina solar enviam a energia aos centros urbanos através de linhas de transmissão.</p>
<p>No caso da <strong>usina fotovoltaica</strong>, são necessários outros dois aparelhos: a bateria e os reguladores. A bateria armazena energia não utilizada para o futuro — conhecido como créditos solares. Os reguladores protegem a bateria de sobrecargas e aumenta a eficácia do sistema e sua segurança.</p>
<p>No Brasil, a categoria de <strong>usina fotovoltaica</strong> está em amplo crescimento, e atualmente Minas Gerais possui uma dessas usinas em seu território. As vantagens da <strong>usina fotovoltaica </strong>são as seguintes:</p>
<ul>
<li>         Possibilidade de instalação em diversos lugares;</li>
<li>         Não emitem gases poluentes;</li>
<li>         Renovável;</li>
<li>         Silenciosa;</li>
<li>         Apresentam baixo custo de manutenção;</li>
<li>         Ocupam pouco espaço, entre outras.</li>
</ul>
<h3>Conheça a MS Projetos Industriais e a MS Solar</h3>
<p>Com sede em Contagem, Minas Gerais, a MS Projetos Industriais atua em todo o território nacional. Nosso segmento de geração fotovoltaica possui equipe especializada em comercialização e implantação de energia, construção, manutenção, entre tantos outros.</p>
<p>Em relação a MS Solar, executamos o melhor projeto viável em implantação, geração, rentabilidade e pós venda. Nossos serviços incluem: prospecção de terras, estudo de viabilidade técnica financeira, dimensionamento do sistema solar, fornecimento de solução completa, acompanhamento pós-venda, análise de dados após implantação, comercialização de energia, contrato e manutenção de energia solar, e muito mais.</p>
<p>Há mais de 15 anos no mercado, temos como diferencial uma equipe de colaboradores especializada e altamente qualificada para atender sua empresa.</p>
<p>Portanto, entre em contato com nossa equipe e conheça todas as facilidades disponíveis nesse segmento. Trabalhamos para garantir soluções e serviços à sua empresa e indústria, levando segurança e responsabilidade. Serviços em <strong>usina fotovoltaica</strong> se tornam ainda mais fáceis com a MS.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>