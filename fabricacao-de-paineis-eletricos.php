<?php
    $title       = "Fabricação de Painéis Elétricos";
    $description = "Atualmente com o desenvolvimento do setor de fabricação de painéis elétricos é possível encontrar grandes tecnologias sem a necessidade de importação.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Se você procura por uma empresa altamente qualificada e tecnicamente perfeita na <strong>fabricação de painéis elétricos </strong>encontrou o lugar certo.  Atualmente com o desenvolvimento do setor de <strong>fabricação de painéis elétricos </strong>é possível encontrar grandes tecnologias sem a necessidade de importação ou contratar empresas internacionais para prestar serviços de engenharia elétrica e automação industrial. Nossa empresa presta atendimento para grandes nomes do mercado que podem ser conferidos através da aba “portfólio” disponível em nosso site. Conheça um pouco mais dos atendimentos já realizados para ter a certeza que a MS Projetos Industriais é a empresa certa para prestar o serviço que vai te auxiliar. Além de <strong>fabricação de painéis elétricos </strong>atuamos com instalações industriais, montagem de painéis elétricos, montagem de subestação, gestão de projetos, implementação de automação industrial e muito mais. A nossa <strong>fabricação de painéis elétricos </strong>é realizada com componentes de alta qualidade fabricados por empresas de muita confiabilidade. Nossa missão é criar soluções em engenharia elétrica que garantam excelência, apoiadas na competência e no aprimoramento tecnológico buscando sempre agir de forma ética e sustentável.</p>
<h2><strong>A melhor fabricação de painéis elétricos</strong></h2>
<p>Para realizar o projeto e <strong>fabricação de painéis elétricos </strong>contamos com profissionais altamente qualificados instruídos nas melhores instituições de ensino do nosso segmento. A engenharia elétrica é uma das mais importantes vertentes da engenharia tendo em vista a influência geral que a administração desse setor tem na vida dos populares. Mais do que uma ciência exata a engenharia é uma metodologia de pensamento que nos permite encontrar detalhes que podem passar desapercebidos em diversas situações e realmente fazer a diferença. Esse senso critico e analítico é extremamente importante para chegar aos melhores resultados possíveis.</p>
<h3><strong>Saiba mais sobre a fabricação de painéis elétricos da MS Projetos Industriais</strong></h3>
<p>Nossa empresa está há mais de 15 anos no mercado realizando a <strong>fabricação de painéis elétricos </strong>e demais serviços. Entre em contato pelos canais de comunicação para tirar quaisquer dúvidas ou solicitar um orçamento para nossa equipe. Prezamos pelo resultado de forma sustentável realizando a manutenção dos recursos naturais para que o planeta seja conservado para as futuras gerações. Prestamos serviços para empresas privadas e para o setor publico com clareza, ética e comprometimento. A política de preservação do meio ambiente e o desenvolvimento sustentável são pontos de alta importância para nossa empresa, conte conosco.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>