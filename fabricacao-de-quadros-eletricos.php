<?php
    $title       = "Fabricação de Quadros Elétricos";
    $description = "A fabricação de quadros elétricos possui alta qualidade técnica e muita experiência para desenvolver equipamentos com ótimo custo x beneficio. Os quadros.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Se você procura por uma empresa qualificada tecnicamente que realize a <strong>fabricação de quadros elétricos </strong>com ótimos preços encontrou o lugar certo. A MS Projetos Industriais é uma das mais recomendadas empresas de engenharia elétrica e automação industrial em território nacional. Nossa empresa está há mais de 15 anos no mercado realizando a <strong>fabricação de quadros elétricos </strong>e diversos outros produtos de alta qualidade para o setor industrial. Nossa empresa atende o mercado privado e segmento publico com ética, transparência e compromisso. A missão de nossa empresa é criar soluções em engenharia elétrica que garantam excelência, apoiadas na competência e no aprimoramento tecnológico buscando sempre agir de forma ética e sustentável. Além da <strong>fabricação de quadros elétricos </strong>realizamos também a fabricação de diversos outros produtos e prestação de diversos serviços como: geração de energia solar, fabricação e instalação de painéis elétricos, instalações e montagem de equipamentos e maquinas industriais, inspeções, emissões de laudo e muito mais. Realizamos a <strong>fabricação de quadros elétricos </strong>com componentes de alta qualidade que são fabricados por empresas de credibilidade e confiabilidade para proporcionar o funcionamento adequado e seguro do equipamento.</p>
<h2><strong>Fabricação de quadros elétricos de alta performance</strong></h2>
<p>A <strong>fabricação de quadros elétricos </strong>possui alta qualidade técnica e muita experiência para desenvolver equipamentos com ótimo custo x beneficio. Os quadros elétricos são responsáveis pela distribuição elétrica de sua rede após receber a energia da fonte de alimentação. Dentro desse equipamento existem chaves que podem acionar ou interromper a alimentação de determinado ponto do esquema elétrico. Também é composto por disjuntores e fusíveis que complementam para o seu ideal funcionamento.</p>
<h3><strong>Saiba mais sobre a fabricação de quadros elétricos da MS Projetos Industriais</strong></h3>
<p>Para saber mais sobre nossa <strong>fabricação de quadros elétricos </strong>ou quaisquer outros produtos, ou serviços disponíveis em nosso catalogo entre em contato e seja atendido por um especialista para te auxiliar da melhor maneira possível. Para solicitar um orçamento basta preencher o formulário na aba “contato” e aguardar o contato de nossa equipe para lhe atender. Trabalhamos com a visão de elevar nossa importância nos próximos anos, nos tornando destaque dentre as maiores fornecedoras de serviços de engenharia e montagens industriais no cenário nacional. Seguimos todos os protocolos de segurança e legislativos respeitando as normas de segurança sobre engenheira elétrica e automação industrial além de atuar de forma sustentável visando preservar o planeta e o meio ambiente.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>