<?php
    $title       = "empresa de Energia Solar Betim";
    $description = "Solicite seu sistema com a MS Solar, empresa de energia solar Betim, e garanta economia de até 95% em sua conta de energia elétrica mensal.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>A MS Solar é uma empresa que presta serviços abrangendo todas as etapas para realizar a instalação de um sistema fotovoltaico. Esses serviços vão desde a inspeção e exploração de terrenos e propriedades para projetos, até as aprovações e homologação da empresa de distribuição de energia em sua área. De forma simples, econômica e rápida, a MS Solar oferece soluções que podem reduzir em até 95% a conta de luz pela energia fornecida da sua região. Contrate os serviços de nossa <strong>empresa de energia solar Betim.</strong></p>
<p>Conheça mais sobre nossa <strong>empresa de energia solar Betim. </strong>A MS Solar é um dos segmentos do grupo MS. Para todo o ramo da Engenharia Elétrica, oferecemos serviços de Projetos, Painéis, Montagens, Eficiência e Energia Solar. Desde a produção de quadros elétricos de distribuição, comando e sinalização, até a montagem e homologação de sistemas de energia fotovoltaica. Fale com nosso atendimento agora mesmo para mais informações.</p>
<p>A cada ano, nossa <strong>empresa de energia solar Betim </strong>vem se expandindo no cenário mineiro e nacional a partir de oportunidades de negócios exclusivas e soluções inteligentes para todo ramo da Engenharia Elétrica. Desde 1992 atuando no mercado como MS Eletric, em nosso portfólio constam diversos serviços para órgãos públicos e empresas privadas.</p>
<p>A<strong> empresa de energia solar Betim </strong>oferece serviços para você que procura usufruir de energia ecológica e sustentável. Gerada a partir da radiação solar, para captar a energia solar é necessário a utilização de painéis fotovoltaicos instalados em áreas como telhados, terrenos, paredes externas e fachadas de prédios. Após a energia ser captada pelos painéis, é convertida pelo inversor que por sua vez alimenta a rede para uso em equipamentos das instalações, para uso em tensões de 127v/220v e até 380v.</p>
<h2><strong>Nossa empresa de energia solar Betim faz desde a regulamentação até a instalação</strong></h2>
<p>Com a MS Solar,<strong> empresa de energia solar Betim</strong>, é possível encontrar serviços que dão suporte a todas as etapas envolvidas para obtenção e uso de energia solar. Todos os equipamentos utilizados para a instalação de sistemas de geração de energia solar pela MS Solar são certificados pelo órgão Inmetro e homologados pela concessionária de energia da sua região de acordo com os processos de aprovação do projeto.</p>
<h3><strong>Economia na conta de energia de até 95% com nossa empresa de energia solar Betim</strong></h3>
<p>Solicite seu orçamento para instalação de seu sistema com nossa <strong>empresa de energia solar Betim </strong>e garanta economia de até 95% em sua conta de energia elétrica mensal. Consulte-nos para mais informações e orçamentos para projetos.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>