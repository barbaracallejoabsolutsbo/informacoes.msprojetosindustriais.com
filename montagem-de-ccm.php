<?php
    $title       = "Montagem de CCM";
    $description = "Ao solicitar a montagem de CCM você conta com um time de profissionais altamente qualificados que tiverem suas capacitações técnicas nas principais.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Se você procura por uma empresa que realize a montagem de CCM com qualidade, agilidade e preço justo chegou ao lugar certo. A MS Projetos Industriais é uma empresa que atua dentro da engenharia elétrica e automação industrial com muita propriedade técnica oferecendo as melhores soluções. A sigla CCM refere-se a “centro de controle de motores” então podemos concluir que a montagem de CCM é a montagem de dispositivo que controle o acionamento dos motores de todo o maquinário da indústria. Esse equipamento pode operar com motores de diversas tensões. Dentro do CCM existem diversos componentes como dispositivos de medição, controles lógicos programáveis (CLP), acionadores entre outros. Esse equipamento é utilizado desde 1950 e vem sofrendo atualizações conforme as necessidades aumentam e a tecnologia dentro do segmento elétrico avança. Principalmente nas indústrias automotivas foi muito utilizado esse equipamento e atualmente é possível encontrá-los em empresas de diversos segmentos. Solicite a montagem de CCM com quem realmente tem propriedade técnica para prestar esse serviço com muita qualidade. A vantagem de realizar a montagem de CCM é realizar o acionamento e controle de todo o maquinário do ambiente de forma segura.</p>
<h2>Solicite a montagem de CCM com a MS Projetos Industriais</h2>
<p>Ao solicitar a montagem de CCM você conta com um time de profissionais altamente qualificados que tiverem suas capacitações técnicas nas principais instituições de ensino dentro de nosso segmento no Brasil. Nossa missão é criar soluções em engenharia elétrica que garantam excelência, apoiadas na competência e no aprimoramento tecnológico buscando sempre agir de forma ética e sustentável. Para alcançar esses resultados, contamos com uma equipe de alta qualidade e conhecimento técnico adquirido nas principais instituições de ensino dentro do nosso segmento.</p>
<h3>Saiba mais sobre a montagem de CCM</h3>
<p>Para saber mais sobre a montagem de CCM ou quaisquer outros serviços oferecidos por nossa empresa entre em contato e seja prontamente atendido por um especialista para te auxiliar da melhor maneira possível. Trabalhamos com a visão de elevar nossa importância nos próximos anos, nos tornando destaque dentre as maiores fornecedoras de serviços de engenharia e montagens industriais no cenário nacional. Atuamos seguindo todas as normas técnicas e ambientais compreendendo que uma de nossas principais funções é atuar de forma sustentável preservando o ambiente para as futuras gerações. Tudo que você precisa sobre engenharia elétrica e automação industrial você encontra em um só lugar.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>