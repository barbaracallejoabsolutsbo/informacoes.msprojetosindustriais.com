<?php
    $title       = "empresa de Energia Fotovoltaica Belo Horizonte";
    $description = "A MS Solar é uma empresa de energia fotovoltaica Belo Horizonte do Grupo MS, no ramo de geração de energia fotovoltaica, com sede em Contagem/MG.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Se você está buscando por uma<strong> empresa de energia fotovoltaica Belo Horizonte</strong>, acaba de encontrar a solução. A MS Projetos é uma empresa que atua no ramo da Engenharia Elétrica e oferece soluções inteligentes e modernas para sistemas elétricos. Um destes tipos de sistema é o de captação e gerenciamento de energia solar, um tipo de energia renovável, ecológico e econômico que garante redução de até 95% em contas de energia elétrica, dependendo do seu investimento.</p>
<p>A MS Projetos Industriais é uma <strong>empresa de energia fotovoltaica Belo Horizonte </strong>que oferece tudo o que você precisa para implementação de um sistema de energia fotovoltaica. Trabalhando no ramo de Engenharia Elétrica desde 1992, somos referência no cenário mineiro e nacional. Com diferenciais como alta eficiência, baixo custo, qualidade e cumprimento de prazos, a cada ano a MS Projetos vem desenvolvendo melhores oportunidades de negócio. Venha fazer seu orçamento conosco.</p>
<p>Nossa <strong>empresa de energia fotovoltaica Belo Horizonte </strong>trabalha também com serviços como: entrada de energia de baixa tensão, armários e quadros customizados, caixas sob medida, quadros elétricos de média tensão, quadros elétricos para comando predial, sinalização, redundância para data centers, quadros para bombas de recalque, incêndio e saneamento. Conheça os nossos produtos feitos especialmente de forma personalizada para você.</p>
<p>Outros serviços também disponíveis em nossa <strong>empresa de energia fotovoltaica Belo Horizonte </strong>são adequação, regulamentação, documentação e medição de sistemas elétricos industriais para enquadramento em normas regulamentadoras.</p>
<p></p>
<h2><strong>Contrate a MS Projetos como sua empresa de energia fotovoltaica Belo Horizonte</strong></h2>
<p>A MS Solar é uma <strong>empresa de energia fotovoltaica Belo Horizonte</strong> do Grupo MS, no ramo de geração de energia fotovoltaica, com sede em Contagem/MG, com atuação em todo o país. Nossa equipe é especializada em registro, projeto, construção, implantação e comercialização de energia solar. A MS Solar é a melhor empresa capaz de executar em termos de implantação do projeto, geração de energia, rentabilidade e suporte pós-venda.</p>
<h3><strong>Serviços disponíveis de energia solar na empresa de energia fotovoltaica Belo Horizonte</strong></h3>
<p>Contrate a MS Projetos para ser a<strong> empresa de energia fotovoltaica Belo Horizonte </strong>que irá desenvolver o seu projeto.Os sistemas fotovoltaicos são projetados para ter uma vida útil de pelo menos 25 anos e, em alguns casos, de até 30 anos sem perder eficiência que comprometa o funcionamento. Os serviços que vão desde a pesquisa de locais para determinar os melhores equipamentos e painéis até formas de manutenção preventiva, proporcionando qualidade no uso, geração e segurança aos consumidores.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>