<?php
    $title       = "empresa de Energia Solar Juiz de Fora";
    $description = "Conheça mais sobre os serviços especiais da MS Solar. Somos uma empresa de energia solar Juiz de Fora com sede em Contagem, Minas Gerais. ";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Conheça mais sobre os serviços especiais da MS Solar. Somos uma<strong> empresa de energia solar Juiz de Fora </strong>com sede em Contagem, Minas Gerais. Oferecendo serviços para todo o país, somos especializados em painéis solares para geração de energia fotovoltaica e todas as etapas envolvidas para obtenção de sistemas deste tipo, sejam integrados na rede elétrica ou isolados.</p>
<p>Os serviços disponibilizados em nossa <strong>empresa de energia solar Juiz de Fora</strong> consistem em instalação e projetos completos para sistema de energia fotovoltaica. Para geração de energia limpa e sustentável, oferecemos serviços que envolvem todas as etapas de regulamentação com sua companhia elétrica, reduzindo significativamente seu gasto com energia elétrica podendo até mesmo, estar consumindo mais energia.</p>
<p>O funcionamento da energia coletada pelos sistemas instalados por nossa <strong>empresa de energia solar Juiz de Fora</strong>, é baseado em placas solares fotovoltaicas que, por geração de energia elétrica a partir de conversão de raios solares, podem alimentar residências, empresas, indústrias e áreas rurais. Sendo uma alternativa econômica, sustentável e ecológica, tem se tornado a escolha de investimento para autonomia no consumo de energia.</p>
<p>Com retornos em até 7 anos do investimento, os sistemas de painéis fotovoltaicos utilizados por nossa <strong>empresa de energia solar Juiz de Fora </strong>são aprovados pelo Inmetro e homologados, durante o processo de regulamentação, junto a sua companhia de eletricidade da região.</p>
<h2><strong>Geração de energia ecológica com nossa</strong> <strong>empresa de energia solar Juiz de Fora</strong></h2>
<p>Baseado em um fenômeno químico da colisão de átomos de silício que compõem as placas solares com os fótons provenientes da luz solar, a energia é gerada a partir de uma corrente elétrica contínua pelo deslocamento dos elétrons, criando a energia solar fotovoltaica, que por sua vez, é convertida pelo inversor em energia elétrica para distribuição a rede elétrica doméstica, industrial e comercial. Contrate nossos serviços e obtenha seu sistema fotovoltaico com nossa <strong>empresa de energia solar Juiz de Fora</strong>.</p>
<h3><strong>Modelos de sistema de energia fotovoltaica, empresa de energia solar Juiz de Fora</strong></h3>
<p>Nossa<strong> empresa de energia solar Juiz de Fora </strong>oferece suporte para manutenção, instalação e projetos de energia fotovoltaica em diversos modelos de operação. Entre opções off-grid, on-grid e híbridos, os sistemas são regulamentados e homologados com sua companhia de distribuição de energia. O excesso de energia gerada pelo seu sistema, quando conectado à rede elétrica, gera créditos para utilização em abate de valor em contas de energia elétrica da sua companhia.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>