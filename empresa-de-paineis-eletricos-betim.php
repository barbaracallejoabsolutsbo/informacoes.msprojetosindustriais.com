<?php
    $title       = "empresa de Painéis Elétricos Betim";
    $description = "Empresa de painéis elétricos Betim que desenvolve projetos de painéis e quadros elétricos com auxílio de equipes técnicas profissionais, conheça a MS Projetos.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Somos uma<strong> empresa de painéis elétricos Betim </strong>que desenvolve projetos de painéis e quadros elétricos com auxílio de equipes técnicas profissionais. Acompanhando todas as etapas de montagem dos painéis, ofertamos qualidade, excelência e prazos incríveis em nossos serviços, com preço acessível e justo.</p>
<p>Encontre outros serviços com a MS Projetos, conheça mais sobre serviços completos para toda área da Engenharia Elétrica. Oferecendo serviços para todo o país, a MS Projetos atua no ramo de serviços para sistemas elétricos em geral. Nossa<strong> empresa de painéis elétricos Betim</strong> produz conforme projeto, dentro de normas nacionais e internacionais, quadros e painéis de comando, distribuição e sinalização.</p>
<p>A MS Painéis,<strong> empresa de painéis elétricos Betim</strong>, atua atendendo condomínios, residências, indústrias, empresas e muito mais. Consulte nosso site para mais informações e tenha suporte completo de profissionais especializados na em sistemas elétricos. Entre muitas opções para melhoria de eficiência, segurança e regulamentação, conheça um pouco mais sobre nossos serviços</p>
<p>Pensando em sempre oferecer serviços com excelência e comprometimento, em nossa <strong>empresa de painéis elétricos Betim </strong>você encontraserviços exclusivos. Desde a manutenção completa para painéis e quadros elétricos, reposição de peças, reforma, instalação e projetos de quadros e painéis além de armários e quadros personalizados, caixas sob medida, quadros elétricos de média tensão, entrada de energia de baixa tensão, quadros para bombas de incêndio, saneamento e muito mais</p>
<h2><strong>Muito mais em nossa empresa de painéis elétricos Betim</strong></h2>
<p>A <strong>empresa de painéis elétricos Betim</strong> oferece o melhor custo benefício e profissionalismo. A MS Painéis, do grupo MS, é uma empresa que atua no ramo de energia elétrica realizando projetos, estudos, regulamentação, instalação e manutenção de diversos tipos de equipamentos e sistemas elétricos. Falando conosco é possível obter orçamentos e mais informações sobre os serviços.</p>
<h3><strong>A empresa de painéis elétricos Betim que você procurava, fale conosco</strong></h3>
<p>Solicite um orçamento com nossa <strong>empresa de painéis elétricos Betim </strong>e tenha o melhor custo benefício com suporte completo para manutenção, planejamento, construção e instalação de quadros e painéis elétricos. Comprometendo-nos sempre a atender serviços preservando o meio ambiente e proporcionando desenvolvimento sustentável, a MS Projetos possui uma gama de serviços que vão desde projetos de eficiência para indústrias até a instalação e geração de energia solar para consumo residencial, empresarial, comercial e industrial.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>