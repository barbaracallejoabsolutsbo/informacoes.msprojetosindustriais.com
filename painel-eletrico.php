<?php
    $title       = "Painel Elétrico";
    $description = "O painel elétrico é um importante equipamento do sistema elétrico tendo em vista que é o responsável por receber a eletricidade, converter de forma segura e.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Se você procura por uma empresa que fabrique ou comercialize painel elétrico para comprar direto da fabrica encontrou o lugar certo. A MS Projetos Industriais é uma empresa que atua nos segmentos de engenharia elétrica e automação industrial fabricando e comercializando equipamentos e prestando diversos serviços. O painel elétrico é um importante equipamento do sistema elétrico tendo em vista que é o responsável por receber a eletricidade, converter de forma segura e redistribuir para pequenos circuitos individuais ou pontos de alimentação. Além de painel elétrico fabricamos também diversos componentes como quadros de eletricidade, subestação de energia, painéis fotovoltaicos e muito mais. Nossa empresa tem a missão de criar soluções em engenharia elétrica que garantam excelência, apoiadas na competência e no aprimoramento tecnológico buscando sempre agir de forma ética e sustentável. Solicite seu painel elétrico com instalação em uma das empresas referências nesse segmento dentro do Brasil. Para saber mais sobre nossa empresa e sobre nossos serviços acesse nosso site ou entre em contato. A instalação desse tipo de equipamento deve ser realizada por um profissional qualificado com registro ativo no órgão regulamentador (CREA). </p>
<h2>O painel elétrico que você procura está aqui dentre nossas opções</h2>
<p>Consulte nosso catálogo de produtos e encontre o painel elétrico perfeito para atender suas necessidades. Atendemos grandes nomes da indústria que podem ser consultados através da aba “portfolio” disponível em nosso site. Nossa empresa atua tanto prestando serviço para o setor privado quanto para o setor público com qualidade, ética, respeito e compromisso. Trabalhamos com a visão de elevar nossa importância nos próximos anos, nos tornando destaque dentre as maiores fornecedoras de serviços de engenharia e montagens industriais no cenário nacional.</p>
<h3>Tudo que você precisa saber sobre o painel elétrico</h3>
<p>Para saber mais sobre o painel elétrico ou quaisquer outros produtos, ou serviços disponíveis em nosso catálogo entre em contato e seja atendido por um especialista para te auxiliar da melhor maneira possível. É importante que a manutenção desse equipamento seja feita periodicamente para prevenir falhas e acidentes que podem ir de pequenos choques até grandes incêndios em caso de falha grave por falta de manutenção do equipamento. Atuamos seguindo todas as normas técnicas e ambientais compreendendo que uma de nossas principais funções é atuar de forma sustentável preservando o ambiente para as futuras gerações. Tudo que você precisa sobre engenharia elétrica e automação industrial em um só lugar. </p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>