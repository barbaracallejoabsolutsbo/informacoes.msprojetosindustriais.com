<?php
    $title       = "empresa de Quadros Elétricos Belo Horizonte";
    $description = "A MS Painéis é uma empresa de quadros elétricos Belo Horizonte que oferece serviços para toda área de Engenharia Elétrica. Conheça mais sobre a MS Projetos.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>A MS Painéis é uma <strong>empresa de quadros elétricos Belo Horizonte</strong> que oferece serviços para toda área de Engenharia Elétrica. Atuando desenvolvendo projetos elétricos industriais, montagem industrial, projetos para captação de energia solar, dentre muitas outras opções, consulte nosso site e conheça mais sobre os serviços disponíveis com nossa empresa.</p>
<p>Encontre serviços especializados em nossa <strong>empresa de quadros elétricos Belo Horizonte</strong> para indústrias da área siderúrgica, saneamento, geração de energia, e mineração. Nossos profissionais são experientes e qualificados, trabalhando sempre em prol da ecologia e sustentabilidade. Consulte serviços especializados em captação de energia solar disponíveis com a MS Solar. Todos os serviços são atendidos pensando na preservação do meio ambiente.</p>
<p>Encontre produtos e serviços dentro de normas nacionais e internacionais com a MS Painéis. Em nossa <strong>empresa de quadros elétricos Belo Horizonte </strong>você encontra serviços para montagem e criação de armários e quadros customizados, entrada de baixa e média tensão, quadros para comando predial, quadros para banco de capacitores, automação de subestação elétrica, quadros de distribuição de tomadas e iluminação, dentre outros produtos e serviços personalizados.</p>
<p>Os projetos envolvidos abrangem diversas etapas do processo de projeto CEMIG. Desde o projeto de entrada de energia conforme ND’s até a parametrização de relé de proteção CEMIG, contrate nossos serviços e usufrua de suporte completo também para realização de toda parte burocrática do processo do início ao fim. Com vasta experiência no segmento, trabalhamos também com elaboração e aprovação de Projetos CEMIG em nossa <strong>empresa de quadros elétricos Belo Horizonte</strong>. Conheça mais sobre a MS.</p>
<h2><strong>Outros serviços em empresa de quadros elétricos Belo Horizonte</strong></h2>
<p>Normalmente em sistemas elétricos industriais, por proteção, são instalados fusíveis, relés e disjuntores. Com a MS, <strong>empresa de quadros elétricos Belo Horizonte,</strong> garantimos também que seus equipamentos não sofram danos.  Conheça nossos projetos de seletividade de proteção, proteção para curtos circuitos afetem, geradores de energia, nobreak e sistema de proteção para todos seus equipamentos.</p>
<h3><strong>Empresa de quadros elétricos Belo Horizonte, MS Projetos e Montagem Industrial</strong></h3>
<p>Com a nossa <strong>empresa de quadros elétricos Belo Horizonte</strong> você encontra diversos serviços para empresas do setor de geração de energia, saneamento, mineração, etc. Atuando com fabricação, montagem de caixas de distribuição em geral, instalações elétricas de baixa e média tensão, adequação a NR10 e NR12, instalação de nobreak, geradores, criação e projetos de sistemas elétricos completos, automação, consulte-nos para mais informações e orçamentos.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>