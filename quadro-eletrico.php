<?php
    $title       = "Quadro Elétrico";
    $description = "Quadro elétrico: conheça objetivos, modelos e funções. A MS Projetos Industriais realiza instalações e outros recursos de qualidade para sua empresa.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Os quadros elétricos são parte essencial de qualquer espaço que necessite de energia elétrica para sua atividade, seja ele comercial, residencial ou industrial. Sendo um equipamento que recebe energia elétrica de uma ou mais fontes de alimentação, o <strong>quadro elétrico</strong> distribui essa energia conforme programado.</p>
<p>Um <strong>quadro elétrico</strong> é composto por diversos acessórios e sistemas, como fusíveis, unidades de proteção, disjuntores, barramento elétrico, entre outros. Para realizar sua função de distribuição ele deve ser construído seguindo regulamentações técnicas, por isso, a contratação de uma empresa especializada como a MS é importante.</p>
<h2>Objetivos e modelos de um <strong>quadro elétrico</strong></h2>
<p>Também chamado quadro de distribuição de energia elétrica, o <strong>quadro elétrico </strong>recebe energia de uma ou mais fontes e distribui por um ou mais circuitos. Seu objetivo, então, é automatizar comandos elétricos e aperfeiçoar a execução das tarefas no sistema de controle e distribuição de energia elétrica.</p>
<p>O <strong>quadro elétrico</strong> leva segurança e evita descargas elétricas e curtos circuitos que poderiam causas danos ao local e às pessoas e comprometeriam a produtividade e funcionalidade do ambiente.</p>
<p>Para projetar e instalar um <strong>quadro elétrico</strong> é necessária a inspeção de itens como o envelhecimento acelerado de componentes, vida útil desses componentes, riscos de incêndio, limites de temperatura, falhas prematuras, e assim por diante. A MS Projetos Industriais possui profissionais especializados para esse serviço, levando segurança e trabalhadores qualificados na instalação e manutenção de quadros elétricos.</p>
<p>Os tipos de <strong>quadro elétrico</strong> podem variar conforme suas necessidades e funções, entre eles, estão:</p>
<ul>
<li>         Quadro geral de baixa tensão;</li>
<li>         Painel elétrico de distribuição e sub-distribuição;</li>
<li>         Centro de controle de motores;</li>
<li>         Painel elétrico de comando e controle;</li>
<li>         Painel elétrico para acionamentos, entre outros.</li>
</ul>
<p>Cada modelo é projetado para um arranjo específico de equipamentos, incluindo aparelhos que fazem o controle de dispositivos mecânicos. Assim, entre em contato com a MS, empresa especializada para definir qual o melhor <strong>quadro elétrico</strong> que atenderá suas necessidades.</p>
<h3>MS Projetos Industriais, excelência em operações integradas</h3>
<p>Localizados em Contagem, atendemos todo o estado de Minas Gerais e estamos conquistando reconhecimento nacional. Objetivamos atender as expectativas de nossos clientes ao proporcionar ótimo custo benefício, qualidade nos materiais, cumprimentos de prazos, e muito mais.</p>
<p>Há mais de 15 anos no mercado, prestamos serviço para órgãos públicos e empresas privadas. Realizamos nosso trabalho com competência técnica, desenvolvimento de soluções ligadas à engenharia e soluções integradas. Entre em contato conosco e solicite um orçamento.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>