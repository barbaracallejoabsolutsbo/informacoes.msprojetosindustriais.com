<?php
    $title       = "empresa de Energia Solar BH";
    $description = "Pensando na expansão das possibilidades em ecologia, sustentabilidade e economia, a MS Solar empresa de energia solar BH oferece serviços completos para você.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Conheça a MS Solar. Uma<strong> empresa de energia Solar BH </strong>do grupo MS com especialistas em sistemas de energia fotovoltaica.Oferecemos serviços que abrangem todas as etapas para obtenção de seu próprio sistema de energia solar fotovoltaica. Desde sistemas integrados com a rede de distribuição quanto como sistemas isolados, com a MS Solar você encontra suporte completo profissional.</p>
<p>Serviços como manutenção e limpeza de sistemas fotovoltaicos também podem ser encontrados em nossa <strong>empresa de energia solar BH</strong>. Mantenha sempre a manutenção de seus equipamentos e sistemas fotovoltaicos e garanta autonomia e eficiência de seus equipamentos por pelo menos 25 anos. Em alguns casos, alcançando até 30 anos sem perda significativa de eficiência, painéis solares é um investimento muito rentável e ecológico.</p>
<p>Investindo em tecnologia e soluções modernas para energia ecológica de fonte solar, nossa <strong>empresa de energia solar BH </strong>é referência no cenário nacional e mineiro. Desde montagens, projetos, até estudos de eficiência e sistemas painéis solares até a construção de quadros elétricos você encontra com a MS.</p>
<p>Para projetos comerciais, empresariais, residenciais, entre outros, nossa <strong>empresa de energia solar BH </strong>oferece mão de obra certificada e experiente para todos os processos envolvidos na implementação de sistemas de energia fotovoltaica.Energia ecológica, gerada por fonte renovável e não poluente. Invista no futuro da energia sustentável, invista com a MS Solar.</p>
<h2><strong>Empresa de energia solar BH que pensa na sustentabilidade e ecologia </strong></h2>
<p>Pensando na expansão das possibilidades em ecologia, sustentabilidade e economia, a MS Solar<strong> empresa de energia solar BH </strong>oferece serviços completos para você. Levamos energia renovável e economia para sua conta de energia elétrica. Fale conosco agora mesmo para orçamentos e mais informações.</p>
<h3><strong>Motivos para investir com nossa empresa de energia solar BH</strong></h3>
<p>Investir com nossa <strong>empresa de energia solar BH </strong>pode garantir diversos benefícios para você, seja para sua residência, empresa, nicho industrial, entre outros. Além de otimização e economia, a durabilidade de sistemas de energia solar possui garantia para até 25 anos de funcionamento sem perda de eficiência do sistema. O investimento no sistema pode ser pago em até 7 anos depois da instalação. Contrate agora mesmo a MS Solar para projetos de painéis solares.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>