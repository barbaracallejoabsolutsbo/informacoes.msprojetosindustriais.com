<?php
    $title       = "Instalação de Quadros Elétricos";
    $description = "Se você procura por uma instalação de quadros elétricos oferecida por uma empresa altamente recomendada e que atua em todas as regiões do Brasil chegou ao lugar certo.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Se você procura por uma instalação de quadros elétricos oferecida por uma empresa altamente recomendada e que atua em todas as regiões do Brasil chegou ao lugar certo. A MS Projetos Industriais é uma empresa que atua dentro da engenharia elétrica e automação industrial fornecendo equipamentos, prestando serviços e fazendo a diferença de forma sustentável. São mais de 15 anos prestando serviço para empresas privadas e o setor público com ética, transparência, compromisso e qualidade. A missão da nossa empresa é criar soluções em engenharia elétrica que garantam excelência, apoiadas na competência e no aprimoramento tecnológico buscando sempre agir de forma ética e sustentável. Contamos com uma grande experiência para realizar a instalação de quadros elétricos tendo em vista que já prestamos esse serviço há muito tempo. Além da instalação de quadros elétricos você pode também realizar a compra de nossos produtos produzidos com componentes de alta qualidade fabricados por empresas de grande credibilidade e confiabilidade. Além da instalação de quadros elétricos prestamos diversos serviços como manutenção em subestação, termografia elétrica, emissão de laudo (NR-10, NR-12, aterramento, ICMS), fabricação de painéis elétricos e muito mais. Visite nosso site para conhecer um pouco mais sobre nossa empresa e nossos serviços.</p>
<h2>O melhor lugar para realizar sua compra e solicitar instalação de quadros elétricos</h2>
<p>Por sermos fabricantes realizamos a instalação de quadros elétricos com muita propriedade tendo o conhecimento de todas as necessidades de nosso equipamento. Para projetar, fabricar e instalar os quadros elétricos contamos com profissionais altamente qualificados com grande propriedade técnica instruídos pelas melhores instituições de ensino dentro de nosso segmento. Com o senso crítico e analítico de nossos profissionais pode-se de forma técnica e seguindo todas as normas alcançar resultados incríveis com nossos produtos e serviços. </p>
<h3>Saiba mais sobre nossa instalação de quadros elétricos</h3>
<p>Para saber mais sobre a instalação de quadros elétricos ou quaisquer outros produtos e serviços realizados por nossa empresa entre em contato e seja atendido por um especialista para te auxiliar da melhor maneira possível. Atendemos grandes nomes da indústria que podem ser consultados através da aba “portfolio” disponível em nosso site. Trabalhamos com a visão de elevar nossa importância nos próximos anos, nos tornando destaque dentre as maiores fornecedoras de serviços de engenharia e montagens industriais no cenário nacional. Conte com a MS Projetos Industriais para tirar seus planos do papel com segurança e qualidade. </p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>