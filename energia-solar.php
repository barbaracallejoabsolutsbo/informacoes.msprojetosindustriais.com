<?php
    $title       = "Energia Solar";
    $description = "A MS Solar conta com mão de obra profissionalizada e toda excelência de uma empresa de energia solar que preza por qualidade, conheça mais. ";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>A MS Solar é uma empresa que trabalha na instalação e implementação de sistemas de captação de <strong>energia solar</strong>. A Energia Fotovoltaica é a energia proveniente do sol que, com placas fotovoltaicas é possível captá-la e, com um inversor especial instalado no sistema elétrico, é possível convertê-la em energia elétrica para uso doméstico, industrial e comercial.</p>
<p>A <strong>energia solar</strong> é uma alternativa sustentável e ecológica de gerar energia. Por não produzir poluentes na atmosfera e se tratar de uma fonte renovável de energia, muitas empresas vêm investindo forte neste segmento como a Tesla, por exemplo. Energia fotovoltaica é um grande progresso para o futuro e, pensando nisso, a MS Solar oferece serviços especializados para todas as etapas de implementação de sistemas de energia fotovoltaica.</p>
<p>A MS Solar conta com mão de obra profissionalizada e toda excelência de uma empresa de<strong> energia solar </strong>que preza por qualidade. Auxiliamos em todo o processo de busca de imóveis e terrenos para a implantação de um sistema completo de geração de energia fotovoltaica em larga escala até a instalação e posicionamento dos painéis. Desde o projeto, construção do sistema, implantação e até mesmo para comercialização de energia proveniente do sol, oferecemos suporte. Conheça mais sobre a MS Solar.</p>
<p>A MS Solar, empresa que realiza projetos para implementação de sistemas de captação de <strong>energia solar</strong>, coloca a sua equipe a prontidão para atendimento e mais informações sobre serviços falando conosco por e-mail, telefone ou WhatsApp.</p>
<h2><strong>Energia solar é o futuro da energia ecológica</strong></h2>
<p>Existem diversos estudos sendo realizados no cenário científico que comprovam que a partir de 2035, a<strong> energia solar </strong>pode se tornar a energia mais utilizada, ultrapassando o uso de energia eólica, carvão e hidrelétricas. Por possuir baixo custo de produção, flexibilidade para construção e localização dos locais que geram este tipo de energia, não produz nenhum poluente e é uma fonte renovável de energia.</p>
<h3><strong>Conheça mais sobre os serviços de energia solar da MS</strong></h3>
<p>Nossa empresa possui mais de 15 anos de experiência em painéis elétricos em projetos e gerenciamento de projetos, automação, montagem e instalação no mercado nacional. Expandindo para área de energia solar, visamos oferecer o que há de melhor em soluções ecológicas para engenharia elétrica.</p>
<p>Outro benefício de instalar um sistema fotovoltaico para captação de <strong>energia solar</strong> conosco inclui: geração de energia limpa e sustentável, retorno garantido do investimento, valorização do imóvel, economia de até 95% na conta de energia elétrica.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>