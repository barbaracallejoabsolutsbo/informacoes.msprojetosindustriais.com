<?php
    $title       = "Painéis Elétricos Contagem";
    $description = "Painéis elétricos Contagem pode ser encontrado na MS Projetos Industriais. Fazemos instalação de quadros elétricos, atendendo todas as normas técnicas.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>O mercado de painéis elétricos possui grande abrangência e necessita de configuração adequada em cada projeto. Para que sua instalação seja feita de maneira correta, sua empresa deve procurar um serviço especializado, já que o setor elétrico é um dos segmentos que mais merecem atenção, pois servem para proteger circuitos elétricos, para ligar máquinas e motores, e assim por diante.</p>
<p>A instalação de <strong>painéis elétricos Contagem</strong> pode ser executada pela MS Projetos Industriais. Somos referencia no mercado quando o assunto é montagem de <strong>painéis elétricos Contagem</strong> de baixa e média tensão.</p>
<h2><strong>Painéis elétricos Contagem</strong> essenciais para a sua segurança</h2>
<p>Sediada em Minas Gerais, na cidade de Contagem, nossa empresa vem se destacando por um serviço de qualidade que atende requisitos como eficiência, ótimo custo benefício, cumprimento de prazos, qualidade de materiais, e assim por diante.</p>
<p>Trabalhamos com serviço de engenharia elétrica e automação industrial, oferecendo soluções integradas e montagens industriais. Um dos segmentos mais destacados é o trabalho com <strong>painéis elétricos Contagem</strong>. Dessa forma, montamos painéis elétricos conforme a demanda de cada projeto, desde aplicações em aeroportos, hotéis, centros comerciais, shoppings, até, indústrias no geral. Utilizando os melhores fornecedores e seguindo as normas técnicas, a solução de sistemas de <strong>painéis elétricos Contagem </strong>é garantida por nós.</p>
<p>O uso de <strong>painéis elétricos Contagem</strong> é obrigatório em setores variados, como hospitais, comércios, condomínios, lojas, indústrias, e assim por diante. O ramo que mais utiliza esse serviço é o industrial, nesse setor são diversas as particularidades para instalação e utilização, por isso, o mais indicado é a contratação de um serviço especializado, como os <strong>painéis elétricos Contagem</strong>.</p>
<p>Os painéis elétricos são utilizados em comércios que precisam de controle das atividades, indicados para o comando e distribuição de energia elétrica. Assim, eles garantem proteção aos circuitos elétricos, levando segurança para os trabalhadores e impedindo danos à empresa em sua totalidade.</p>
<h3>MS Projetos Industriais oferece excelência em <strong>painéis elétricos Contagem</strong></h3>
<p>Temos como missão disponibilizar soluções em engenharia que garantam excelência, agindo de forma ética e sustentável e utilizando aprimoramento tecnológico e competência. Para isso, trabalhamos com excelência no atendimento aos clientes, no cumprimento das normas técnicas e na utilização de materiais de qualidade fornecidos no mercado.</p>
<p>A MS Projetos Industriais está há mais de 15 anos no mercado, prestando serviço para órgãos públicos e empresas privadas. Com empreendedorismo, competência técnica e desenvolvimento de soluções ligadas à engenharia, estamos conquistando reconhecimento tanto mineiro, como em território nacional.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>