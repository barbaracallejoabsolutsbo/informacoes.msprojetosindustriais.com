<?php
    $title       = "Adequação NR10";
    $description = "Nosso serviço de adequação NR10 é feita e analisada por um engenheiro eletricista da MS Projetos, conheça nossos serviços de engenharia elétrica.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Conheça a MS Projetos Industriais. Com o tempo mais hábil do mercado, inspecionamos e emitimos laudos para toda a instalação elétrica industrial em qualquer local do país. Entre diversos serviços de instalação elétrica para os mais variados segmentos, atendemos a todas as normas nacionais e internacionais.</p>
<p>Trabalhamos com materiais dos melhores fornecedores, seguindo todos critérios da Engenharia, produzimos desde quadros elétricos de distribuição, comando e sinalização para shoppings e prédios comerciais, até laudos e inspeções. Conheça nosso serviço de <strong>adequação NR10.</strong></p>
<p>O laudo NR10 é um tipo de documento oficial emitido por um profissional habilitado de engenharia elétrica. Neste laudo consta diversas informações levantadas e que devem possuir <strong>adequação NR10</strong>, uma norma regulamentadora para controle das instalações, para que não haja riscos elétricos aos colaboradores ou mesmo usuários dos espaços que interagem com estas instalações elétricas.</p>
<p>A <strong>adequação NR10</strong> é um serviço de implementação para que todas as normas de segurança para operação de máquinas industriais estejam de acordo, para funcionamento seguro para funcionários e demais pessoas que acessam o local. Para estar de acordo com a <strong>adequação NR10, </strong>o local deve estar acordo com algumas normas: Sistema de gestão da segurança, documentação técnica e projetos, certificações de equipamentos e ferramentas, procedimentos de trabalho e instruções técnicas, comprovação de treinamento para colaboradores, análises de riscos e medidas de controle, além de alguns pontos a consultar com a MS Projetos Industriais.</p>
<h2><strong>Fale conosco agora mesmo e conheça mais sobre nossa adequação NR10</strong></h2>
<p>Visando garantir a segurança de todos envolvidos em trabalhos com eletricidade e para suprir as necessidades da <strong>adequação NR10, </strong>a MS Projetos Industriais desenvolve seu projeto por equipes técnicas especializadas e experientes, treinadas para acompanhamento do projeto desde a implementação e instalação dos sistemas elétricos até as etapas de laudo e documentação.</p>
<h3><strong>O que é o laudo NR10 para serviço de adequação NR10</strong></h3>
<p>Nosso serviço de<strong> adequação NR10 </strong>oferece todo suporte para emitir seu relatório técnico das instalações com todas informações necessárias e devidamente analisadas por um engenheiro eletricista da MS Projetos. Adeque-se agora mesmo a Norma Regulamentadora 10 e garanta segurança para funcionários, colaboradores e usuários de seu espaço.</p>
<p>Para orçamentos, fale agora mesmo com um de nossos atendentes. Oferecemos propostas diferenciadas a custo benefício especial, com rapidez e agilidade.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>