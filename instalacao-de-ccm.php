<?php
    $title       = "Instalação de CCM";
    $description = "A instalação de CCM trás segurança pelo fato de integrar demais dispositivos em conjunto com o motor e realizar seu acionamento de forma simples e segura.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Se você procura por uma empresa grandemente qualificada para realizar a instalação de CCM encontrou o lugar ideal. A MS Projetos Industriais é uma empresa que atua dentro da engenharia elétrica e automação industrial com muita propriedade técnica oferecendo as melhores soluções. A sigla CCM refere-se a “centro de controle de motores” então podemos concluir que a instalação de CCM é a instalação de um mesmo dispositivo que controla os motores de todo o maquinário da indústria. Esse equipamento pode operar com motores de diversas frequências. Dentro do CCM existem diversos componentes como dispositivos de medição, controles lógicos programáveis (CLP), acionadores entre outros. A vantagem de realizar a instalação de CCM é realizar o acionamento e controle de todo o maquinário do ambiente de forma segura. Esse equipamento é utilizado desde 1950 e vem sofrendo atualizações conforme as necessidades aumentam e a tecnologia dentro do segmento elétrico avança. Muito utilizado em indústrias automotivas esse sistema e atualmente é possível encontrá-los em empresas de diversos segmentos. A instalação de CCM trás segurança pelo fato de integrar demais dispositivos em conjunto com o motor e realizar seu acionamento de forma simples e segura tendo em vista que existem diversos dispositivos de proteção para proteger seu equipamento e o utilizador.</p>
<h2>Solicite a instalação de CCM com a MS Projetos Industriais</h2>
<p>Nossa empresa atua com a instalação de CCM há mais de 15 anos e possui grande propriedade para realizar esse serviço com qualidade. Nossa missão é criar soluções em engenharia elétrica que garantam excelência, apoiadas na competência e no aprimoramento tecnológico buscando sempre agir de forma ética e sustentável. Para alcançar esses resultados, contamos com uma equipe de alta qualidade e conhecimento técnico adquirido nas principais instituições de ensino dentro do nosso segmento. Trabalhamos com a visão de elevar nossa importância nos próximos anos, nos tornando destaque dentre as maiores fornecedoras de serviços de engenharia e montagens industriais no cenário nacional.</p>
<h3>Saiba mais sobre a instalação de CCM</h3>
<p>Para desenvolver um projeto e realizar o orçamento da instalação de CCM preencha as informações solicitadas na aba “contato” e aguarde que nossa equipe entrará em contato para desenvolver o seu projeto. Caso tenha dúvidas ou queira saber sobre alguns dos serviços prestados por nossa empresa entre em contato e seja atendido por um especialista para te auxiliar da melhor maneira possível. </p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>