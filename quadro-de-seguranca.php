<?php
    $title       = "Quadro de Segurança";
    $description = "Conheça a importância do quadro de segurança. A MS Projetos Industriais realiza instalação e outros serviços para segurança de sua empresa.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>O <strong>quadro de segurança</strong> tem como função garantir que as máquinas e equipamentos tenham proteção em um ambiente seja ele industrial, residencial ou comercial. As redes elétricas precisam desse equipamento para evitar acidentes, como sobrecargas elétricas ou curtos circuitos.</p>
<p>Portanto, investir no <strong>quadro de segurança </strong>para sua empresa, evita falhas que prejudicarão tanto o local como os trabalhadores. Para isso, conheça a MS: empresa especializada em projetos industriais.</p>
<h2>Garanta proteção para sua empresa com <strong>quadro de segurança</strong></h2>
<p>Um <strong>quadro de segurança </strong>é comporto por proteções de rápida atuação, garantindo maior segurança a todo sistema e evitando choques elétricos, acidentes e falhas que poderia causar incêndios e danos ao local.</p>
<p>O setor elétrico é um dos segmentos que mais merecem atenção, pois esses acidentes são comuns em ambientes em que o sistema elétrico não é adequadamente instalado. É fundamental que na montagem sejam usados dispositivos de alta tecnologia para bom desempenho, diminuição de gastos e máximo aproveitamento do <strong>quadro de segurança.</strong></p>
<p>Por isso, nosso serviço com<strong> quadro de segurança</strong> irá proteger seu sistema elétrico, levando segurança e controle. O setor que mais utiliza <strong>quadro de segurança</strong> é o industrial. Nele são diversas as particularidades para instalação e utilização, por isso, é indicada a contratação de um serviço especializado como a MS.</p>
<p>No entanto, seu uso pode ser obrigatório também em segmentos como comércios de lojas, shoppings, hospitais, e assim por diante. A MS garante qualidade no serviço de montagem e instalação, execução das normas técnicas e profissionais especializados.</p>
<h3>Conheça a MS Projetos Industriais</h3>
<p>Temos como diferencial uma equipe de colaboradores especializada e altamente qualificada para cada um de nossos serviços. Formada por profissionais experientes, os funcionários passam por processos constantes de capacitação. Além disso, recebem orientação para atuarem sempre de acordo com normas técnicas, tendências de segmento e tecnologia moderna, oferecendo o melhor atendimento na área.</p>
<p>Localizada em Minas Gerais, nossa empresa se destaca por um serviço de qualidade que atende requisitos como eficiência, ótimo custo benefício, cumprimento de prazos, qualidade de materiais, e assim por diante.</p>
<p>Trabalhamos com serviço de montagem, instalação e fabricação de <strong>quadro de segurança</strong>, e utilizamos soluções integradas de engenharia e montagem industrial para atender as expectativas de nossos clientes, oferecendo consultoria e gerenciamento de projetos.</p>
<p>Esse mercado possui grande abrangência e necessita de configuração adequada em cada projeto. Para que seja feita de maneira correta, sua empresa deve procurar um serviço especializado como a MS.</p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>