<?php
    $title       = "empresa de Energia Solar Rio de Janeiro";
    $description = "Serviços especializados em painéis solares é com nossa empresa de energia solar Rio de Janeiro, contrate a MS Solar para serviços de Engenharia Elétrica.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Contrate a MS Solar como a sua<strong> empresa de energia solar Rio de Janeiro</strong>. A MS Solar é uma empresa do grupo MSque trabalha com o segmento de geração, distribuição, instalação e compartilhamento de sistemas de energia fotovoltaica. Com total versatilidade, usufrua de energia renovável de fonte solar, proporcionando redução incrível em contas de energia elétrica além de ser uma fonte ecológica e não poluente de energia.</p>
<p>Desde o processo de prospecção de imóveis e terras para implantação de sistema completos de geração de energia fotovoltaica, até a instalação e posicionamento dos painéis, a MS Solar atua com mão de obra especializada e todo profissionalismo de uma <strong>empresa de energia solar Rio de Janeiro</strong> que trabalha com excelência. Faça desde a construção de sistemas, implantação e inclusive, comercialização de energia.</p>
<p>Há mais de 15 anos atuando no mercado nacional com elaboração e gestão de projetos, automação, montagem e instalação a nível industrial, a nossa<strong> empresa de energia solar Rio de Janeiro </strong>possui experiência com painéis elétricos. Conhecida anteriormente MS Electric, a MS Solar é uma empresa do grupo MS do segmento de energia solar e trabalha no desenvolvimento de soluções inteligentes e integradas com tecnologia para sua residência, empresa, indústria, entre outros.</p>
<p>Entre os diversos benefícios de contratar os serviços de instalação de sistemas de geração fotovoltaica conosco são: geração de energia limpa e sustentável, proteção contra a inflação de distribuidoras de energia elétrica, retorno garantido do investimento, valorização do seu imóvel, economia de até 95% em sua conta de energia elétrica além de muito mais disponível em nossa <strong>empresa de energia solar Rio de Janeiro</strong>.</p>
<h2><strong>Custo benefício investindo nossa empresa de energia solar Rio de Janeiro</strong></h2>
<p>O melhor custo benefício do mercado nacional. A MS Solar é uma<strong> empresa de energia solar Rio de Janeiro</strong> com profissionais certificados e experientes, contrate nossos serviços completos para regulamentação, projetos e instalação de sistemas de geração de energia elétrica proveniente da energia solar. Consulte-nos.</p>
<h3><strong>Encontre serviços especializados em nossa empresa de energia solar Rio de Janeiro</strong></h3>
<p>Aqui você pode fazer desde prospecção de terras para instalação de sistemas, projetos, estudos de viabilidade, dimensionamento de sistema, até análise de dados, comercialização de energia, manutenção. Oferecemos suporte completo no acompanhamento pós venda. Serviços especializados em painéis solares é com nossa<strong> empresa de energia solar Rio de Janeiro, MS Solar</strong>. Contrate a MS Solar para serviços profissionais para toda área de energia fotovoltaica e Engenharia Elétrica.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>