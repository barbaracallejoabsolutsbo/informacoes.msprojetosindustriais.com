<?php
    $title       = "Instalação de Painéis Elétricos";
    $description = "A principal importância da instalação de painéis elétricos é receber e distribuir corretamente a energia para os pontos de alimentação de uma empresa ou indústria.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Se você procura por uma empresa que realize a venda e instalação de painéis elétricos com agilidade e qualidade encontrou o lugar certo. A MS Projetos Industriais é uma empresa que atua dentro da engenharia elétrica e automação industrial com muita propriedade técnica oferecendo as melhores soluções. Nossa empresa presta atendimento para grandes nomes do mercado que podem ser conferidos através da aba “portfólio” disponível em nosso site. Conheça um pouco mais dos atendimentos já realizados para ter a certeza que a MS Projetos Industriais é a empresa certa para prestar o serviço que vai te auxiliar. Nossa empresa realiza a fabricação e instalação de painéis elétricos com qualidade e agilidade. A principal importância da instalação de painéis elétricos é receber e distribuir corretamente a energia para os pontos de alimentação de uma empresa ou indústria. Além da instalação de painéis elétricos atuamos com instalações industriais em geral, montagem de quadros elétricos, montagem de subestação, gestão de projetos, implementação de automação industrial e muito mais. Nossa missão é criar soluções em engenharia elétrica que garantam excelência, apoiadas na competência e no aprimoramento tecnológico buscando sempre agir de forma ética e sustentável.</p>
<h2>Solicite agora mesmo a instalação de painéis elétricos </h2>
<p>Ao realizar a instalação de painéis elétricos você garante que seus equipamentos estejam seguros e não recebam sobrecarga de energia com a má distribuição. Além disso, esse equipamento atua impedindo a perda de corrente gerando a má funcionalidade dos equipamentos. Trabalhamos com a visão de elevar nossa importância nos próximos anos, nos tornando destaque dentre as maiores fornecedoras de serviços de engenharia e montagens industriais no cenário nacional. A engenharia elétrica é uma das mais importantes vertentes da engenharia tendo em vista a influência geral que a administração desse setor tem na vida dos populares.</p>
<h3>Saiba mais sobre a instalação de painéis elétricos </h3>
<p>Para realizar um orçamento de compra e instalação de painéis elétricos  basta ir até a aba “contato” disponível em nosso site, preencher todos os campos fornecendo o máximo de informação e aguardar o contato de nossa equipe. Caso você tenha quaisquer dúvidas sobre nossos produtos ou serviços entre em contato e seja atendido por um especialista para te auxiliar da melhor maneira possível. Atuamos seguindo todas as normas técnicas e ambientais compreendendo que uma de nossas principais funções é atuar de forma sustentável preservando o ambiente para as futuras gerações. </p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>