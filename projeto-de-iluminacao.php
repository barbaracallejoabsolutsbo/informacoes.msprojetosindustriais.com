<?php
    $title       = "Projeto de Iluminação";
    $description = "Nosso projeto de iluminação é realizado por profissionais experientes. A MS Projetos Industriais garante qualidade em todo o processo.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>A MS Projetos Industriais desenvolve soluções junto ao cliente proporcionando apoio técnico durante a consultoria e gerenciamento de projetos. Um de nossos serviços é a elaboração do <strong>projeto de iluminação</strong> para sua indústria ou comércio.</p>
<p>Um <strong>projeto de iluminação</strong> é aquele que irá harmonizar o ambiente com luz natural e artificial de maneira a atingir um conforto visual sem perder a funcionalidade e garantindo uma economia de energia elétrica.</p>
<p>Para sua eficiência, é preciso entender a quantidade de luz necessária para o ambiente, e isso é conquistado através da experiência de profissionais técnicos especializados. Assim, pensar na iluminação de um ambiente de forma antecipada permite um bom <strong>projeto de iluminação</strong> e agrega valor ao espaço.</p>
<h2>Benefícios de um <strong>projeto de iluminação</strong></h2>
<p>Um <strong>projeto de iluminação</strong> proporciona economia de energia elétrica, seja em ambientes comerciais, residenciais ou industriais. Nossa empresa está preparada para atender sua demanda, oferecendo consultoria e gerenciamento durante o projeto.</p>
<p>Para isso, nossos profissionais experientes analisam corretamente cada fator e garantem um ótimo planejamento. O <strong>projeto de iluminação</strong> não se trata apenas de aparência e beleza, mas sim de eficiência e funcionalidade. Essa análise garante que o ambiente esteja plenamente adequado para sua função, levando maior amplitude e valorizando o espaço.</p>
<p>O erro de não considerar fatores que influenciam na luminosidade e em seu aproveitamento ideal, pode levar a um gasto excessivo de energia. Um projeto como esse irá evitar que os ambientes estejam mal iluminados ou com iluminação exagerada, ou seja, esse serviço planeja corretamente a iluminação do seu espaço.</p>
<p>Por isso, se você necessita de um serviço especializado em <strong>projeto de iluminação </strong>conheça a MS Projetos Industriais.</p>
<h3>MS Projetos Industriais e nossa área de projetos</h3>
<p>Localizada na cidade de Contagem, atendemos todo o estado de MG e expandimos em território nacional. Garantimos serviços de qualidade que atendem as expectativas de nossos clientes, proporcionando ótimo custo benefício, qualidade nos materiais, cumprimentos de prazos, e muito mais.</p>
<p>Nossos profissionais se atentam em pontos específicos para então melhor oferecer o resultado ao cliente, garantindo um excelente <strong>projeto de iluminação</strong>. O setor elétrico é um dos segmentos que mais necessitam de atenção para sejam evitados acidentes comuns. Esse cuidado é garantido na contratação de uma empresa de confiança.</p>
<p>A MS trabalha com soluções integradas que vão desde instalações e montagens industriais, até consultoria e gestão de projetos<strong>.</strong> Atendemos o setor elétrico e industrial através de uma equipe de profissionais especializados.</p>


                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>