<?php
    $title       = "Laudo de Aterramento e SPDA";
    $description = "O laudo de aterramento e SPDA é um documento que afirma que o local foi vistoriado e aprovado segundo as normas técnicas e de segurança exigidas.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Se você procura por uma empresa que realize a avaliação com emissão de laudo de aterramento e SPDA encontrou o lugar certo. A MS Projetos Industriais é uma empresa que atua dentro da engenharia elétrica e automação industrial prestando serviços e comercializando produtos de alta qualidade. Nossa empresa oferece diversos serviços de avaliação e laudo de segurança para garantir que o funcionamento de sua empresa ocorra respeitando todas as normas necessárias. Atuamos dentro da engenharia prestando diversos serviços além da emissão de laudo de aterramento e SPDA como manutenção em subestação, termografia elétrica, emissão de laudo (NR-10, NR-12, ICMS), fabricação de painéis elétricos e muito mais. Estamos há mais de 15 anos no mercado prestando atendimento para o setor publico e privado com qualidade, transparência, ética e compromisso. O laudo de aterramento e SPDA é um documento que afirma que o local foi vistoriado e aprovado segundo as normas técnicas e de segurança exigidas sobre proteção contra descargas atmosféricas. O laudo de aterramento e SPDA é emitido após a avaliação técnica de um profissional habilitado e credenciado para realizar a mesma. Durante sua inspeção o profissional verifica diversos fatores que em conjunto são aprovados ou não seguindo as exigências das normas técnicas de segurança. </p>
<h2>O melhor lugar para solicitar avaliação com emissão de laudo de aterramento e SPDA</h2>
<p>Por estar há muito tempo no mercado realizando a avaliação com laudo de aterramento e SPDA contamos com uma grande experiência que faz total diferença no senso crítico necessário para realizar as avaliações. Atuamos seguindo todas as normas técnicas e ambientais compreendendo que uma de nossas principais funções é atuar de forma sustentável preservando o ambiente para as futuras gerações. Garanta a segurança do seu ambiente e assegure a integridade de seus colaboradores, de seu maquinário e de sua empresa no geral. </p>
<h3>Saiba mais sobre a avaliação com emissão de laudo de aterramento e SPDA</h3>
<p>Para saber mais sobre a avaliação e emissão de laudo de aterramento e SPDA ou quaisquer outros serviços entre em contato, seja auxiliado por um especialista para te atender da melhor maneira possível. Trabalhamos com a visão de elevar nossa importância nos próximos anos, nos tornando destaque dentre as maiores fornecedoras de serviços de engenharia e montagens industriais no cenário nacional. Atuamos prezando valores como ética, respeito, trabalho em equipe, compromisso, responsabilidade ambiental e sustentável.</p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>