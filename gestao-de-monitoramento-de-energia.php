<?php
    $title       = "Gestão de Monitoramento de Energia";
    $description = "A gestão e monitoramento de energia pode indicar totalmente os pontos que mais consomem energia em sua empresa e propor soluções para essa redução.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Se você precisa de um sistema de gestão e monitoramento de energia para realizar o mapeamento de seu consumo mensal de energia, encontrou o lugar ideal para lhe auxiliar. A MS Projetos Industriais é uma empresa altamente conceituada que atua dentro dos ramos de automação industrial e engenharia civil. Contamos com uma equipe técnica de profissionais que possuem formação acadêmica nas melhores instituições de ensino dentro de nosso segmento. Além da gestão e monitoramento de energia, oferecemos diversos serviços como gerenciamento de energia, instalações elétricas, montagem de máquinas industriais, venda e instalação de painéis elétricos, inspeções, emissão de laudo e muito mais. Atuamos seguindo todas as normas técnicas e ambientais compreendendo que uma de nossas principais funções é atuar de forma sustentável preservando o ambiente para as futuras gerações. Não procure por gestão e monitoramento de energia sem antes consultar as soluções que somente a MS Projetos Industriais pode oferecer para você. Para realizar um orçamento de gestão e monitoramento de energia vá até a aba “contato” preencha o formulário cedendo o máximo de informações possíveis sobre as suas necessidades e aguarde o contato de nossa equipe. </p>
<h2>A melhor gestão e monitoramento de energia está a poucos cliques de você</h2>
<p>A gestão e monitoramento de energia pode indicar totalmente os pontos que mais consomem energia em sua empresa e propor soluções para essa redução de forma que não impacte o trabalho realizado em seu ambiente. Nossa empresa está há mais de 15 anos no mercado realizando diversos atendimentos para o setor público e privado. Consulte a aba “portfólio” disponível em nosso site para conhecer as empresas de grande nome que fazem parte de nossa cartela de clientes. A missão da nossa empresa é criar soluções em engenharia elétrica que garantam excelência, apoiadas na competência e no aprimoramento tecnológico buscando sempre agir de forma ética e sustentável.</p>
<h3>Saiba mais sobre o monitoramento de energia</h3>
<p>Para saber mais sobre o monitoramento de energia ou quaisquer outros serviços disponíveis em nosso catálogo entre em contato e seja prontamente auxiliado por um especialista para te atender da melhor maneira possível. Trabalhamos com a visão de elevar nossa importância nos próximos anos, nos tornando destaque dentre as maiores fornecedoras de serviços de engenharia e montagens industriais no cenário nacional. Atuamos com agilidade, qualidade e compromisso em todos os atendimentos realizados.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>