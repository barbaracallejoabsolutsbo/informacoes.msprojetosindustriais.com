<?php
    $title       = "Projeto Luminotécnico";
    $description = "Nosso projeto luminotécnico é realizado por profissionais experientes. A MS Projetos Industriais garante qualidade em todos os serviços.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>O <strong>projeto luminotécnico</strong> é aquele que irá harmonizar seu ambiente com luz natural e artificial de maneira que se atinja um conforto visual, funcionalidade e economia. Pensar na iluminação de um ambiente de forma antecipada permite um bom <strong>projeto luminotécnico </strong>e agrega valor ao espaço.</p>
<p>Para sua eficiência, é preciso entender a quantidade de luz necessária para o ambiente, e isso é conquistado através da experiência de profissionais técnicos especializados.</p>
<h2>Conheça a MS e nossa área de projetos</h2>
<p>A MS Projetos Industriais desenvolve soluções em parceria com o cliente, proporcionando apoio técnico na consultoria e no gerenciamento de projetos. Um de nossos serviços é a elaboração do <strong>projeto luminotécnico.</strong></p>
<p>Nossos profissionais se atentam em pontos específicos para então melhor oferecer o resultado final ao cliente. Alguns dos aspectos considerados são: a altura do pé direto, as cores da parede e do piso, se o ambiente é externo ou interno, a função destinada ao ambiente, a potência das lâmpadas, entre outros.</p>
<p>A MS Projetos Industriais trabalha com soluções integradas que vão desde instalações e montagens industriais, até consultoria e gestão de projetos. Buscamos atender o setor elétrico e industrial através de uma equipe de profissionais especializados.</p>
<h3>Vantagens de um <strong>projeto luminotécnico</strong></h3>
<p>O <strong>projeto luminotécnico</strong> não se trata de aparência e beleza, mas sim de eficiência e funcionalidade. Um projeto como esse irá evitar que os ambientes estejam mal iluminados ou com iluminação exagerada, ou seja, esse serviço planeja corretamente a iluminação do seu espaço.</p>
<p>Essa análise garante que o ambiente esteja plenamente adequado para sua função, levando maior amplitude, valorizando móveis e todo o espaço.  O erro de não considerar fatores que influenciam na luminosidade e em seu aproveitamento ideal, pode levar a um gasto excessivo de energia.</p>
<p>Assim, um <strong>projeto luminotécnico </strong>beneficia a economia de energia elétrica, seja em ambientes comerciais, residenciais, e assim por diante.  Para isso, nossos profissionais experientes analisam corretamente cada fator e garantem um ótimo planejamento e um excelente <strong>projeto luminotécnico</strong>.</p>
<p>Localizada na cidade de Contagem, atendemos todo o estado de Minas Gerais e expandimos em território nacional. Nos destacamos por serviços de qualidade que atendem as expectativas de nossos clientes ao proporcionar ótimo custo benefício, qualidade nos materiais, cumprimentos de prazos, e muito mais.</p>
<p>Nossa empresa está preparada para atender sua demanda, oferecendo consultoria e gerenciamento nos projetos. Por isso, se você procura um serviço especializado em <strong>projetos luminotécnicos </strong>conte com a MS Projetos Industriais.</p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>