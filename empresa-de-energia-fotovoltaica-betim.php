<?php
    $title       = "empresa de Energia Fotovoltaica Betim";
    $description = "Entre as diversas vantagens em contratar a MS Solar como sua empresa de energia fotovoltaica Betim, estão os diversos de serviços para energia solar. ";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Conheça a MS Solar, uma<strong> empresa de energia fotovoltaica Betim </strong>do grupo MS Projetos que atua com implementação, geração de energia, suporte total para projetos, documentação e instalação de sistemas de captação de energia solar e sua distribuição.</p>
<p>Especializada em serviços de Engenharia Elétrica, a MS Projetos atua no ramo desde 1992 oferecendo soluções modernas e eficazes para sistemas elétricos. Desde a criação de quadros elétricos de distribuição, quadros de comando, quadros de sinalização e etc., oferecemos diversas soluções para shoppings, hotéis, estações de tratamento de água e esgoto, indústrias e muito mais. A MS Solar é uma <strong>empresa de energia fotovoltaica Betim </strong>que oferece serviços que vão desde a comercialização de energia solar até projetos de implementação residencial, comercial e industrial.</p>
<p>Diversos projetos e soluções podem ser encontrados conosco. É possível desenvolver projetos para compartilhar energia solar, para implementação em condomínios, residências, indústrias, empresas, centros comerciais e muito mais. Com eficiência e agilidade, entregamos projetos em prazos excelentes com toda certificação e regulamentação junto às concessionárias de distribuição de energia da sua região. Contrate os serviços especializados de nossa <strong>empresa de energia fotovoltaica Betim</strong> e usufrua de economia de energia de até 95% na sua conta de energia elétrica.</p>
<p>Com fornecedores de alto padrão, nossa <strong>empresa de energia fotovoltaica Betim</strong> oferece equipamentos homologados e aprovados pelo Inmetro. Desde a prospecção de terras rurais para implementação dos painéis solares até o estudo de rentabilidade, a MS Solar oferece todo o suporte profissional. Fale conosco agora mesmo e contrate nossos serviços para todo o país.</p>
<h2><strong>Serviços disponíveis na MS Solar, empresa de energia fotovoltaica Betim</strong></h2>
<p>Além de serviços convencionais de uma empresa de engenharia elétrica,<strong> a MS Solar, empresa de energia fotovoltaica Betim</strong>, oferece os melhores equipamentos e mão de obra especializada para instalação, manutenção e regulamentação de sistemas fotovoltaicos para captação e uso de energia solar.</p>
<h3><strong>Conheça mais sobre a MS Solar, a maior empresa de energia fotovoltaica Betim</strong></h3>
<p>Entre as diversas vantagens em contratar a MS Solar como sua <strong>empresa de energia fotovoltaica Betim</strong>, estão a possibilidades diversas de serviços para sistemas de energia solar. Os benefícios da utilização de energia solar vão desde economia a benefícios ecológicos. Reconhecida no cenário mineiro e nacional, a MS Solar do grupo MS desenvolve desde o projeto até a instalação com todo suporte pós venda. Fale conosco agora mesmo para orçamentos e mais informações.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>