<?php
    $title       = "Painéis Elétricos Betim";
    $description = "Os painéis elétricos Belo Horizonte são equipamentos dos sistemas elétricos responsáveis por receber a eletricidade, converter de forma segura e redistribuir.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Se você procura por uma empresa que realize a fabricação, venda e instalação de painéis elétricos Betim encontrou o lugar certo. A MS Projetos Industriais é uma empresa que projeta, fabrica, comercializa e instala painéis elétricos Betim com qualidade, agilidade e segurança. Se tratando de equipamentos de tensão elevada é muito importante seguir todos os protocolos de segurança para lidar com esse tipo de produto. Trabalhamos com a visão de elevar nossa importância nos próximos anos, nos tornando destaque dentre as maiores fornecedoras de serviços de engenharia e montagens industriais no cenário nacional. A MS Projetos Industriais está há mais de 15 anos no mercado prestando serviços dentro da engenharia elétrica e automação industrial. Nossa empresa atua com a missão de criar soluções em engenharia elétrica que garantam excelência, apoiadas na competência e no aprimoramento tecnológico buscando sempre agir de forma ética e sustentável. Os painéis elétricos Belo Horizonte são equipamentos dos sistemas elétricos responsáveis por receber a eletricidade, converter de forma segura e redistribuir para pequenos circuitos individuais ou pontos de alimentação. Se estiver precisando realizar uma compra de painéis elétricos Belo Horizonte não vá para outro lugar sem antes conhecer as opções com ótimo custo x benefício que somente uma empresa referência pode oferecer para seus clientes. </p>
<h2>As melhores opções de painéis elétricos Belo Horizonte estão aqui</h2>
<p>Nossos painéis elétricos Belo Horizonte são fabricados com componentes de alta tecnologia fabricados por empresas de muita credibilidade e confiabilidade. Além disso, contamos com uma equipe de técnica de desenvolvedores e projetistas altamente qualificados que passaram por instruções nas melhores instituições de ensino do país dentro de nosso segmento. Antes de realizar sua compra é importante ressaltar que a manutenção desse equipamento seja feita periodicamente para prevenir falhas e acidentes que podem ir de pequenos choques até grandes incêndios em caso de falha grave por falta de manutenção do equipamento.</p>
<h3>Saiba mais sobre nossos painéis elétricos Belo Horizonte</h3>
<p>Para saber mais sobre os painéis elétricos Belo Horizonte ou quaisquer outros produtos, ou serviços disponíveis em nosso catálogo entre em contato e seja atendido por um especialista para te auxiliar da melhor maneira possível. Estando há muito tempo no mercado, somos uma das empresas mais recomendadas do segmento e já realizamos diversos atendimentos de muito destaque propondo soluções incríveis e assertivas para solucionar os problemas elétricos de nossos clientes.</p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>