<?php
    $title       = "Adequação NR12";
    $description = "O processo de adequação NR12 analisa e avalia diversos fatores como limites das máquinas; dados e descrições da máquina; entre outros, fale com a MS Projetos.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>A necessidade de um serviço de<strong> adequação NR12</strong>, normalmente, acontece quando um fiscal do ministério do trabalho aponta alguma incompatibilidade nas condições de trabalho em relação à operação de máquinas ou a condição destes equipamentos a serem operados, devendo adequá-los aos parâmetros legais regulamentares desta norma.</p>
<p>A <strong>adequação NR12</strong> é de extrema importância, garantindo que os operadores de maquinários industriais não fiquem expostos a equipamentos que causem algum risco ou perigo. Diversos fatores devem ser levados em consideração para que todos os padrões estejam de acordo com a Norma Regulamentadora número 12, e para que isso aconteça, contrate agora mesmo os serviços completos da MS Projetos.</p>
<p>Além do serviço para <strong>adequação NR12,</strong> com a MS Projetos você encontra desde a produção de quadros elétricos de distribuição, comando, entre outras funções, para os mais diversificados ramos que vão desde aeroportos e shoppings a estações de tratamento de esgoto, por exemplo. Conheça mais sobre os serviços disponíveis com a MS Projetos.</p>
<p>Desenvolvida por uma equipe técnica preparada e treinada para <strong>adequação NR12, </strong>orçamos seu projeto de acordo com todos os pontos envolvidos. Fale conosco e tenha atendimento personalizado para esclarecimento de dúvidas, orçamentos e muito mais.</p>
<h2><strong>Pontos necessários para a adequação NR12</strong></h2>
<p>Realizada sempre de acordo com a implantação da NR12, a<strong> adequação NR12 </strong>é feita por uma equipe profissional preparada para o serviço. Com um Engenheiro Eletricista são feitos desde a orientação para saúde e preservação dos trabalhadores até o armazenamento, instalação e operação dos equipamentos, bem como equipamentos de proteção envolvidos e toda prevenção de acidentes por meio de treinamentos e instruções.</p>
<h3><strong>Itens avaliados para adequação NR12</strong></h3>
<p>O processo de<strong> adequação NR12 </strong>analisa e avalia diversos fatores, entre eles estão: Limites das máquinas; dados e descrições da máquina; inventário da máquina; análise de riscos de acidentes; classificação de riscos; categoria de segurança da máquina; qualificação profissional dos operadores; sinalização; sistema de segurança e emergência; manual, projeto e demais documentações; lista de prioridades; ações e recomendações necessárias.</p>
<p>É sempre recomendável realizar o Laudo NR12 quando houver a fabricação de uma máquina nova, analisando os riscos existentes atuais e os que eventualmente apareçam futuramente quando houver qualquer alteração estrutural ou de segurança.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>