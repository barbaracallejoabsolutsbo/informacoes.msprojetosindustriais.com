<?php
    $title       = "empresa de Projetos Elétricos Contagem";
    $description = "Conheça mais sobre serviços especiais para todo segmento de energia solar com a MS Solar. Contrate a MS como sua empresa de projetos elétricos Contagem.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Localizada em Minas Gerais, a MS é uma<strong> empresa de projetos elétricos Contagem</strong> que conta com serviços especializados para sistemas elétricos e projetos elétricos em geral. Oferecemos serviços de consultoria e gestão de projetos, executados sempre por equipes qualificadas e com larga experiência, além de suporte técnico para operação, criação e gestão de projetos elétricos.</p>
<p>Contrate a MS como sua<strong> empresa de projetos elétricos Contagem.</strong> Para todo setor elétrico e industrial, oferecemos soluções inteligentes para diversas aplicações da Engenharia. Trabalhamos com equipes experientes e qualificadas nos serviços de estudos de curto circuito; elaboração de projetos CEMIG; projeto elétrico conceitual, básico e executivo; alteração de demanda contratada; laudos de aterramento, adequação a NR10 e NR12.</p>
<p>Com larga experiência, nossa<strong> empresa de projetos elétricos Contagem</strong> também trabalha com elaboração e aprovação de Projetos CEMIG. Os projetos envolvidos abrangem diversas etapas que podem ser encontradas em nosso site. Desde o projeto de entrada de energia conforme ND’s até a parametrização de relé de proteção CEMIG, encontre suporte completo para realizar toda parte burocrática do processo de projetos CEMIG do início ao fim.</p>
<p>Encontre também, com nossa<strong> empresa de projetos elétricos Contagem</strong>, serviços de termografia/termovisão elétrica em painéis elétrico e subestação, laudos para aterramentos, estudos de curto circuito, adequação e implantação de normas regulamentadoras, entre outros serviços de consultoria e gestão de projetos para sistemas elétricos.</p>
<h2><strong>Outros segmentos de nossa empresa de projetos elétricos Contagem</strong></h2>
<p>Conheça serviços de energia solar completos com a MS,<strong> empresa de projetos elétricos Contagem. </strong>Com serviços completos para instalação e regularização de sistemas de energia fotovoltaica, a MS Solar, do grupo MS, garante serviços especializados que geram benefícios como economia, ecologia e sustentabilidade por longos anos. Conheça mais sobre serviços especiais para todo segmento de energia solar com a MS Solar.</p>
<h3><strong>Sustentabilidade e ecologia com a MS, empresa de projetos elétricos Contagem </strong></h3>
<p>Garantindo excelência em serviços prestados em nossa <strong>empresa de projetos elétricos Contagem</strong>, prezamos sempre pela ecologia e sustentabilidade. Além de serviços especializados em energia solar, todos os serviços e demandas solicitadas são atendidas pensando totalmente na preservação do meio ambiente. Atendendo indústrias da área siderúrgica, mineração, saneamento, geração de energia, etc, nossos profissionais são experientes e qualificados. Consulte-nos para orçamentos.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>