<?php
    $title       = "Estudo de Seletividade";
    $description = "O estudo de seletividade é a metodologia perfeita para determinar os equipamentos necessários para preservar seu sistema elétrico assim como os usuários. ";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>O <strong>estudo de seletividade </strong>é uma metodologia para literalmente selecionar os equipamentos de proteção elétrica necessário para o seu sistema elétrico. Se você procura por uma empresa credenciada e altamente qualificada para realizar o <strong>estudo de seletividade </strong>para o seu caso encontrou o lugar certo. A MS Projetos Industriais é uma empresa que atua na engenharia elétrica e automação industrial. São mais de 15 anos atuando em diversas regiões do Brasil e prestando serviço para grandes nomes do mercado. A missão de nossa empresa é criar soluções em engenharia elétrica que garantam excelência, apoiadas na competência e no aprimoramento tecnológico buscando sempre agir de forma ética e sustentável. Além do <strong>estudo de seletividade </strong>oferecemos diversos atendimentos de alta qualidade e tecnicamente impecáveis como manutenção em subestação, termografia elétrica, emissão de laudo (NR-10, NR-12, aterramento, ICMS), fabricação de painéis elétricos e muito mais. Por tantas qualidades que nossa grande equipe possui a MS Projetos Industriais é o melhor lugar para você solicitar um <strong>estudo de seletividade</strong> de qualidade para proteger o sistema elétrico do seu ambiente industrial.</p>
<h2><strong>Mas porque o estudo de seletividade é um serviço tão importante?</strong></h2>
<p>O <strong>estudo de seletividade </strong>é a metodologia perfeita para determinar os equipamentos necessários para preservar seu sistema elétrico assim como os usuários. Normalmente a manutenção corretiva ou substituição de aparelhos elétricos é incontavelmente mais desvantajosa do que investir em proteção e manutenções preventivas constantes. Cuide do patrimônio de sua empresa e da integridade de seus funcionários com os componentes mais tecnológicos e eficazes do mercado para proteger o seu sistema elétrico.</p>
<h3><strong>Saiba mais sobre o estudo de seletividade</strong></h3>
<p>Para realizar o <strong>estudo de seletividade </strong>é necessária a presença de um engenheiro elétrico especializado para obter os melhores resultados. Caso você tenha dúvidas sobre quaisquer um de nossos serviços entre em contato e seja atendido por um especialista altamente qualificado para te auxiliar da melhor maneira possível. Trabalhamos com a visão de elevar nossa importância nos próximos anos, nos tornando destaque dentre as maiores fornecedoras de serviços de engenharia e montagens industriais no cenário nacional. Para realizar seu primeiro contato ou para saber um pouco mais sobre nossa empresa utilize os canais de comunicação disponíveis em nosso site. Atuamos seguindo todas as normas técnicas e ambientais compreendendo que uma de nossos principais funções é atuar de forma sustentável preservando o ambiente para as futuras gerações.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>