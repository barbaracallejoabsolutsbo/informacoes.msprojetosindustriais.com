<?php
    $title       = "empresa de Energia Solar Contagem";
    $description = "Contrate a MS Solar como sua empresa de energia solar Contagem e usufrua de serviços para todo o processo de instalação de sistemas de energia solar.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Conheça a maior<strong> empresa de energia solar Contagem</strong>, MS Solar. Pensando em soluções completas para você, oferecemos suporte completo desde a elaboração de projetos completos para sistemas de energia fotovoltaica até todas as etapas de regulamentação e homologação pelas concessionárias fornecedoras de energia na sua região. Saiba mais sobre nossos serviços acessando nosso site.</p>
<p>Entre diversos benefícios que você pode encontrar investindo em energia sustentável e renovável, o principal fator é a economia, e, com a MS Solar você adquire sistemas completos para captação de energia fotovoltaica e todos serviços de instalação de uma <strong>empresa de energia solar Contagem</strong>. Contrate nossos serviços e garanta já seu próprio sistema de energia solar.</p>
<p>Pensando em cada detalhe do processo de regulamentação, projeto completo para posição, angulação dos painéis para otimizar, e , de forma inteligente, gerando economia de até 95% na sua conta de energia elétrica, os serviços de nossa <strong>empresa de energia solar Contagem </strong>garantem custo benefício garantido e retorno do investimento em até 7 anos após a implementação do sistema.</p>
<p>Com durabilidade de até 25 anos, e em alguns casos até 30 anos, os painéis fotovoltaicos não perdem eficiência de captação durante esse período, garantindo benefícios por prolongadas décadas e, após algum prazo, praticamente zerando sua conta de energia elétrica. Entre sistemas off-grid, on-grid e híbridos, a nossa<strong> empresa de energia solar Contagem</strong> oferece serviços de todas as etapas envolvidas na implementação de qualquer um destes tipos de sistemas.</p>
<h2><strong>Principais motivos para investir com nossa empresa de energia solar Contagem</strong></h2>
<p>Os benefícios em implementar seu sistema de energia fotovoltaico com nossa <strong>empresa de energia solar Contagem </strong>são diversos, enfatizando o custo benefício, prazo de entrega e comprometimento de nossos profissionais. Garantindo economia para conta de energia elétrica e com garantia de retorno do investimento em alguns anos, é um tipo de energia sustentável, renovável e ecológica que é tendência no mercado.</p>
<h3><strong>Contrate os serviços da MS Solar, a maior empresa de energia solar Contagem</strong></h3>
<p>Contrate a MS Solar como sua <strong>empresa de energia solar Contagem </strong>e garanta serviços especializados para todo o processo de instalação e regulamentação de sistemas de captação e uso de energia solar.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>