<?php
    $title       = "empresa de Energia Solar Nova Lima";
    $description = "Com o melhor custo benefício do mercado regional e nacional, a MS Solar é uma empresa de energia solar Nova Lima que oferece serviços para todo o país.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Uma<strong> empresa de energia solar Nova Lima</strong>, normalmente, oferece serviços para obtenção de sistemas para captação de energia solar. Com a MS Solar não é diferente. Especializada em todos os serviços que estão envolvidos nas etapas para implementação de sistemas de energia fotovoltaica, a MS Solar trabalha com mão de obra especializada e equipamentos certificados e homologados.</p>
<p>Existem diversos modelos de solução para implementação de energia solar. Empresas sustentáveis que utilizam de energia sustentável costumam ocupar as primeiras colocações de empresas sustentáveis e ecológicas. Invista em energia solar com a MS Solar, <strong>empresa de energia solar Nova Lima. </strong>Para o ramo industrial, comercial e doméstico, serviços com mão de obra especializada para projeto e instalação completa dos sistemas de energia fotovoltaica.</p>
<p>Soluções inteligentes e modernas para geração de energia sustentável você encontra com nossa <strong>empresa de energia solar Nova Lima</strong>, MS Solar. Do grupo MS, desde 1992 oferecendo serviços para área de sistemas elétricos, hoje abrangemos todos assuntos ligados a Engenharia Elétrica. Acesse nosso site, conheça nosso catálogo de serviços e contrate a MS para todo suporte para sistemas elétricos e geração de energia fotovoltaica.</p>
<p>Para instalação, investimento para geração de energia solar, sistemas completos, contrate nossa <strong>empresa de energia solar Nova Lima</strong>. Com a MS Solar é muito fácil investir em soluções para energia solar fotovoltaica. Com diversos relatos de relação entre custo e benefício, sistemas de geração podem devolver o investimento inicial em até 7 anos, oferecendo retorno durante todo o tempo de durabilidade dos equipamentos, que pode exceder os 25 anos sem perda de eficiência.</p>
<h2><strong>Custo benefício é com a MS Solar, empresa de energia solar Nova Lima</strong></h2>
<p>Com o melhor custo benefício do mercado regional e nacional, a MS Solar é uma<strong> empresa de energia solar Nova Lima </strong>que oferece serviços para todo o país. Com profissionais certificados e experientes, contrate nossos serviços completos para projetos, regulamentação e instalação de sistemas de geração de energia fotovoltaica.</p>
<h3><strong>Serviços especializados de empresa de energia solar Nova Lima</strong></h3>
<p>Com serviços que vão desde a instalação, projeto, dimensionamento de terras, prospecção, regulamentação e homologação com a distribuidora de energia da sua região, entre outros, nossa <strong>empresa de energia solar Nova Lima</strong>, MS Solar, oferece profissionais certificados e mão de obra especializada para todos os processos e etapas da implementação.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>