<?php
    $title       = "empresa de Energia Fotovoltaica Contagem";
    $description = "Não apenas uma empresa de energia fotovoltaica Contagem, a MS Projetos tem serviços para diversos tipos de sistemas elétricos, confira.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Encontre diversos serviços para energia solar com a MS Projetos. Somos uma<strong> empresa de energia fotovoltaica Contagem</strong>, Minas Gerais. Oferecemos projetos residenciais, comerciais ou industriais que garantem a diminuição de até 95% da conta de energia de casas ou empresas com funcionamento por pelo menos 25 anos sem perda de eficiência.</p>
<p>Não apenas uma <strong>empresa de energia fotovoltaica Contagem</strong>, mas também oferecemos serviços de Engenharia Elétrica para diversos tipos de sistemas elétricos, desde residenciais até industriais. Desde 1992 no mercado, oferecemos serviços que atendem desde empresas privadas a órgãos públicos. Prazo, qualidade, custo e eficiência são nossos diferenciais. Conheça mais sobre a MS Projetos e seus serviços para a área de energia solar.</p>
<p>Com vasto portfólio, acesse nosso site e conheça mais sobre nossos principais trabalhos. Nossa <strong>empresa de energia fotovoltaica Contagem </strong>oferece serviços para implementação de sistemas de painéis solares fotovoltaicos com durabilidade de 25 anos, em alguns casos chegando até 30 anos. O projeto é feito com a MS Projetos e apresentado à concessionária de energia da região para aprovação e liberação da instalação do sistema.</p>
<p>Após instalado, a concessionária da região faz a troca do medidor convencional para um medidor bidirecional, que irá medir e computar toda energia consumida e injetada no sistema da rede. Os equipamentos utilizados por nossa <strong>empresa de</strong> <strong>energia fotovoltaica Contagem</strong> são homologados pela concessionária, certificados pelo Inmetro e com aprovação prévia do projeto e especificações técnicas pela concessionária.</p>
<h2><strong>Empresa de energia fotovoltaica Contagem e o funcionamento do sistema</strong></h2>
<p>A energia gerada pelos equipamentos de captação fotovoltaica é utilizada para alimentação dos equipamentos de sua residência, por exemplo. O sistema pode ser on-grid, off-grid ou híbrido. No caso do on-grid, quando há queda de energia, por interagir diretamente com a concessionária de luz, a energia oferecida pelo sistema também é cortada para manutenções seguras do sistema pela concessionária. Em caso de sistemas híbridos ou off-grid, por contarem com baterias, o sistema poderá continuar sendo alimentado pela energia armazenada nas baterias. Contrate agora mesmo os serviços de nossa <strong>empresa de energia fotovoltaica Contagem</strong>.</p>
<h3><strong>Escolha a MS Projetos como sua empresa de energia fotovoltaica Contagem</strong></h3>
<p>A energia solar vem crescendo exponencialmente no Brasil, constantemente sendo procurada, é um segmento que conta com diversos modelos de equipamentos, sistemas, projetos, formas de instalação e inclusive manutenção. Com a MS Projetos você encontra todo suporte de uma empresa de<strong> energia fotovoltaica Contagem.</strong></p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>