<?php
    $title       = "empresa de Energia Solar Belo Horizonte";
    $description = "A MS Solar é uma empresa de energia solar Belo Horizonte, do grupo MS, que trabalha com diversos segmentos da Engenharia Elétrica. ";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Uma <strong>empresa de energia solar Belo Horizonte </strong>pode oferecer diversos serviços para o ramo de energia solar. A MS Solar, basicamente, é uma empresa que realiza serviços que abrangem todas as etapas para a realização da instalação de sistemas fotovoltaicos. Estes serviços vão desde a vistoria e prospecção de terras e imóveis até a homologação do projeto junto a empresa de distribuição de energia da sua região.</p>
<p>Para indústrias, residências, empresas, condomínios, shoppings, entre outros, os sistemas e serviços oferecidos por nossa <strong>empresa de energia solar Belo Horizonte</strong> são realizados por profissionais certificados e engenheiros eletricistas. Com formas simples, econômicas e ágeis, a MS Solar oferece soluções que dão benefícios de até 95% em redução da sua conta de energia elétrica fornecida pela concessionária de energia da sua região.</p>
<p>Para montar uma <strong>empresa de energia solar Belo Horizonte </strong>a MS Solar também oferece todo o suporte. É possível obter suporte para todas as etapas para criação de condomínios solares, fazendas de energia solar, entre outras opções. Toda energia gerada por estes sistemas poderá ser usada e ser compartilhada em redes de acordo com o solicitado.</p>
<p>Outros serviços que podem ser encontrados com nossa <strong>empresa de energia solar Belo Horizonte, </strong>MS Projetos, tangem desde a medições, projetos de melhorias e gestão integral de energia, até projetos, laudos, adequações para normas regulamentadoras e muito mais.</p>
<h2><strong>Porque escolher a MS Solar como sua empresa de energia solar Belo Horizonte</strong></h2>
<p>A MS Solar é uma <strong>empresa de energia solar Belo Horizonte,</strong> do grupo MS,que trabalha com diversos segmentos da Engenharia Elétrica. Além de serviços de laudos, documentação e projetos para empresas e indústrias, é possível encontrar suporte total para todas as etapas envolvidas na instalação, geração e consumo de energia solar. Escolha a MS Solar como a sua empresa e solicite seu orçamento falando com nosso atendimento.</p>
<h3><strong>Empresa de energia solar Belo Horizonte que oferece soluções para você</strong></h3>
<p>Com a energia solar é possível usufruir de energia sustentável, ecológica e renovável. Durante o dia, os módulos solares captam a luz solar e geram energia. Essa energia passa pelo inversor e é convertida nas características da rede para uso para alimentar qualquer aparelho da casa. Se nem toda a energia for consumida, o excesso de energia é liberado na rede, gerando seus créditos de energia. A MS Solar pode fornecer uma variedade de serviços. Contrate os serviços da MS Solar,<strong> empresa de energia solar Belo Horizonte. </strong></p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>