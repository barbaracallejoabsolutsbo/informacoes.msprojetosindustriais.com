<?php
    $title       = "Painel Fotovoltaico";
    $description = "O painel fotovoltaico é o responsável por absorver a energia solar em sistemas de captação e geração de energia solar. Esse equipamento fica posicionado.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Se você procura por uma empresa de alta qualidade que fabrique, entregue e instale o painel fotovoltaico que você precisa encontrou o lugar ideal. A MS Projetos Industriais está há mais de 15 anos no mercado prestando serviços dentro da engenharia elétrica e automação industrial. Nosso painel fotovoltaico é construído com tecnologia de ponta e conta com os melhores componentes fabricados por empresas de alta confiabilidade e credibilidade no mercado. Optamos por essa escolha para garantir os melhores resultados de forma segura e assertiva. Contamos com uma equipe de projetistas, desenvolvimento e produção extremamente qualificados para realizar a produção completa do painel fotovoltaico. A missão da nossa empresa é criar soluções em engenharia elétrica que garantam excelência, apoiadas na competência e no aprimoramento tecnológico buscando sempre agir de forma ética e sustentável. O painel fotovoltaico é o responsável por absorver a energia solar em sistemas de captação e geração de energia solar. Esse equipamento fica posicionado em áreas abertas onde exista iluminação solar para captar os raios ultravioletas e encaminhar para que o sistema realize a conversão em energia elétrica. Atualmente esse tipo de equipamento é muito procurado tendo em vista o crescimento e propagação de métodos de energia com fonte renovável. </p>
<h2>O painel fotovoltaico que você precisa está aqui</h2>
<p>Sempre que precisar de um painel fotovoltaico não deixe de conferir as opções que somente a MS Projetos Industriais tem em seu catálogo de produtos. São quase duas décadas dedicadas ao desenvolvimento e avanço tecnológico dentro da engenharia elétrica e automação industrial. Trabalhamos com a visão de elevar nossa importância nos próximos anos, nos tornando destaque dentre as maiores fornecedoras de serviços de engenharia e montagens industriais no cenário nacional.</p>
<h3>Saiba mais sobre o painel fotovoltaico da MS Projetos Industriais</h3>
<p>Para saber mais sobre o painel fotovoltaico ou quaisquer outros produtos, ou serviços disponíveis em nosso catálogo entre em contato e seja atendido por um especialista para te auxiliar da melhor maneira possível. Atuamos seguindo todas as normas técnicas e ambientais compreendendo que uma de nossas principais funções é atuar de forma sustentável preservando o ambiente para as futuras gerações. Tudo que você precisa sobre engenharia elétrica e automação industrial em um só lugar. Nossa empresa atua tanto prestando serviço para o setor privado quanto para o setor público com qualidade, ética, respeito e compromisso.</p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>