<?php
    $title       = "empresa de Projetos Elétricos";
    $description = "A MS Projetos, empresa de projetos elétricos, atende demandas do setor elétrico e industrial oferecendo soluções integradas para áreas diversas da Engenharia.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Localizada em Contagem, Minas Gerais, a MS é uma<strong> empresa de projetos elétricos </strong>que oferece serviços especializados para toda área de sistemas elétricos. Atendendo desde residências até indústrias, oferecemos serviços de consultoria e gestão de projetos. Executados sempre por equipes qualificadas, o apoio técnico para operação, criação e gestão de projetos elétricos, mecânicos ou de automação é completamente especializado.</p>
<p>A MS Projetos,<strong> empresa de projetos elétricos</strong>, atende demandas do setor elétrico e industrial oferecendo soluções integradas para áreas diversas da Engenharia. Trabalhamos com equipes qualificadas e experientes nos serviços como: Projeto elétrico conceitual, básico e executivo; estudos de curto circuito; elaboração de projetos CEMIG e alteração de demanda contratada; laudos de aterramento, adequação a NR10 e NR12, entre outros serviços.</p>
<p>Nossa <strong>empresa de projetos elétricos </strong>busca sempre atender as demandas de nossos clientes com excelência, cumprimento de prazos, eficiência e qualidade, além de baixo custo e profissionalismo. Desde 1992, anteriormente conhecida como MS Eletric, desenvolvemos nestes anos de empresa diversos projetos para instituições privadas e órgãos públicos em todo o país.</p>
<p>A <strong>empresa de projetos elétricos </strong>oferece todas as etapas envolvidas no desenvolvimento de projeto conceitual, básico e executivo. Entre as etapas, serviços como dimensionamento de equipamentos elétricos, elaboração de projeto de rede elétrica (aérea, subterrânea, entre outros), projetos de aterramento, estudo de cargas e demandas, diagramas unifilar, trifilar, multifilar, entre outros serviços especializados para toda área de engenharia elétrica, contrate a MS para desenvolver seu projeto.</p>
<h2><strong>Conheça a empresa de projetos elétricos, MS Projetos</strong></h2>
<p>Conheça mais sobre a MS Projetos, a<strong> empresa de projetos elétricos</strong> que oferece serviços para soluções inteligentes e integradas para projetos elétricos. Atuamos também na área de eficiência energética e sustentabilidade, prestando serviços de diagnóstico, medições, análise, gestão de projetos, monitoramento e controle, medição e verificação, e certificação para implementação de padrões de gerenciamento de energia.</p>
<h3><strong>Contrate nossa empresa de projetos elétricos para seu projeto</strong></h3>
<p>Fale com nosso atendimento e contrate a melhor<strong> empresa de projetos elétricos </strong>de Contagem, Minas Gerais. Oferecendo serviços para todo o país, solicite seu orçamento por e-mail, telefone ou WhatsApp e usufrua do melhor custo benefício em serviços especializados para projetos de sistemas elétricos.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>