<?php
    $title       = "Quadros Elétricos";
    $description = "Conheça a importância dos quadros elétricos. A MS Projetos Industriais realiza instalação e outros serviços para segurança de sua empresa.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Os <strong>quadros elétricos</strong> também chamados quadros de distribuição de energia elétrica. São parte importante de todo comércio, residência ou indústria que precise de energia elétrica para funcionamento.</p>
<p>Sendo um equipamento que recebe energia de uma ou mais fontes de alimentação, o quadro elétrico distribui essa energia conforme programado. Seu objetivo, então, é automatizar comandos elétricos e aperfeiçoar a execução das tarefas no sistema de controle e distribuição de energia elétrica.</p>
<h2>Conheça alguns modelos de <strong>quadros elétricos</strong></h2>
<p>Incluindo aparelhos que fazem o controle de dispositivos mecânicos, cada modelo é projetado para um arranjo específico de equipamentos. Assim, conhecer os <strong>quadros elétricos</strong> é importante e contratar uma empresa especializada é essencial, para definir qual deles atenderá melhor suas necessidades.</p>
<p>Os <strong>quadros elétricos</strong> podem variar conforme suas necessidades e funções, entre eles, estão:</p>
<ul>
<li>         Quadro geral de baixa tensão;</li>
<li>         Centro de controle de motores;</li>
<li>         Painel elétrico de distribuição e sub-distribuição;</li>
<li>         Painel elétrico de comando e controle;</li>
<li>         Painel elétrico para acionamentos, entre outros.</li>
</ul>
<p>Para projetar e instalar um quadro elétrico é necessária a inspeção de itens como o envelhecimento acelerado de componentes, vida útil desses componentes, riscos de incêndio, limites de temperatura, falhas prematuras, e assim por diante. Por isso, a contratação de uma empresa especializada como a MS é irá facilitar todo o processo.</p>
<h3>Importância dos <strong>quadros elétricos </strong>e serviço especializado</h3>
<p>Os <strong>quadros elétricos</strong> levam segurança e evita descargas elétricas e curtos circuitos que poderiam causas danos ao local e às pessoas. Esses equipamentos são compostos por diversos acessórios e sistemas, como fusíveis, unidades de proteção, disjuntores, barramento elétrico, entre outros. Para realizar sua função de distribuição ele deve ser construído seguindo regulamentações técnicas.</p>
<p>Formada por profissionais experientes, a MS Projetos Industriais está localizada no estado de Minas Gerais. Nossos funcionários passam por processos constantes de capacitação. Além disso, recebem orientação para atuarem sempre de acordo com normas técnicas, tendências de segmento e tecnologia moderna, oferecendo o melhor atendimento na área.</p>
<p>Trabalhamos com serviço de montagem, instalação e fabricação de <strong>quadros elétricos,</strong> utilizando soluções integradas de engenharia e montagem industrial para atender as expectativas de nossos clientes, oferecendo consultoria e gerenciamento de projetos.</p>
<p>Temos como diferencial uma equipe de colaboradores especializada e altamente qualificada. O mercado de <strong>quadros elétricos</strong> possui grande abrangência e necessita de configuração adequada em cada projeto. Para correto manuseio e operação durante o trabalho sua empresa deve procurar um serviço especializado como a MS.</p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>