<?php
    $title       = "Montagem Eletromecânica";
    $description = "Para realizar a montagem elétrica industrial, disponibilizamos profissionais altamente capacitados que foram instruídos nas melhores instituições de ensino dentro de.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Se você procura por uma empresa qualificada e recomendada para realizar a montagem eletromecânica que você precisa, encontrou o lugar certo. A MS Projetos Industriais é uma empresa que atua dentro da engenharia elétrica e automação industrial com qualidade e propriedade técnica. São mais de 15 anos prestando serviço para empresas privadas e para o setor público com ética, transparência, compromisso e qualidade. A missão da nossa empresa é criar soluções em engenharia elétrica que garantam excelência, apoiadas na competência e no aprimoramento tecnológico buscando sempre agir de forma ética e sustentável. A montagem eletromecânica é o serviço destinado para a montagem de máquinas, equipamentos e derivados que incluam funcionamento elétrico e mecânico em sua estrutura. Para realizar a montagem eletromecânica disponibilizamos profissionais altamente qualificados que vão até o local com as ferramentas necessárias somente para realizar a montagem e instalação de equipamentos eletromecânicos. Realizar a montagem eletromecânica é essencial para o bom funcionamento e segurança do equipamento para que funcione de forma ideal. Ao funcionar de forma ideal além de realizar suas funções com qualidade o equipamento oferece segurança para o utilizador garantindo assim sua integridade física. </p>
<h2>Realizamos montagem eletromecânica com qualidade e agilidade</h2>
<p>Nossa empresa possui uma vasta experiência em prestar a montagem eletromecânica, por isso realiza esse serviço com muita qualidade, propriedade técnica, segurança e agilidade. Atuamos seguindo todas as normas técnicas e ambientais compreendendo que uma de nossas principais funções é atuar de forma sustentável preservando o ambiente para as futuras gerações. Ao realizar a montagem correta você garante o funcionamento e usabilidade de todo o potencial do seu equipamento sem prejudicar sua vida útil. Atendemos grandes nomes da indústria que podem ser consultados através da aba “portfolio” disponível em nosso site. Conte com uma empresa de alta qualidade e confiabilidade para realizar as instalações que você precisa, entre em contato conosco agora mesmo. </p>

<h3>Saiba mais sobre a montagem eletromecânica da MS Projetos Industriais</h3>
<p>Para saber mais sobre o serviço de montagem eletromecânica ou de quaisquer outros oferecidos pela MS Projetos Industriais entre em contato e seja atendido por um especialista para te auxiliar da melhor maneira possível. Trabalhamos com a visão de elevar nossa importância nos próximos anos, nos tornando destaque dentre as maiores fornecedoras de serviços de engenharia e montagens industriais no cenário nacional. Prezamos por metodologias ecológicas e sustentáveis para preservar o planeta e seus recursos naturais de forma com que se renovem constantemente para o abastecimento das futuras gerações.</p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>