<?php
    $title       = "Projeto CEMIG";
    $description = "Elaboração de projeto CEMIG é um dos serviços disponibilizados pela MS Projetos. Conte com a nossa equipe de profissionais experientes.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>A MS Projetos Industriais trabalha com soluções integradas que vão desde instalações e montagens industriais, até consultoria e gestão de <strong>projeto CEMIG.</strong> Nossa área de projetos busca atender o setor elétrico e industrial por meio de uma equipe de profissionais especializados.</p>
<p>Nossa equipe desenvolve soluções em parceria com o cliente, proporcionando apoio técnico na consultoria e no gerenciamento de projetos. Um de nossos serviços é a elaboração e aprovação de <strong>projeto CEMIG.</strong></p>
<h2>Conheça a MS Projetos Industriais e nossos objetivos</h2>
<p>Sediada em Minas Gerais, na cidade de Contagem, nossa empresa vem se destacando por um serviço de qualidade que atende requisitos como eficiência, ótimo custo benefício, cumprimento de prazos, qualidade de materiais, e assim por diante.</p>
<p>Temos como missão disponibilizar soluções em engenharia que garantam excelência, agindo de forma ética e sustentável e utilizando aprimoramento tecnológico e competência. Para isso, trabalhamos com excelência no cumprimento das normas técnicas, na utilização de materiais de qualidade fornecidos no mercado e no atendimento aos clientes.</p>
<p>A MS Projetos Industriais está há mais de 15 anos no mercado, prestando serviço para órgãos públicos e empresas privadas. Com empreendedorismo, competência técnica e desenvolvimento de soluções ligadas à engenharia, estamos conquistando reconhecimento também na área de <strong>projeto CEMIG</strong>.</p>
<h3>Saiba mais sobre o <strong>projeto CEMIG</strong></h3>
<p>A Cemig é uma das principais fornecedoras de energia elétrica do Brasil. Só em Minas Gerais, possuem mais de 8,7 milhões de consumidores, divididos entre 774 municípios. Uma das facilidades que a MS Projetos oferece é a elaboração de <strong>projeto CEMIG</strong> de acordo com o padrão solicitado por eles.</p>
<p>Sempre que há uma nova ligação de energia elétrica, troca de titularidade e alteração de carga ou nível de tensão, a Cemig exige o cumprimento de uma série de normas para instalação, por isso o <strong>projeto CEMIG</strong> é importante. A alta tensão é extremamente perigosa, sendo muito arriscado para pessoas que não possuem domínio técnico.</p>
<p>A MS tem uma larga experiência em elaboração e aprovação de <strong>projeto CEMIG</strong>, os quais incluem:</p>
<ul>
<li>         Projeto de Padrão Cemig;</li>
<li>         Projeto Fotovoltaico Cemig;</li>
<li>         Homologação de Sistema Solar Fotovoltaico junto a CEMIG;</li>
<li>         Projeto de Rede de Distribuição CEMIG;</li>
<li>         Projeto de Entrada de Energia Conforme ND's;</li>
<li>         Subestação 13,8kV - 23,1kV - 34,5kV - 138kV;</li>
<li>         Parametrização de Relé de Proteção CEMIG;</li>
<li>         Coordenograma de Proteção e Seletividade CEMIG.</li>
</ul>
<p>Ao contratar nossos serviços, realizamos toda a parte burocrática do <strong>projeto CEMIG</strong> para que você tenha total segurança e transparência em sua atuação. O processo é garantido por nós do início ao fim.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>