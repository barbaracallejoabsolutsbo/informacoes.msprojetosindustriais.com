<?php
    $title       = "Fabricação de Quadro Elétricos";
    $description = "Nossa empresa atua com a fabricação de quadro elétrico há muito tempo por isso conta com uma grande experiência além da alta qualidade para realizar essa.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Se você procura por uma <strong>fabricação de quadro elétrico </strong>realizada totalmente em território nacional e com ótimos prazos encontrou o lugar ideal. A MS Projetos Industriais é uma empresa que atua dentro das áreas de engenharia elétrica e automação industrial fornecendo equipamentos, prestando serviços e fazendo a diferença de forma sustentável. Nossa empresa atua com a <strong>fabricação de quadro elétrico </strong>há muito tempo por isso conta com uma grande experiência além da alta qualidade para realizar essa fabricação. São mais de 15 anos atuando em diversas regiões do Brasil e prestando serviço para grandes nomes do mercado. A missão de nossa empresa é criar soluções em engenharia elétrica que garantam excelência, apoiadas na competência e no aprimoramento tecnológico buscando sempre agir de forma ética e sustentável. Além da <strong>fabricação de quadro elétrico </strong>prestamos diversos serviços como manutenção em subestação, termografia elétrica, emissão de laudo (NR-10, NR-12, aterramento, ICMS), fabricação de painéis elétricos e muito mais. A <strong>fabricação de quadro elétrico </strong>é realizada com componentes de alta qualidade fabricados por empresas de grande confiabilidade para garantir um bom e seguro funcionamento de nosso produto.</p>
<h2><strong>A melhor fabricação de quadro elétrico</strong></h2>
<p>Com quase duas décadas de atuação no mercado nossa empresa é altamente recomendada para a <strong>fabricação de quadro elétrico </strong>e demais produtos para engenharia elétrica e automação industrial. Contamos uma equipe de projetistas e desenvolvedores de alta qualidade técnica com formação nas principais instituições de ensino dentro de nosso segmento. Com o senso critico e analítico de nossos profissionais podemos de forma técnica e seguindo todas as normas para alcançar resultados incríveis com nossos produtos e serviços.</p>
<h3><strong>Saiba mais sobre a fabricação de quadro elétrico</strong></h3>
<p>Para saber mais sobre a <strong>fabricação de quadro elétrico </strong>ou quaisquer outros produtos e serviços realizados por nossa empresa entre em contato e seja atendido por um especialista para te auxiliar da melhor maneira possível. Prestamos serviços para empresas privadas e para o setor publico com ética, transparência, compromisso e qualidade. Atendemos grandes nomes da indústria que podem ser consultados através da aba “portfolio” disponível em nosso site. Trabalhamos com a visão de elevar nossa importância nos próximos anos, nos tornando destaque dentre as maiores fornecedoras de serviços de engenharia e montagens industriais no cenário nacional. Prezamos por metodologias ecológicas e sustentáveis para preservar o planeta e seus recursos naturais de forma com que se renovem constantemente para o abastecimento das futuras gerações.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>