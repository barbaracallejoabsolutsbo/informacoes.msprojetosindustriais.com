<?php
    $title       = "Diagrama Multifilar";
    $description = "O diagrama multifilar é um tipo de diagrama que representa condutores elétricos envolvidos num sistema que realiza conexão com componentes.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>O <strong>diagrama multifilar </strong>é um tipo de diagrama que representa condutores elétricos envolvidos num sistema que realiza conexão com componentes. Detalhando todas as conexões de um projeto elétrico, com este diagrama é possível visualizar toda a distribuição da carga elétrica bem como verificar todos os detalhes de cada conexão. Geralmente, o <strong>diagrama multifilar </strong>detalha dados sobre barramento, circuito terminal, aterramento, quadros associados à instalação e dados dos quadros. Serve principalmente para identificação e detalhamento de esquemas elétricos de painéis e motores para posteriores manutenções ou interpretações. Para instalações residenciais, por exemplo, normalmente se fazem diagramas unifilares, pelo nível de dificuldade.</p>
<p>A diferença entre o <strong>diagrama multifilar</strong> e o unifilar basicamente é que, no primeiro, você tem a representação em desenho dos condutores em si de forma detalhada, já no diagrama unifilar, você representa a quantidade de condutores e desenha uma única linha que representa o cabo acomodado, por exemplo, condutores elétricos passando por dentro de conduítes. Ambos diagramas possuem o mesmo objetivo, sendo o unifilar mais adequado para residências pelo grau de dificuldade de interpretação e de representação em multifilar.</p>
<p>O <strong>diagrama multifilar</strong> é recomendado para ligações diretas a motores ou painéis elétricos, já o diagrama unifilar para plantas baixas de residência, por exemplo. Mas existem ainda outros modelos de diagrama como o trifilar, para máquinas trifásicas e comandos elétricos. Todos estes modelos de diagrama podem ser encontrados com a MS Projetos. Fale conosco agora mesmo para orçamentos e mais informações.</p>
<h2><strong>Qual objetivo do diagrama multifilar</strong></h2>
<p>O<strong> diagrama multifilar, </strong>diferente do diagrama unifilar, é uma versão mais detalhada com a representação dos condutores e componentes em seu posicionamento correto. Serve para representar de forma precisa a instalação elétrica para posterior interpretação. Realizado por nossos profissionais engenheiros elétricos, é muito importante manter seu diagrama sempre atualizado para segurança de todos colaboradores e usuários do local.</p>
<h3><strong>Faça o diagrama multifilar com a MS Projetos</strong></h3>
<p>Obtenha seu<strong> diagrama multifilar </strong>com a MS Projetos, além de serviços de documentação e regulamentação de instalações elétricas, com a MS é possível criar projetos completos de painéis de energia fotovoltaica, painéis e quadros elétricos para empresas, shoppings, hotéis e muito mais. Fale com nosso atendimento agora mesmo e solicite seu orçamento.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>