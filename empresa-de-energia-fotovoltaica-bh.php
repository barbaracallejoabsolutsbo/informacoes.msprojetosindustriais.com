<?php
    $title       = "empresa de Energia Fotovoltaica BH";
    $description = "Nossa empresa de energia fotovoltaica BH oferece serviços de Engenharia Elétrica para todo o país, conheça a MS Solar e contrate-nos agora mesmo.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Conheça mais sobre a MS Projetos Industriais, a sua<strong> empresa de energia fotovoltaica BH </strong>e para todo o país. Oferecendo diversas soluções inteligentes para gestão, medição, documentação, regulamentação, entre outros serviços para sistemas elétricos. Para sua residência, indústria, empresa, condomínio, shoppings, hotéis, entre outros.</p>
<p>Outros serviços que podem ser encontrados em nossa <strong>empresa de energia fotovoltaica BH </strong>é o de implementação de painéis solares para captação de energia fotovoltaica para diversos fins, seja comercial, residencial ou industrial. Obtendo eficiência e economia de até 95% no uso da energia de concessionárias de luz, a energia solar é renovável, ecológica e tem se tornado tendência em grandes empresas e investimentos.</p>
<p>Faça todo o seu projeto de captação de energia solar com a MS Projetos, <strong>empresa de energia fotovoltaica BH</strong>. Com agilidade, profissionalismo e qualidade, trabalhamos desde a instalação até a regulamentação do sistema com sua concessionária de luz de sua região. Aqui você encontra serviços exclusivos: estudo de viabilidade técnica financeira, prospecção de terras, incorporação de imóveis, fornecimento de soluções completas, especificações técnicas para geração de energia em qualquer tipo de imóvel, instalação, projetos, acompanhamento pós venda e muito mais. Consulte nosso site para mais informações e fale conosco para orçamentos de serviços exclusivos que você só encontra em nossa <strong>empresa de energia fotovoltaica BH.</strong></p>
<h2><strong>Contrate os serviços de incorporação com nossa empresa de energia fotovoltaica BH</strong></h2>
<p>Fale com nosso atendimento para orçamentos, mais informações e dúvidas.<strong> Nossa empresa de energia fotovoltaica BH </strong>oferece serviços de Engenharia Elétrica para todo o país. Serviços de instalação, adaptação, adequação, regulamentação, documentação de sistemas elétricos e muito mais.</p>
<h3><strong>Invista em sua startup com nossa empresa de energia fotovoltaica BH</strong></h3>
<p>Com nossa<strong> empresa de energia fotovoltaica BH </strong>você tem todo suporte para encontrar as melhores formas de comercializar energia solar fotovoltaica. Existem diversos modelos de projetos para aproveitamento e desenvolvimento de sistemas de captação em zonas rurais para serem compartilhados com diversos usuários de uma mesma rede. Alguns modelos como fazendas de energia solar, condomínios de energia solar, entre outros, criam possibilidades de geração de energia solar suficiente para alimentar e dar suporte a residências, indústrias e empresas.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>