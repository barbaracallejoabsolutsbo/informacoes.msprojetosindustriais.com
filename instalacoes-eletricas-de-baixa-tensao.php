<?php
    $title       = "Instalações Elétricas de Baixa Tensão";
    $description = "Não solicite instalações elétricas de baixa tensão em outro lugar sem antes consultar as opções de alta qualidade e preço justo da MS Projetos Industriais. 
";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Se você procura por uma empresa que realize instalações elétricas de baixa tensão seguindo todas as normas técnicas exigidas pelo regulamento da ABNT NBR-5410 encontrou o lugar certo. A MS Projetos Industriais possui uma vasta experiência em realizar instalações elétricas de baixa tensão, para prestar esse serviço contamos com profissionais altamente qualificados que foram instruídos nas melhores instituições de ensino dentro de nosso segmento. Nossa empresa é referência em qualidade dentro da engenharia elétrica e automação industrial. São mais de 15 anos prestando serviço para grandes nomes do setor privado e para o setor público também. Além de realizar as instalações elétricas de baixa tensão também atuamos com instalações elétricas no geral, instalações eletromecânica, inspeção com laudo, montagem e instalação de subestação, monitoramento de energia e muito mais. Para conhecer os trabalhos prestados por nossa empresa e os grandes nomes que atendemos dentro do Brasil confira a aba “portfólio” disponível em nosso site. Não solicite instalações elétricas de baixa tensão em outro lugar sem antes consultar as opções de alta qualidade e preço justo da MS Projetos Industriais. </p>
<h2>Os melhores serviços de instalações elétricas de baixa tensão estão aqui</h2>
<p>Realizamos as instalações elétricas de baixa tensão em todos os tipos de empresa, indústria, agronegócio e muito mais. Atuamos seguindo todas as normas técnicas e ambientais compreendendo que uma de nossas principais funções é atuar de forma sustentável preservando o ambiente para as futuras gerações. Em nossa empresa prezamos pelo trabalho em equipe realizado com ética, transparência, competência, compromisso, responsabilidade ambiental e sustentabilidade. A missão da nossa empresa é criar soluções em engenharia elétrica que garantam excelência, apoiadas na competência e no aprimoramento tecnológico buscando sempre agir de forma ética e sustentável.</p>
<h3>Saiba mais sobre nossos serviços de instalações elétricas de baixa tensão</h3>
<p>Para saber mais sobre os serviços de instalações elétricas de baixa tensão ou quaisquer outros ofertados por nossa empresa entre em contato e seja atendido por um especialista para te auxiliar da melhor maneira possível. Caso deseje fazer um orçamento preencha os campos através da aba “contato” fornecendo o máximo de informações possíveis para que nossos profissionais entrem em contato para auxiliar informando o seu orçamento e explicando as necessidades de intervenção para solucionar sua situação. Tudo que você precisa dentro da engenharia elétrica e automação industrial você encontra aqui. Conheça mais sobre nossos produtos e nossa empresa através de nosso site. </p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>