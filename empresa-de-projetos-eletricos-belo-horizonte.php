<?php
    $title       = "empresa de Projetos Elétricos Belo Horizonte";
    $description = "Com a MS Projetos Industriais você encontra serviços especializados de uma empresa de projetos elétricos Belo Horizonte, conheça mais.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Com a MS Projetos Industriais você encontra serviços especializados de uma <strong>empresa de</strong> <strong>projetos elétricos Belo Horizonte. </strong>Desde 1992 atuando no Brasil, anteriormente conhecida como MS Eletric, desenvolvemos projetos elétricos para instituições privadas e órgãos públicos em todo o país.</p>
<p>Nossa <strong>empresa de</strong> <strong>projetos elétricos Belo Horizonte</strong>, atendendo as demandas de nossos clientes com cumprimento de prazos, eficiência e qualidade, além de baixo custo e profissionalismo, oferece serviços completos para projetos elétricos. Acesse nosso site e conheça mais sobre os serviços que podem ser encontrados com a MS Projetos, que vão desde eficiência, montagens industriais até sustentabilidade com painéis solares.</p>
<p>Fale conosco e escolha a MS para serviços especializados. A <strong>empresa de</strong> <strong>projetos elétricos Belo Horizonte</strong> oferece todas as etapas envolvidas no desenvolvimento de projeto conceitual, básico e executivo. Entre os passos e serviços desenvolvidos é possível encontrar: projetos de aterramento, estudo de cargas e demandas, diagramas unifilar, trifilar, multifilar, dimensionamento de equipamentos elétricos, elaboração de projeto de rede elétrica (aérea, subterrânea, entre outros), entre outros serviços especializados para toda área de engenharia elétrica.</p>
<p>Também atuando na construção de painéis e quadros elétricos, a<strong> empresa de</strong> <strong>projetos elétricos Belo Horizonte </strong>oferece as melhores soluções de automação e distribuição elétrica para indústrias. Montamos conforme o projeto, dentro de normas internacionais e nacionais e seguindo critérios da Engenharia, com os melhores fornecedores de materiais como Schneider, Siemens, ABB, entre outros.</p>
<h2><strong>Coordenação e seletividade com a empresa de</strong> <strong>projetos elétricos Belo Horizonte</strong></h2>
<p>Normalmente os sistemas elétricos, por proteção, utilizam fusíveis, relés e disjuntores. Nossa <strong>empresa de</strong> <strong>projetos elétricos Belo Horizonte </strong>garante que seus equipamentos não sofram danos com projetos de seletividade de proteções, fazendo que quando curtos circuitos ocorram, afetem apenas o sistema de proteção.</p>
<h3><strong>Montagem industrial é na empresa de</strong> <strong>projetos elétricos Belo Horizonte, MS Projetos</strong></h3>
<p>A MS Projetos e Montagens Industriais é uma<strong> empresa de</strong> <strong>projetos elétricos Belo Horizonte </strong>que trabalha com diversas especialidades de serviços para empresas do setor de mineração, geração de energia, saneamento, entre outros. Trabalhamos com instalações elétricas de baixa e média tensão, adequações a normas regulamentadoras NR10 e NR12, instalação de nobreak, geradores, montagem e instalação de sistemas elétricos completos, automação e muito mais outros serviços disponíveis para consulta.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>