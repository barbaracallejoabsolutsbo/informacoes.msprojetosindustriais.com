<?php
    $title       = "Montagem de QGBT";
    $description = "Para realizar a montagem de QGBT disponibilizamos profissionais altamente qualificados para realizar o seu atendimento com segurança e qualidade. ";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Se você precisa realizar uma montagem de QGBT e procurar por uma empresa qualificada e recomendada para prestar esse serviço encontrou o lugar certo. A MS Projetos Industriais está há mais de 15 anos no mercado prestando serviços dentro da engenharia elétrica e automação industrial. Para realizar a montagem de QGBT disponibilizamos profissionais altamente qualificados para realizar o seu atendimento com segurança e qualidade. São quase duas décadas prestando serviço para empresas privadas e para o setor público com ética, transparência, compromisso e qualidade. Trabalhamos com a visão de elevar nossa importância nos próximos anos, nos tornando destaque dentre as maiores fornecedoras de serviços de engenharia e montagens industriais no cenário nacional. Para a segurança dos usuários é essencial que a montagem de QGBT seja realizada corretamente tendo em vista que esse equipamento é alimentado eletricamente com diversas tensões e correntes que podem ocasionar acidentes. Aproveite essa grande oportunidade de solicitar a montagem de QGBT com uma empresa altamente competente para garantir a segurança de seus equipamentos com a distribuição correta da energia para os circuitos individuais que os alimentam.</p>
<h2>Saiba mais sobre a montagem de QGBT</h2>
<p>A montagem de QGBT refere-se a montagem de um quadro geral de baixa tensão. Esse tipo de equipamento é responsável por receber a energia, converter para baixa tensão e redistribuir para pequenos circuitos ou pontos de alimentação. Realizar a montagem de maneira profissional é indispensável tendo em vista que o conhecimento necessário para realizar esse processo tem que ser extremamente afiado. Atuamos seguindo todas as normas técnicas e ambientais compreendendo que uma de nossas principais funções é atuar de forma sustentável preservando o ambiente para as futuras gerações. Realizamos todo o atendimento técnico necessário e se você precisar existem diversos outros serviços que prestamos para indústrias como manutenção em subestação, termografia elétrica, emissão de laudo (NR-10, NR-12, aterramento, ICMS), fabricação de painéis elétricos e muito mais. Tudo que você precisa sobre engenharia elétrica e automação industrial você encontra aqui na MS Projetos Industriais.</p>
<h3>Solicite aqui sua montagem de QGBT</h3>
<p>Para solicitar a montagem de QGBT vá até a aba “contato” e preencha todos os campos enviando o máximo de informações para que nossos profissionais possam avaliar os melhores recursos para a sua situação. Caso tenha dúvidas sobre esse ou quaisquer outros oferecidos por nossa empresa entre em contato e seja atendido por um especialista para te auxiliar da melhor maneira possível.</p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>