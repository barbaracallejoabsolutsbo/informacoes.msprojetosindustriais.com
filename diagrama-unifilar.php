<?php
    $title       = "Diagrama Unifilar";
    $description = "Com mão de obra especializada, a MS Projetos é a melhor opção para você realizar seu diagrama unifilar seja para residências, empresas, hotéis, entre outros.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Se você procura por uma empresa que faça<strong> diagrama unifilar </strong>a MS Projetos é sua melhor opção. Tão importante quanto qualquer outro tipo de diagrama, este tipo de diagrama normalmente é utilizado em plantas baixas de construções. Ele representa o esquema elétrico por desenhos técnicos, que combinados com símbolos definidos por normas, representam graficamente circuitos elétricos e eletrônicos.</p>
<p>Contamos com uma equipe técnica de profissionais experientes para desenvolver projetos e soluções completas para sistemas elétricos, assim como estudo e criação de <strong>diagrama unifilar</strong>. Contrate nossos serviços profissionais de engenharia elétrica de forma segura e responsável.</p>
<p>A MS Projetos é uma empresa especializada em engenharia elétrica, oferecendo soluções e serviços para lhe garantir desde projetos, documentação e adequação às normas regulatórias até sinalização e direcionamento da produção de quadros elétricos para hotéis, shopping centers, aeroportos e muito mais. Em nossas redes sociais você encontra fotos de serviços que vão além de serviço de esquema em <strong>diagrama unifilar</strong>.</p>
<p>Seja para área residencial, comercial, industrial ou outros, o <strong>diagrama unifilar</strong> é indispensável para representação de todos os pontos de conexão dos dispositivos e caminho dos condutores na planta, além de outros detalhes. Unifilar representa um fio, isso quer dizer que, todos os cabos são representados por símbolos sobre apenas um traço, normalmente este traço é a representação de um eletroduto ou caminho a que estes cabos são passados. Também serve para verificar rapidamente a quantidade de condutores em caminhos de eletrodutos, por exemplo. É o tipo de diagrama mais utilizado por profissionais da área de sistemas elétricos.</p>
<h2><strong>Diagrama unifilar completo e dentro das normas ABNT é com a MS Projetos</strong></h2>
<p>O<strong> diagrama unifilar </strong>deve ser feito por um profissional da área da engenharia elétrica. Este profissional representa através de um documento, símbolos e descrições e compõem o diagrama. Cada símbolo tem um significado diferente e para ter acesso a todos os símbolos basta consultar a norma NBR 5444 da ABNT. Apesar de serem inúmeros símbolos, os principais representados são: Eletrodutos, caixas de passagem, condutores, quadros de distribuição, interruptores, luminárias e tomadas.</p>
<h3><strong>Contrate a MS Projetos para realizar seu diagrama unifilar</strong></h3>
<p>Com mão de obra especializada, a MS Projetos é a melhor opção para você realizar seu <strong>diagrama unifilar </strong>seja para residências, empresas, centros comerciais, hotéis, entre outros<strong>. </strong>Em forma de documento, tenha todas descrições sobre sua instalação elétrica em forma de diagrama para interpretação futura.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>