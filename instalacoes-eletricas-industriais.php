<?php
    $title       = "Instalações Elétricas Industriais";
    $description = "Se forem realizadas más instalações elétricas industriais além de poder danificar o seu maquinário coloca em risco a integridade física dos frequentadores do ambiente.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Se você procura pelo melhor atendimento para realizar instalações elétricas industriais com qualidade e agilidade encontrou o lugar certo. A MS Projetos Industriais é uma empresa que realiza instalações elétricas industriais com muita propriedade técnica refletindo diretamente na alta qualidade de nossos serviços. Estamos há mais de 15 anos no mercado prestando serviço para empresas do setor privado e do setor público. Em nossa carteira de clientes contamos com grandes nomes que podem ser consultados da aba “portfolio” disponível em nosso site, assim como os serviços prestados para os mesmos. A realização das instalações elétricas industriais deve ser realizada por um técnico especializado com experiência para que saia tudo como planejado. Se forem realizadas más instalações elétricas industriais além de poder danificar o seu maquinário coloca em risco a integridade física dos frequentadores do ambiente por risco de curto circuito, incêndio, descarga de tensão, entre outros. Trabalhamos com a visão de elevar nossa importância nos próximos anos, nos tornando destaque dentre as maiores fornecedoras de serviços de engenharia e montagens industriais no cenário nacional. Para chegar a esse resultado prezamos valores como trabalho em equipe, transparência, ética, respeito, sustentabilidade e compromisso. </p>
<h2>Solicite aqui as instalações elétricas industriais que você precisa</h2>
<p>Na MS Projetos Industriais você encontra todos os tipos de instalações elétricas industriais que precisar. Somos altamente especializados para realizar esse tipo de atendimento com segurança, qualidade e agilidade. Contamos com uma equipe técnica formada pelas principais instituições de ensino dentro de nosso segmento. Prezamos muito pela qualidade e segurança que nosso trabalho oferece para nossos clientes. Proteja seu equipamento realizando as instalações corretas e preservando seu patrimônio. Solicite uma instalação com quem realmente tem propriedade para encarar qualquer situação dentro desse segmento. </p>
<h3>Saiba mais sobre as instalações elétricas industriais</h3>
<p>Caso você tenha alguma dúvida sobre as instalações elétricas industriais ou quaisquer outros serviços ofertados por nossa empresa entre em contato e seja atendido por um especialista para te auxiliar da melhor maneira possível. Atuamos seguindo todas as normas técnicas e ambientais compreendendo que uma de nossas principais funções é atuar de forma sustentável preservando o ambiente para as futuras gerações. São quase duas décadas de história prestando atendimento com excelência em todas as regiões do Brasil. Nos destacamos pela maneira qualitativa que prestamos nossos serviços e fabricamos nossos produtos. Tudo que você precisa dentro da engenharia elétrica e automação industrial você encontra aqui.</p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>