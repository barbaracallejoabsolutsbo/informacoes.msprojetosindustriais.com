<?php
    $title       = "Montagem e Instalações de  Painéis Elétricos";
    $description = "A montagem e instalações de painéis elétricos devem ser realizadas por profissionais habilitados para esse serviço e credenciados nos órgãos de fiscalização. ";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Se você está procurando por uma empresa altamente qualificada para prestar o serviço de montagem e instalações de painéis elétricos com segurança e agilidade encontrou o lugar certo para solicitar atendimento. A MS Projetos Industriais é uma empresa que atua dentro da engenharia elétrica e automação industrial fornecendo equipamentos, prestando serviços e fazendo a diferença de forma sustentável. Contamos com uma equipe altamente qualificada para realizar a montagem e instalações de painéis elétricos prestando todo o atendimento necessário para a sua situação. Os painéis elétricos são responsáveis por receber, converter e distribuir energia para circuitos individuais de acordo com as necessidades dos mesmos. A montagem e instalações de painéis elétricos devem ser realizadas por profissionais habilitados para esse serviço e credenciados nos órgãos de fiscalização de nosso segmento (CREA). A missão da nossa empresa é criar soluções em engenharia elétrica que garantam excelência, apoiadas na competência e no aprimoramento tecnológico buscando sempre agir de forma ética e sustentável. Sempre que você precisar de um serviço de montagem e instalações de painéis elétricos não vá procurar em outro lugar sem antes consultar as opções de alta qualidade e baixo custo que a MS Projetos Industriais pode oferecer para você. </p>
<h2>Solicite aqui o serviço de montagem e instalações de painéis elétricos</h2>
<p>Se não realizado corretamente o serviço de montagem e instalações de painéis elétricos pode danificar seus equipamentos e até mesmo colocar em risco a integridade física de seus colaboradores. Muitos acidentes que ocorrem por conta de eletricidade poderiam ser evitados com a utilização de metodologias corretas que seguem as normas técnicas de segurança exigidas para essas situações. Atuamos seguindo todas as normas técnicas e ambientais compreendendo que uma de nossas principais funções é atuar de forma sustentável preservando o ambiente para as futuras gerações. </p>
<h3>Saiba mais sobre o serviço de montagem e instalações de painéis elétricos</h3>
<p>Para saber mais sobre nosso serviço de montagem e instalações de painéis elétricos ou quaisquer outros oferecidos pela MS Projetos Industriais entre em contato e seja atendido por um especialista para te auxiliar da melhor maneira possível. Trabalhamos com a visão de elevar nossa importância nos próximos anos, nos tornando destaque dentre as maiores fornecedoras de serviços de engenharia e montagens industriais no cenário nacional. Tudo que você precisa sobre engenharia elétrica e automação industrial você encontra aqui. </p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>