<?php
    $title       = "Gerenciamento de Energia";
    $description = "O gerenciamento de energia é um serviço de consultoria que pode ser aplicado em grandes indústrias e empresas com o propósito de encontrar oportunidade.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Você sabe a grande diferença que o gerenciamento de energia pode causar dentro da sua empresa quando bem aplicado? Se você está procurando por uma empresa que ofereça o melhor atendimento para gerenciamento de energia através da disponibilização de profissionais tecnicamente muito habilitados encontrou o lugar certo. A MS Projetos Industriais é uma empresa que atua dentro dos segmentos de engenharia elétrica e automação industrial. Oferecemos diversos produtos e serviços com a missão de criar soluções em engenharia elétrica que garantam excelência, apoiadas na competência e no aprimoramento tecnológico buscando sempre agir de forma ética e sustentável. Contamos com grandes empresas do mercado privado e estatais dentre nossos clientes que podem ser conferidos através da aba “portfólio” disponível em nosso site. O gerenciamento de energia é um serviço de consultoria que pode ser aplicado em grandes indústrias e empresas com o propósito de encontrar oportunidade para diminuir o consumo e consequentemente o gasto com energia. Dentro do gerenciamento de energia diversos processos podem ser adotados para chegar a um resultado satisfatório que realmente faça diferença no consumo de energia. </p>
<h2>Conheça um pouco mais sobre nosso serviço de gerenciamento de energia</h2>
<p>A aplicação do serviço de gerenciamento de energia é realizada através da análise profissional de um engenheiro elétrico ou engenheiro de automação industrial. Através da análise criteriosa do especialista é possível encontrar pontos de grande consumo e alguns de médio a pequeno consumo que podem ser diminuídos ou extinguidos dependendo de cada situação. Após essa análise o profissional emite um parecer técnico com as melhores soluções de acordo com as constatações realizadas durante sua análise. Atuamos seguindo todas as normas técnicas e ambientais compreendendo que uma de nossas principais funções é atuar de forma sustentável preservando o ambiente para as futuras gerações. </p>
<h3>Solicite já o serviço de gerenciamento de energia da MS Projetos Industriais</h3>
<p>Agora que você já entendeu o que é e como é útil o gerenciamento de energia entre em contato agora mesmo e solicite um orçamento. Para saber mais sobre esse ou quaisquer outros produtos ou serviços oferecidos por nossa empresa entre em contato com nossa equipe técnica e seja atendido por um especialista para te auxiliar da melhor maneira possível. A MS Projetos Industriais é uma empresa bem avaliada dentro de seu segmento que marca sua atuação pelo domínio dos métodos e normas técnicas em cada atendimento oferecido. Nos preocupamos também em diminuir o impacto ambiental para preservar a vida de nosso planeta através de soluções sustentáveis. </p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>