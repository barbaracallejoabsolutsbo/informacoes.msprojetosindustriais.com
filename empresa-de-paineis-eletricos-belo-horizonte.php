<?php
    $title       = "empresa de Painéis Elétricos Belo Horizonte";
    $description = "A MS Painéis, empresa de painéis elétricos Belo Horizonte, desenvolve projetos completos por equipes técnicas experientes e competentes. ";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Atuando em todo o Brasil, a MS Projetos e Montagens Industriais trabalha com diversos serviços especializados para sistemas elétricos. Nossa<strong> empresa de painéis elétricos Belo Horizonte </strong>monta desde quadros elétricos conforme projeto, dentro de normas nacionais e internacionais até a produção de quadros e painéis de distribuição, comando e sinalização. Conheça mais sobre nossos serviços.</p>
<p>A MS Painéis, <strong>empresa de painéis elétricos Belo Horizonte</strong>, desenvolve projetos completos por equipes técnicas experientes. Acompanhando todas as etapas de montagem dos painéis elétricos, garantimos qualidade, agilidade e excelência em nossos serviços. Entre outros serviços que podem ser encontrados com a MS Projetos, conheça mais um pouco sobre o que pode ser encontrado conosco.</p>
<p>Com nossa <strong>empresa de painéis elétricos Belo Horizonte </strong>você encontra serviços exclusivos de manutenção completa para painéis e quadros elétricos, inclusive para reposição de peças, reforma, instalação e projetos do absoluto zero. Trabalhamos com produtos e serviços como: caixas sob medida, armários e quadros personalizados, entrada de energia de baixa tensão, quadros elétricos de média tensão, quadros para bombas de incêndio, saneamento, entre outros a consultar em nosso site.</p>
<p>Os serviços prestados pela<strong> empresa de painéis elétricos Belo Horizonte</strong>, MS Painéis, servem tanto para condomínios e residências quanto para indústrias, empresas e comercialização de energia. Consulte nosso atendimento para mais informações e suporte completo de profissionais especializados na área de sistemas elétricos, inclusive para indústrias.</p>
<h2><strong>Encontre serviços de painéis solares na empresa de painéis elétricos Belo Horizonte</strong></h2>
<p>A MS Painéis,<strong> empresa de painéis elétricos Belo Horizonte</strong>, faz parte do grupo MS, que também presta serviços para implementação de sistemas de painéis fotovoltaicos para captação da energia solar para uso doméstico, comercial, industrial, entre outros. Conheça mais sobre sistemas de painéis solares acessando nosso site.</p>
<h3><strong>Empresa de painéis elétricos Belo Horizonte que faz projeto e instalação</strong></h3>
<p>A <strong>empresa de painéis elétricos Belo Horizonte, </strong>MS Projetos, trabalha com todos os processos envolvidos na produção de quadros e painéis elétricos. Com a missão de oferecer soluções para todo o ramo de engenharia, oferecemos sempre serviços com ética e sustentabilidade. Trabalhamos com montagens e engenharia elétrica a nível industrial. Realizando seus serviços preservando o meio ambiente, desenvolvemos projetos que agem em prol da sustentabilidade. Entre os diversos segmentos de atuação, trabalhamos com projetos, energia solar, eficiência elétrica e montagens industriais.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>