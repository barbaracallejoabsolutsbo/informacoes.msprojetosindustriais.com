<?php
    $title       = "Instalações Elétricas de Media Tensão";
    $description = "Solicite as instalações elétricas de média tensão da MS Projetos Industriais e fique tranquilo quanto a funcionalidade e segurança de seu equipamento.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Se você procura por uma empresa que realize as instalações elétricas de média tensão com qualidade seguindo todas as exigências da norma ABNT NBR-14039 encontrou o lugar certo. Seja bem-vindo a MS Projetos Industriais, somos uma empresa que atua dentro dos segmentos de engenharia elétrica e automação industrial com alta propriedade técnica e muita qualidade em prestar atendimento. Estamos há quase duas décadas no mercado prestando instalações elétricas de média tensão e diversos outros serviços que auxiliam as empresas tecnicamente com muita segurança e qualidade. Nossa empresa atua com a missão de criar soluções em engenharia elétrica que garantam excelência, apoiadas na competência e no aprimoramento tecnológico buscando sempre agir de forma ética e sustentável. Para realizar as instalações elétricas de média tensão contamos com uma equipe técnica de alta qualidade onde nossos profissionais foram instruídos pelas melhores instituições de ensino dentro desse segmento. Solicite as instalações elétricas de média tensão da MS Projetos Industriais e fique tranquilo quanto a funcionalidade e segurança de seu equipamento. Para conhecer os trabalhos prestados por nossa empresa e os grandes nomes que atendemos dentro do Brasil confira a aba “portfólio” disponível em nosso site. </p>
<h2>O melhor serviço de instalações elétricas de média tensão do mercado</h2>
<p>Por estar no mercado há mais de 15 anos realizando diversos serviços como as instalações elétricas de média tensão possui alta experiência além da grande qualidade que empregadas em conjunto no dia a dia proporcionam resultados incríveis em cada trabalho realizado por nossos profissionais. Atuamos seguindo todas as normas técnicas e ambientais compreendendo que uma de nossas principais funções é atuar de forma sustentável preservando o ambiente para as futuras gerações. Conheça mais sobre nossa empresa e nossos serviços prestados acessando nosso site.</p>
<h3>Saiba mais sobre nosso serviço de instalações elétricas de média tensão</h3>
<p>É essencial que as instalações elétricas de média tensão sejam realizadas por profissionais competentes para garantir a qualidade e segurança em seu funcionamento. Uma má instalação industrial pode comprometer a integridade de seu equipamento com descargas de alta tensão que podem queimar componentes gerando muito prejuízo. Em situações mais extremas a falha das instalações elétricas pode colocar em risco a integridade física de seus colaboradores e a segurança do ambiente de trabalho. Não corra esses riscos e entre em contato agora mesmo para solicitar atendimento com a MS Projetos Industriais. </p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>