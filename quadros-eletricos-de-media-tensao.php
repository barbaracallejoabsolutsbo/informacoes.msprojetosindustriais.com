<?php
    $title       = "Quadros Elétricos de Média Tensão";
    $description = "Quadros elétricos de média tensão: conheça esse equipamento e sua importância. A MS Projetos Industriais investe na segurança de nossos clientes.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Seja em um ambiente industrial ou comercial, se o local precisa manter equipamentos e máquinas em constante funcionamento, ou necessita de energia elétrica para funcionamento geral, as empresas precisarão de <strong>quadros elétricos de média tensão</strong> ou baixa tensão.</p>
<p>Quadros elétricos são usados em áreas para distribuição de energia elétrica. O mercado de quadros elétricos de média e baixa tensão tem ampla abrangência de aplicação e para garantir qualidade de sua instalação, sua fabricação e montagem devem ser feitas de acordo com normas técnicas vigentes.</p>
<h2>A MS Projetos Industriais no serviço de <strong>quadros elétricos de média tensão</strong></h2>
<p>Formada por profissionais experientes, a MS Projetos Industriais está localizada no estado de Minas Gerais. Nossos funcionários passam por processos constantes de capacitação e atendem todos os requisitos de segurança e qualidade que sua empresa precisa para <strong>quadros elétricos de média tensão</strong>.</p>
<p>Além disso, recebem orientação para atuarem sempre conforme as normas técnicas, tendências de segmento e tecnologia moderna, oferecendo o melhor atendimento na área. Trabalhamos com uma equipe de confiança e com capacidade técnica para planejamento de projetos e instalação de equipamentos.</p>
<p>Os <strong>quadros elétricos de média tensão</strong> atuam como um centro de carga e são de fácil instalação, independente se montados em dependências de empresas ou em fábricas maiores. Para essa segurança, os quadros são testados e analisados com tecnologias modernas para a certeza de segurança e funcionalidade plena.</p>
<p>Como esses equipamentos entram em operação apenas com emissão de certificados e laudos técnicos, nossos profissionais garantem operação completa para que os <strong>quadros elétricos de média tensão</strong> tenham toda eficiência disponível, evitando sobrecargas elétricas ou erros.</p>
<h3>Saiba mais sobre <strong>quadros elétricos de média tensão</strong></h3>
<p>Em qualquer local que instalados os quadros elétricos, deve ser feito um projeto elétrico detalhado para a montagem do quadro, e esse serviço é realizado por uma empresa especializada. O projeto elaborado irá garantir a melhor forma de instalação dos <strong>quadros elétricos de média tensão</strong> com o menor custo possível.</p>
<p>Assim, os<strong> quadros elétricos de média tensão</strong> necessitam de documentação técnica e detalhes que incluem diagramas funcionais, elétricos, dimensionais e mecânico da montagem. Para isso, são essenciais profissionais capacitados no assunto, proporcionando o melhor planejamento para empresas e indústrias que querem instalar quadros elétricos de baixa ou média tensão.</p>
<p>Em cada caso de aplicação dos quadros elétricos, há uma configuração diferente para adequação com o projeto. Por isso, é importante contratar um serviço especializado como a MS Projetos Industriais.</p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>