<?php
    $title       = "Montagem de Painéis Elétricos";
    $description = "Se você precisa de um atendimento para realizar a montagem de painéis elétricos com agilidade e preço justo encontrou o lugar certo. A MS Projetos Industriais. ";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Se você precisa de um atendimento para realizar a montagem de painéis elétricos com agilidade e preço justo encontrou o lugar certo. A MS Projetos Industriais está há mais de 15 anos no mercado realizando a montagem de painéis elétricos e diversos outros serviços como manutenção em subestação, termografia elétrica, emissão de laudo (NR-10, NR-12, aterramento, ICMS), fabricação de painéis e quadros elétricos e muito mais. Os painéis elétricos são responsáveis pela distribuição de energia para diversos circuitos individuais que existem dentro de uma rede elétrica industrial. Por ser extremamente importante para a alimentação elétrica dentro da empresa, a montagem de painéis elétricos deve ser realizada por um profissional tecnicamente habilitado e com formação comprovada para realizar a atuação. Em nossa empresa atuamos seguindo todas as normas técnicas e ambientais compreendendo que uma de nossas principais funções é atuar de forma sustentável preservando o ambiente para as futuras gerações. Para alcançar os melhores resultados contamos com uma equipe de alta qualidade e conhecimento técnico adquirido que já possui vasta experiência na prestação de serviços para indústrias. Solicite o serviço de montagem de painéis elétricos de quem realmente tem muita propriedade e experiência no assunto.</p>
<h2>O melhor serviço de montagem de painéis elétricos do Brasil</h2>
<p>Contamos com diversas avaliações positivas de nossos serviços como o serviço de montagem de painéis elétricos. Atendemos grandes nomes da indústria que podem ser consultados através da aba “portfolio” disponível em nosso site. Nossa empresa atua tanto prestando serviço para o setor privado quanto para o setor público com qualidade, ética, respeito e compromisso. Trabalhamos com a visão de elevar nossa importância nos próximos anos, nos tornando destaque dentre as maiores fornecedoras de serviços de engenharia e montagens industriais no cenário nacional.</p>
<h3>Saiba mais sobre nosso serviço de montagem de painéis elétricos</h3>
<p>Para saber mais sobre a montagem de painéis elétricos ou quaisquer outros serviços ou produtos disponibilizados por nossa empresa entre em contato e seja atendido por um especialista para te auxiliar da melhor maneira possível. Um equipamento desses quando mal montado ou instalado pode ocasionar diversas falhas, comprometer a integridade do seu equipamento e até mesmo a integridade do utilizador. Não corra esse risco e solicite um especialista da MS Projetos Industriais para realizar sua instalação sempre que precisar. Tudo que você precisa sobre engenharia elétrica e automação industrial você encontra em um só lugar.</p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>