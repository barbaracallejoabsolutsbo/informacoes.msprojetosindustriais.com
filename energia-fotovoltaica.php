<?php
    $title       = "Energia Fotovoltaica";
    $description = "Encontre todos os serviços envolvidos na obtenção de sistemas de energia fotovoltaica com a MS Solar. Conheça a MS Projetos e nos contrate agora mesmo.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>A <strong>energia fotovoltaica </strong>pode gerar economia de até 95% em sua conta de energia elétrica pela distribuidora de eletricidade da sua região. Isto porque, a energia solar, é uma energia proveniente dos raios solares, para ser captada e utilizada, depende de apenas um investimento inicial em um sistema para captação e distribuição de energia solar. O investimento no sistema de captação pode ser recuperado em até 7 anos com a implementação do sistema.</p>
<p>Sendo uma fonte renovável, limpa e ecológica de energia, a <strong>energia fotovoltaica</strong> se baseia no fenômeno fotoelétrico pela absorção de fótons e liberação de elétrons que ocorre nas placas solares, gerando corrente elétrica. Esta corrente elétrica é convertida pelo inversor em energia elétrica para ser distribuída a residências, condomínios, empresas e indústrias.</p>
<p>Diversas empresas vêm investindo em sustentabilidade com energia fotovoltaica, além de ser uma novidade também no cenário de comercialização de energia elétrica. A MS Solar, empresa do segmento de energia solar e engenharia elétrica, oferece serviços especializados em projetos de implementação de sistemas de captação e utilização de <strong>energia fotovoltaica,</strong> envolvendo todos os processos burocráticos para aprovação do projeto e homologação de equipamentos junto à distribuidora de energia da sua região.</p>
<p>Encontre todos os serviços envolvidos na obtenção de sistemas de <strong>energia fotovoltaica </strong>com a MS Solar. Especializada em prospecção de terras e imóveis para projetos de energia solar, oferecemos todo o suporte de uma equipe profissional que acompanha do início ao fim de cada projeto auxiliando em todos os processos.</p>
<p></p>
<h2><strong>Benefícios da energia fotovoltaica para empresas</strong></h2>
<p>Os benefícios no investimento em <strong>energia fotovoltaica, </strong>para empresas de todo o tamanho, são nítidos no cenário atual no mundo todo. Proporcionando redução na conta de energia elétrica mesmo utilizando mais energia do que antes, os sistemas de captação de energia solar garantem eficiência e energia renovável para uso industrial e empresarial, além de ser uma escolha segura de investimento, pois o retorno é garantido.</p>
<h3><strong>Benefícios da energia fotovoltaica para residências</strong></h3>
<p>Tanto para sistemas integrados à rede elétrica convencional quanto sistemas independentes de <strong>energia fotovoltaica </strong>podem proporcionar benefícios diversos inclusive quando usados em residências. Com economia na conta de energia, proporciona segurança à inflação energética, além de em casos de sistemas híbridos e independentes, poderem manter os equipamentos ligados mesmo em queda de luz elétrica.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>