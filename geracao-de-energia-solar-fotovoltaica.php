<?php
    $title       = "Geração de Energia Solar Fotovoltaica";
    $description = "Para realizar a geração de energia solar fotovoltaica é necessário a criação de um sistema de captação através de painéis solares, que vão captar a energia.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Você sabe para que se destina a geração de energia solar fotovoltaica e como é realizado esse processo? Primeiramente podemos considerar a geração de energia solar fotovoltaica como um método de captação de energia solar e conversão em energia elétrica para utilização comercial, industrial ou residência. Na MS Projetos Industriais você encontra as melhores soluções em engenharia elétrica e automação industrial com ótimos preços e uma qualidade técnica incrível. Contamos com profissionais altamente qualificados que foram instruídos pelas melhores instituições de ensino dentro desse segmento. Nossa empresa atua no mercado há quase 2 décadas e além de geração de energia solar fotovoltaica oferece diversos serviços como subestação, termografia elétrica, emissão de laudo (NR-10, NR-12, aterramento, ICMS), fabricação de painéis elétricos e muito mais. Não perca essa oportunidade de solicitar um sistema de geração de energia solar fotovoltaica em uma empresa referência dentro desse segmento no Brasil. Nossa missão é criar soluções em engenharia elétrica que garantam excelência, apoiadas na competência e no aprimoramento tecnológico buscando sempre agir de forma ética e sustentável. Atuamos seguindo todas as normas técnicas e ambientais compreendendo que uma de nossas principais funções é atuar de forma sustentável preservando o ambiente para as futuras gerações. </p>
<h2>Solicite aqui seu sistema de geração de energia solar fotovoltaica</h2>
<p>Para realizar a geração de energia solar fotovoltaica é necessário a criação de um sistema de captação através de painéis solares, que vão captar a energia produzida pelos raios solares e encaminhar para que o sistema faça a conversão de energia solar em energia elétrica e posteriormente seja distribuído através do quadro elétrico. Para realizar esse procedimento utilizamos equipamentos de alta tecnologia que foram desenvolvidos com componentes de qualidade fabricados por empresas de confiança.  </p>
<h3>A geração de energia solar fotovoltaica é segura e se paga a longo prazo</h3>
<p>Com a geração de energia solar fotovoltaica você pode realizar o pagamento do sistema à (não tão) longo prazo com a diminuição dos gastos com energia elétrica fornecidos por empresas estatais. Com esse sistema você fica livre das taxações e correções em cima da sua conta de luz. Para saber mais sobre esse ou nossos outros serviços entre em contato com nossa equipe e seja auxiliado por um especialista durante o seu atendimento. Atendemos com qualidade, segurança e agilidade para proporcionar bons resultados. </p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>