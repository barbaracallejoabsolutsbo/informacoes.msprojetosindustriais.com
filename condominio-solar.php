<?php
    $title       = "Condomínio Solar";
    $description = "Existem diversas formas de se produzir energia solar com um condomínio solar, conheça os serviços da MS Solar para sistemas completos de captação fotovoltaica.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>O<strong> condomínio solar </strong>é um modelo diferente de produção de energia solar em larga escala para uso desta eletricidade para alimentação de diversos imóveis. Além de ser uma fonte limpa e renovável de energia elétrica, estes tipos de condomínio proporcionam uma alta redução na conta de luz dos usuários, tornando o projeto um tanto quanto mais atrativo.</p>
<p>Com a MS Projetos é possível realizar o projeto e obter toda certificação e documentação para um sistema de<strong> condomínio solar </strong>de acordo com as suas necessidades. Os condomínios solares consistem em equipamentos formados por células fotovoltaicas de silício. Estas placas captam a energia solar sem que a temperatura influencie no processo, portanto não depende de raios solares diretos. O inversor converte a energia solar em energia elétrica para ser distribuída para toda rede.</p>
<p>O modelo de<strong> condomínio solar </strong>pode ser considerado também um sistema de fazenda de energia solar. É também um modelo de negócio com a possibilidade de compartilhamento de energia em vários investidores, a fim de montarem um grande local para o aproveitamento da luz solar.</p>
<p>Desenvolvido por uma equipe técnica profissional e experiente, os projetos para <strong>condomínio solar </strong>podem ser encontrados com a MS Projetos. Contrate nossos serviços especializados em Engenharia Elétrica e encontre soluções em sistemas elétricos para diversas aplicações.</p>
<h2><strong>Quais modalidades de condomínio solar existem?</strong></h2>
<p>Existem diversas formas de produzir energia solar com um<strong> condomínio solar</strong>, podendo ser para sua residência ou até mesmo empresa. Também é possível encontrar locatários de energia proveniente de condomínios solares. Conheça mais sobre todos estes serviços e projetos falando com a MS Projetos.</p>
<p>A fim de promover soluções para todo ramo de sistemas elétricos a nível industrial, a MS Projetos conta com uma equipe especializada para atender toda demanda de Engenharia Elétrica especialmente para você. Contate-nos.</p>
<p></p>
<h3><strong>Quais benefícios em criar um sistema de condomínio solar</strong></h3>
<p>Um<strong> condomínio solar </strong>basicamente é um tipo de sistema de placas que pode ser utilizado de diversas formas para beneficiar diversas pessoas de uma vez só. Uma forma democrática de diminuir os gastos com energia, com reduções de até 90% em conta de luz em alguns casos, os consumidores podem aproveitar de um sistema como esse para não sofrer com as taxas de energia abusivas.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>