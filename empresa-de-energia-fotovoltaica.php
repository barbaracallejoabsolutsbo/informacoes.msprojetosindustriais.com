<?php
    $title       = "empresa de Energia Fotovoltaica";
    $description = "Contrate a MS Projetos, nossos serviços de empresa de energia fotovoltaica e obtenha seu sistema regularizado com a concessionária de luz da sua região.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>A MS Projetos não só é uma<strong> empresa de energia fotovoltaica, </strong>como também oferece soluções completas para diversos tipos de sistemas elétricos industriais. Desde a criação de diagramas, documentação, adequação a normas regulamentadoras, até construção e instalação de painéis de controle, quadros elétricos e muito mais. Conheça mais sobre os serviços para toda a área de energia fotovoltaica.</p>
<p>Especializada também em painéis solares e tudo que envolve este tipo de fonte de energia elétrica, a MS Projetos é uma<strong> empresa de energia fotovoltaica </strong>que presta diversos serviços para energia solar.</p>
<p>O funcionamento da energia solar fotovoltaica é feito por um sistema de captação de energia composto por painéis solares. Estes painéis solares captam a energia solar e por meio de conversores, transforma a energia solar em energia elétrica para uso doméstico, empresarial, industrial e comercial. Existem diversas aplicações da energia fotovoltaica para o mercado hoje em dia, que vão desde fazendas de energia solar para empresas, indústrias e condomínios, até energia solar residencial. Consulte nossa <strong>empresa de energia fotovoltaica</strong> para realizar seu projeto.</p>
<p>Dependendo do projeto feito com nossa <strong>empresa de energia fotovoltaica, </strong>seu projeto pode garantir eficiência e economia de até 95% em contas de energia, dependendo do seu investimento. Faça orçamentos conosco e para mais informações fale com nosso atendimento.</p>
<h2><strong>Quais serviços a MS Projetos, empresa de energia fotovoltaica, oferece</strong></h2>
<p>A<strong> empresa de energia fotovoltaica</strong>, MS Projetos, dispõe de serviços para a área de energia solar que vão desde a prospecção de terras até o contrato e manutenção da energia solar, bem como instalação, acompanhamento pós venda e comercialização de energia. Com nossos serviços é possível alcançar uma economia de quase 95% na conta de luz ou mais. Contrate nossos serviços agora mesmo.</p>
<h3><strong>Empresa de energia fotovoltaica e créditos de energia</strong></h3>
<p>Durante todo o dia, os módulos de placas solares captam a luz solar para gerar energia elétrica, que passa pelo inversor e é utilizado na rede elétrica. Após passar pelo inversor, ela pode alimentar qualquer equipamento elétrico da casa, mas caso nem toda energia for utilizada, o que sobra é lançado na rede gerando créditos. Esses créditos são abatidos nas próximas contas de luz, com validade de 60 meses, e caso não sejam utilizados no mês em que foram gerados, podem ser utilizados neste prazo para abater em contas de luz futuras. Contrate nossos serviços de <strong>empresa de energia fotovoltaica </strong>e obtenha seu sistema regularizado com a concessionária de luz da sua região.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>