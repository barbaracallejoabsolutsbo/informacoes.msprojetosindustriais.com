<?php
    $title       = "Geração de Energia Solar";
    $description = "A geração de energia solar é um recurso que pode reduzir drasticamente os gastos de sua empresa ou indústria através de um investimento que hoje em dia se paga.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Você sabe como funciona a geração de energia solar e como esse recurso pode ser importante para o planeta? Na MS Projetos Industriais nós atuamos com projetos de geração de energia solar com alta tecnologia, muita qualidade e segurança. Estamos no mercado há mais de 15 anos dentro dos segmentos de engenharia elétrica e automação industrial. Contamos com profissionais altamente treinados e qualificados para empregar os conceitos e metodologias necessárias para obter os melhores resultados. Nossa empresa atua com a visão de elevar nossa importância nos próximos anos, nos tornando destaque dentre as maiores fornecedoras de serviços de engenharia e montagens industriais no cenário nacional. Para isso empregamos valores que prezamos essenciais como transparência, compromisso, respeito, ética e sustentabilidade. A geração de energia solar basicamente é realizada captando a energia do sol e transformando em energia elétrica para o uso comercial, industrial e até mesmo doméstico. A geração de energia solar vem avançando de forma tão intensa no Brasil que atualmente até mesmo algumas empresas já disponibilizam essa possibilidade nas construções de seus empreendimentos. Principalmente por ser renovável e com uma fonte altamente acessível em um país tropical, esse desenvolvimento tende a crescer cada vez mais.</p>
<h2>Tudo que você procura sobre geração de energia solar</h2>
<p>A geração de energia solar é um recurso que pode reduzir drasticamente os gastos de sua empresa ou indústria através de um investimento que hoje em dia se paga em não tão longo prazo com a redução da própria conta de energia. A captação de energia solar é realizada através da instalação de painéis solares em áreas estratégicas onde o sol passa a ser uma fonte de fornecimento muito eficaz. Existem outros métodos de captação porém não se destinam a conversão e utilização de energia elétrica.</p>
<h3>Saiba mais sobre a geração de energia solar</h3>
<p>Para saber mais sobre a geração de energia solar ou quaisquer outros produtos ou serviços disponíveis em nosso catálogo entre em contato e seja atendido por um especialista para te auxiliar da melhor maneira possível. Atuamos seguindo todas as normas técnicas e ambientais compreendendo que uma de nossas principais funções é atuar de forma sustentável preservando o ambiente para as futuras gerações. Nossa missão é criar soluções em engenharia elétrica que garantam excelência, apoiadas na competência e no aprimoramento tecnológico buscando sempre agir de forma ética e sustentável.</p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>