<?php
    $title       = "empresa de Energia Fotovoltaica Rio de Janeiro";
    $description = "A MS Solar é uma empresa de energia fotovoltaica Rio de Janeiro que oferece todo suporte para você que procura por serviços para energia solar. ";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Oferecendo serviços para todo o país, nossa<strong> empresa de energia fotovoltaica Rio de Janeiro </strong>é especializada em serviços para o ramo de energia solar. A MS Solar, do grupo MS, é referência no cenário nacional e mineiro. Com serviços especializados para diversos segmentos da Engenharia Elétrica, nosso diferencial é a oferta de soluções modernas e econômicas para serviços de documentação.</p>
<p>A MS Projetos trabalha com o segmento de<strong> empresa de energia fotovoltaica Rio de Janeiro </strong>por meio da MS Solar. Oferecendo serviços que vão desde a prospecção de terras rurais para sistemas que alimentam grandes construções como indústrias, condomínios ou rede compartilhada, até instalação personalizada residencial para uso pessoal.</p>
<p>Referência na área de<strong> empresa de energia fotovoltaica Rio de Janeiro</strong>, a MS Solar <strong>empresa de energia fotovoltaica Rio de Janeiro </strong>também oferece serviços de incorporação de imóveis, estudo de viabilidade técnica financeira, fornecimento de soluções completas, especificações técnicas para geração de energia solar para residências, indústrias e empresas. Encontre soluções e suporte para projetos completos para energia solar conosco.</p>
<h2><strong>Nossa empresa de energia fotovoltaica Rio de Janeiro garante redução de até 95%</strong></h2>
<p>A MS Solar é uma<strong> empresa de energia fotovoltaica Rio de Janeiro </strong>que oferece todo suporte para você que procura por serviços para energia solar. Fornecendo soluções completas, atendemos desde empresas, indústrias até residências e condomínios. Com total acompanhamento pós venda, realizamos todo estudo e projeto, análise de dados após a implantação, além de comercialização de energia solar. Conheça nossos serviços e solicite orçamentos falando com nosso atendimento.</p>
<h3><strong>Serviços da empresa de energia fotovoltaica Rio de Janeiro MS Projetos</strong></h3>
<p>A MS Solar é um dos segmentos da MS Projetos Industriais. Nossa <strong>empresa de energia fotovoltaica Rio de Janeiro </strong>oferece serviços que vão além de energia solar sustentável. Aqui é possível encontrar serviços completos para montagens, projetos, implantação de sistemas elétricos, quadros e painéis de alimentação, comando e controle para diversas aplicações. Estudos completos de eficiência energética para projetos de melhorias e adequação de normas regulamentadoras.</p>
<p>Entre todas as etapas de qualquer projeto aqui contratado, a MS Projetos oferece suporte de profissionais certificados e experientes. Conheça mais sobre nossos serviços acessando nosso site.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>