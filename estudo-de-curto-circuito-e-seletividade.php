<?php
    $title       = "Estudo de Curto Circuito e Seletividade";
    $description = "Faça seu estudo de curto circuito e seletividade com a MS Projetos e tenha suporte de uma equipe profissional altamente preparada e experiente. ";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>O<strong> estudo de curto circuito e seletividade </strong>é um método utilizado na Engenharia Elétrica para conhecer o comportamento de uma rede de sistema elétrico de potência quando ocorre a falta de energia elétrica. Ao final do estudo, é gerado um laudo que fornece a descrição metodológica com conclusões e recomendações para prevenção de curtos circuitos.</p>
<p>Baseado na análise do sistema elétrico para determinar a magnitude das correntes e suas flutuações durante uma falha elétrica, o<strong> estudo de curto circuito e seletividade </strong>é indispensável para segurança de equipamentos e maquinários industriais. Determinando o nível de gravidade dos curtos circuitos é possível prevenir danos e preparar um sistema que conte com proteção para equipamentos e sistemas elétricos.</p>
<p>Após o <strong>estudo de curto circuito e seletividade </strong>é possível criar todo um suporte para proteção dos equipamentos e sistemas baseados em disjuntores, fusíveis, relés, entre outros com serviços de coordenação e seletividade. Os passos envolvidos neste processo são divididos em algumas etapas e a MS Projetos realiza todos eles.</p>
<p>Faça seu <strong>estudo de curto circuito e seletividade </strong>com a MS Projetos e tenha suporte de uma equipe profissional altamente preparada e experiente. Os benefícios em investir em estudos de curto-circuito são inúmeros, enaltecendo sempre o fator de segurança, confiabilidade na rede elétrica e para atendimento de normas e padrões de funcionamento industrial. Com vasta experiência no cenário de Engenharia Elétrica, a MS Projetos é especializada na aplicação de estudos de curto circuito utilizando softwares e equipamentos modernos. Fale conosco para mais informações.</p>
<h2><strong>A importância do estudo de curto circuito e seletividade</strong></h2>
<p>Fundamental para funcionamento seguro de sistemas elétricos, o<strong> estudo de curto circuito e seletividade </strong>determina e dimensiona parâmetros de funcionamento de sistemas elétricos para proteção e redução de riscos de danos nas instalações, trabalhadores e máquinas, além de ser indispensável também para conformidade com normas regulamentadoras vigentes.</p>
<h3><strong>Contrate a MS Projetos para fazer o estudo de curto circuito e seletividade</strong></h3>
<p>Fale com nosso atendimento agora mesmo e solicite seu orçamento para serviços de <strong>estudo de curto circuito e seletividade </strong>para sua empresa. Referência no cenário nacional, a MS Projetos já desenvolveu serviços para diversos órgãos públicos e empresas privadas.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>