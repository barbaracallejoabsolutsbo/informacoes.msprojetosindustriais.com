<?php
    $title       = "Placa de Energia Solar";
    $description = "Conheça a placa de energia solar: fácil de instalar e mínima manutenção. A MS Projetos Industriais disponibiliza serviços no segmento de geração fotovoltaica.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>A <strong>placa de energia solar </strong>é um equipamento capaz de capturar os raios solares e transformá-los em energia elétrica. Ela é importante atualmente para soluções sustentáveis. Investir em uma <strong>placa de energia solar</strong> é econômico a longo prazo, compensando gastos financeiros com energia elétrica.</p>
<p>E como a energia é convertida em eletricidade? O efeito fotovoltaico é o que garante a transformação dessa energia. A instalação de um sistema fotovoltaico é rápida pode ser feita por uma empresa especializada.</p>
<h2>Conheça a MS Projetos Industriais e nossos serviços com energia solar</h2>
<p>Com sede em Contagem, no estado de Minas Gerais e atuante em todo o território nacional, nosso segmento de geração fotovoltaica possui equipe especializada em comercialização e implantação de energia, prospecção de imóveis, incorporação, projeto, construção, e assim por diante.</p>
<p>Há casos em que a <strong>placa de energia solar</strong> e o inversor são montados em estruturas já existentes, o que requer apenas pequenas adaptações. A durabilidade mínima do sistema com <strong>placa de energia solar</strong> é de 25 anos e sua manutenção se restringe a limpeza das placas, o que pode ser feita entre 6 e 12 meses, dependendo da inclinação instalada.</p>
<p>Esse equipamento é composto por um painel especial onde estão as células fotovoltaicas, e a moldura que une esses elementos é em alumínio, ou seja, se trata um produto pensado para atender as mais adversas mudanças climáticas, perfeitas tanto para construções residenciais, como para empresas e indústrias.</p>
<h3>Vantagens de um sistema com <strong>placa de energia solar</strong></h3>
<p>Compostas por materiais de qualidade e bastantes resistentes, a <strong>placa de energia solar</strong> tem elevada vida útil. Esse equipamento necessita de mínima manutenção, é muito fácil de instalar e barato para manter.</p>
<p>Podendo ser utilizada em áreas isoladas de rede elétrica, seu sistema é facilmente instalado com uma equipe técnica especializada, assim seu funcionamento é descomplicado e exige um prazo médio de uma semana em instalações residenciais, por exemplo.</p>
<p>Além da facilidade e simplicidade, a utilização da <strong>placa de energia solar</strong> não polui, é renovável, limpa e silenciosa.</p>
<p>Portanto, entre em contato com nossa equipe e conheça todas as facilidades que a MS disponibiliza nesse segmento. Trabalhamos desde a prospecção de terras, estudo de viabilidade técnica financeira, dimensionamento do sistema solar, até o fornecimento de solução completa, especificação técnica para geração em residências, empresas e indústrias, e muito mais. A aquisição da <strong>placa de energia solar</strong> se torna ainda mais fácil com a MS.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>