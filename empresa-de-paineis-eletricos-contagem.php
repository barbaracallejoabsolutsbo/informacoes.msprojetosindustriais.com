<?php
    $title       = "empresa de Painéis Elétricos Contagem";
    $description = "A MS Projetos, empresa de painéis elétricos Contagem, Minas Gerais, trabalha com a missão de oferecer soluções para todo o ramo de engenharia elétrica.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>A<strong> empresa de painéis elétricos Contagem </strong>que oferece o melhor custo benefício e profissionalismo do mercado é a MS Painéis.Do grupo MS, aMS Painéis é uma empresa que atua no ramo de energia elétrica realizando estudos, projetos de regulamentação, projetos completos de instalação e inclusive manutenção de sistemas elétricos. É possível obter orçamentos e mais informações sobre os serviços falando com nosso atendimento ou acessando o site.</p>
<p>Com a<strong> empresa de painéis elétricos Contagem</strong>, encontre o melhor custo benefício com suporte completo para construção, manutenção, planejamento e instalação de quadros e painéis elétricos.O comprometimento ao atender serviços preservando o meio ambiente e proporcionando desenvolvimento sustentável é o diferencial da MS Projetos. Com uma diversidade de serviços que vão desde projetos de eficiência para indústrias até a instalação e geração de energia solar para consumo empresarial, comercial e industrial, inclusive residencial.</p>
<p>Encontre serviços também para sistemas de captação de energia solar com a MS Solar, extensão da<strong> empresa de painéis elétricos </strong>Contagem. Os serviços oferecidos vão desde a concepção do projeto até a realização da instalação e implementação junto a distribuidora de energia da sua região. Contate-nos para mais informações e solicite seu orçamento.</p>
<p>A MS, <strong>empresa de painéis elétricos</strong>, coloca a sua equipe a prontidão para atendimento e mais informações sobre serviços falando conosco por e-mail, telefone ou whatsapp. </p>
<h2><strong>A empresa de painéis elétricos Contagem que você procurava é a MS.</strong></h2>
<p>A MS,<strong> empresa de painéis elétricos Contagem</strong>, também presta serviços para implementação de sistemas de painéis fotovoltaicos. Usufrua da captação da energia solar para uso doméstico, industrial, comercial, e muito mais. Conheça mais sobre sistemas de painéis solares acessando nosso site e visite a MS Solar.</p>
<h3><strong>Empresa de painéis elétricos Contagem para projetos completos</strong></h3>
<p>Desde montagens industriais até projetos de sistemas elétricos completos, realizamos serviços preservando o meio ambiente. Com diversos projetos em prol da sustentabilidade, entre as diversas áreas de atuação, trabalhamos também com projetos para energia solar.</p>
<p>A<strong> empresa de painéis elétricos Contagem</strong>, Minas Gerais, trabalha com a missão de oferecer soluções para todo o ramo de engenharia elétrica. Com todos os processos envolvidos na produção de quadros e painéis elétricos, oferecemos sempre serviços com ética e sustentabilidade.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>