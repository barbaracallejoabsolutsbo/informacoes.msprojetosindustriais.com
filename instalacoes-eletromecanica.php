<?php
    $title       = "Instalações Eletromecânica";
    $description = "As instalações eletromecânicas estão relacionadas a diversos segmentos industriais que utilizam máquinas eletromecânicas para desenvolver suas funções do dia a dia.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Se você procura por uma empresa que realize instalações eletromecânicas com qualidade e agilidade encontrou o lugar ideal. A MS Projetos Industriais é uma empresa que atua dentro da engenharia elétrica e automação industrial fornecendo equipamentos, prestando serviços e fazendo a diferença de forma sustentável. São mais de 15 anos prestando serviço para empresas privadas e para o setor público com ética, transparência, compromisso e qualidade. As instalações eletromecânicas estão relacionadas a diversos segmentos industriais que utilizam máquinas eletromecânicas para desenvolver suas funções do dia a dia. A missão da nossa empresa é criar soluções em engenharia elétrica que garantam excelência, apoiadas na competência e no aprimoramento tecnológico buscando sempre agir de forma ética e sustentável. Além das instalações eletromecânicas prestamos diversos serviços como manutenção em subestação, termografia elétrica, emissão de laudo (NR-10, NR-12, aterramento, ICMS), fabricação de painéis elétricos e muito mais. Trabalhamos com a visão de elevar nossa importância nos próximos anos, nos tornando destaque dentre as maiores fornecedoras de serviços de engenharia e montagens industriais no cenário nacional. Solicite as instalações eletromecânicas que você necessita com quem realmente tem propriedade para realizá-las, conte conosco sempre que precisar.</p>
<h2>Saiba como são feitas as instalações eletromecânicas da MS Projetos Industriais</h2>
<p>Para realizar as instalações eletromecânicas analisamos o equipamento a ser instalado e a estrutura do local e se necessário fornecemos componentes para otimizar a distribuição elétrica e realizar uma instalação com qualidade e segurança. Um equipamento mal instalado pode ocasionar diversas falhas, comprometer a integridade do seu equipamento e até mesmo a integridade do utilizador. Não corra esse risco e solicite um especialista da MS Projetos Industriais para realizar sua instalação sempre que precisar. </p>
<h3>Saiba mais sobre nossas instalações eletromecânicas</h3>
<p>Para saber mais sobre as instalações eletromecânicas ou quaisquer outros produtos e serviços realizados por nossa empresa entre em contato e seja atendido por um especialista para te auxiliar da melhor maneira possível. Prezamos por metodologias ecológicas e sustentáveis para preservar o planeta e seus recursos naturais de forma com que se renovem constantemente para o abastecimento das futuras gerações. Atendemos grandes nomes da indústria que podem ser consultados através da aba “portfolio” disponível em nosso site. Conte com uma empresa de alta qualidade e confiabilidade para realizar as instalações que você precisa, entre em contato conosco agora mesmo. </p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>