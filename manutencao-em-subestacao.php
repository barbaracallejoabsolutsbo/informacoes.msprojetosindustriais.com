<?php
    $title       = "Manutenção em Subestação";
    $description = "Com a manutenção em subestação você garante o máximo desempenho do equipamento prolongando sua vida útil de usabilidade. A subestação é um dispositivo muito.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Se você procura por uma empresa altamente qualificada para realizar a manutenção em subestação elétrica com agilidade e praticidade encontrou o lugar certo. A MS Projetos Industriais é uma empresa que atua dentro da engenharia elétrica e automação industrial fabricando, comercializando, instalando equipamentos e prestando serviços de alta importância para o segmento. Estamos há mais de 15 anos realizando a manutenção em subestação e por possuir uma grande experiência realizamos esse atendimento com muita propriedade. A manutenção em subestação é realizada de forma preventiva buscando identificar e reparar falhas previamente antes que ela comprometa o abastecimento de energia do local. Com a manutenção em subestação você garante o máximo desempenho do equipamento prolongando sua vida útil de usabilidade. A subestação é um dispositivo muito importante para a indústria de forma geral por ser responsável pela transformação, proteção, controle e distribuição da energia elétrica. Atuamos seguindo todas as normas técnicas e ambientais compreendendo que uma de nossas principais funções é atuar de forma sustentável preservando o ambiente para as futuras gerações. Disponibilizamos profissionais de alta qualidade para realizar as manutenções disponibilizadas em nosso catálogo de serviços. </p>
<h2>Solicite aqui sua manutenção em subestação</h2>
<p>Realizamos a manutenção em subestação de diversos fabricantes com muita propriedade técnica do funcionamento do equipamento. Não deixe de conhecer nossos outros serviços de alta qualidade como manutenções elétricas em geral, instalações elétricas industriais de média e baixa tensão, avaliação e emissão de laudo com parecer técnico, comercialização de diversos equipamentos e muito mais. Tudo que você precisa sobre engenharia elétrica ou automação industrial em um só lugar. Trabalhamos com a visão de elevar nossa importância nos próximos anos, nos tornando destaque dentre as maiores fornecedoras de serviços de engenharia e montagens industriais no cenário nacional.</p>
<h3>Saiba mais sobre a manutenção em subestação</h3>
<p>Caso você tenha dúvidas sobre a manutenção em subestação ou quaisquer outros produtos e serviços disponíveis em nosso catálogo entre em contato e seja atendido por um especialista para te auxiliar da melhor maneira possível. Realizar a manutenção preventiva periodicamente previne falhas graves que comprometam o funcionamento de sua empresa além de diminuir os custos que muitas vezes são altamente elevados para realizar as manutenções corretivas. Nossa missão é criar soluções em engenharia elétrica que garantam excelência, apoiadas na competência e no aprimoramento tecnológico buscando sempre agir de forma ética e sustentável. Conte conosco sempre que precisar. </p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>