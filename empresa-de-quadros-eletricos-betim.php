<?php
    $title       = "empresa de Quadros Elétricos Betim";
    $description = "Nossa empresa de quadros elétricos Betim oferece custo benefício em serviços especializados para toda área da Engenharia Elétrica.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Com a MS Painéis você encontra serviços de<strong> empresa de quadros elétricos Betim </strong>de forma atenciosa e personalizada. Para todo o ramo industrial, empresarial e residencial, oferecemos serviços que vão desde a criação e personalização de quadros e armários de eletricidade até a automação de subestações para indústrias e empresas. Nossos serviços atendem normas nacionais e internacionais da Engenharia e sempre utilizamos fornecedores de alto padrão de equipamentos para nossos sistemas e projetos.</p>
<p>Nossa <strong>empresa de quadros elétricos Betim</strong>, MS Painéis, trabalha desenvolvendo quadros conforme projetado, de forma customizada. As opções de quadros são diversas, vão desde quadros elétricos para comando predial até quadros de bomba de recalque e incêndio. Consulte nosso atendimento para orçamentos e mais informações e contrate a MS para serviços com excelência.</p>
<p>Outros segmentos atendidos em nossa <strong>empresa de quadros elétricos Betim </strong>se estendem também a ecologia, sustentabilidade e valorização do imóvel. A MS Solar, empresa do segmento de energia solar do grupo MS, oferece serviços especializados em projetos de implementação de sistemas de captação e utilização de energia fotovoltaica.</p>
<p>Todos os processos envolvidos em todas etapas burocráticas dos serviços oferecidos em nossa<strong> empresa de quadros elétricos Betim </strong>são acompanhados por nossos profissionais qualificados e experientes, que acompanham todo o processo para que os projetos sejam desenvolvidos dentro dos prazos e com total qualidade de instalação dos sistemas, captação, medição e distribuição de energia conforme os serviços solicitados.</p>
<h2><strong>Proteção elétrica é com a empresa de quadros elétricos Betim </strong></h2>
<p>Normalmente, nos sistemas elétricos, existem sistemas de proteção compostos por relés, fusíveis e disjuntores. Conheça nossos projetos de seletividade de proteção, proteção para curtos circuitos, geradores de energia, nobreak e sistema de proteção para todos seus equipamentos. A <strong>empresa de quadros elétricos Betim </strong>desenvolve sistemas e estudos quegarantem que seus equipamentos não sofram danos com curtos circuitos ou descargas elétricas.  </p>
<h3><strong>Empresa de quadros elétricos Betim que faz customização de armários elétricos</strong></h3>
<p>Seguindo o seu projeto, desenvolvemos quadros elétricos, painéis e armários de acordo com a demanda de cada cliente. Com mão de obra especializada, nossa<strong> empresa de quadros elétricos Betim </strong>oferece custo benefício e total excelência em serviços especializados para toda área da Engenharia Elétrica. Fale conosco.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>