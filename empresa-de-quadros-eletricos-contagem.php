<?php
    $title       = "empresa de Quadros Elétricos Contagem";
    $description = "Nossa empresa de quadros elétricos Contagem trabalha com muitas opções de quadros e painéis, contrate a MS Projetos para serviços de Engenharia Elétrica.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Com a MS você encontra a disponibilidade diversos serviços de<strong> empresa de quadros elétricos Contagem.</strong> Para todo o ramo industrial, empresarial e residencial, oferecemos serviços personalizados que vão desde a criação de quadros e armários de eletricidade até a automação de subestações para indústrias. Nossos produtos e serviços seguem normas internacionais e nacionais de Engenharia. Com fornecedores de alto padrão de equipamentos para nossos sistemas e projetos e sempre seguindo o seu projeto, desenvolvemos quadros elétricos, painéis e armários de acordo com a demanda de cada caso.</p>
<p>Com mão de obra experiente, nossa<strong> empresa de quadros elétricos Contagem </strong>oferece custo benefício e total excelência em serviços especializados para Engenharia Elétrica. Fale conosco para orçamentos e mais informações de serviços para indústrias, empresas, residências, centros comerciais, shoppings, aeroportos, entre outros.</p>
<p>A <strong>empresa de quadros elétricos Contagem </strong>também oferece serviços para adequação de normas para indústrias, automação para subestações, quadros de comando e sinalização, quadros de distribuição de tomadas e iluminação, comando predial, saneamento, entre outros. Desde 1992 atendendo empresas privadas e órgãos públicos, a MS Projetos oferece soluções integradas para sistemas elétricos.</p>
<p>Desenvolvendo quadros conforme a demanda dos clientes, de forma customizada, nossa <strong>empresa de quadros elétricos Contagem</strong> trabalha com muitas opções de quadros e painéis. Entre as opções que vão desde quadros elétricos para comando predial até quadros de bomba de recalque e incêndio, fale nosso atendimento para orçamentos. Consulte-nos para mais informações e contrate a MS para serviços de Engenharia Elétrica.</p>
<h2><strong>O que faz a MS, empresa de quadros elétricos Contagem </strong></h2>
<p>Entre tantos serviços que podem ser encontrados com nossa <strong>empresa de quadros elétricos Betim</strong>, os que mais se destacam são a procura por quadros e armários personalizados, quadros elétricos para comando em prédios, automação, sistemas elétricos para datacenters, centros de tratamento e saneamento, entre outras opções.</p>
<h3><strong>Benefícios ao contratar a MS Painéis, empresa de quadros elétricos Contagem</strong></h3>
<p>Além do melhor custo benefício x qualidade disponibilizado com a MS Painéis,<strong> empresa de quadros elétricos Betim</strong>, auxiliamos em todos os processos burocráticos de regulamentação, documentação e adequação a normas regulamentadoras. Desde estudos, medições até a realização de laudos, a MS Painéis oferece serviços completos para a área de Engenharia Elétrica. </p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>