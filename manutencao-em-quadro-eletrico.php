<?php
    $title       = "Manutenção em Quadro Elétrico";
    $description = "A manutenção em quadro elétrico pode ser realizada periodicamente para preservar seu equipamento, prolongando a vida útil de sua usabilidade. Nossa empresa está há mais.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Se você procura por uma empresa altamente qualificada para realizar a manutenção em quadro elétrico com agilidade e praticidade encontrou o lugar certo. A MS Projetos Industriais é uma empresa que atua dentro da engenharia elétrica e automação industrial fabricando, comercializando, instalando equipamentos e prestando serviços de alta importância para o segmento. Possuímos mais de 10 anos de experiência em prestar manutenção em quadro elétrico com qualidade e muita propriedade técnica. A manutenção em quadro elétrico pode ser realizada de forma preventiva ou corretiva, a forma preventiva realiza manutenções periódicas para analisar e substituir componentes que estão próximo da falha, já a manutenção corretiva é uma intervenção técnica a partir do momento em que o equipamento apresenta defeito. A manutenção em quadro elétrico pode ser realizada periodicamente para preservar seu equipamento, prolongando a vida útil de sua usabilidade. Nossa empresa está há mais de 15 anos no mercado prestando diversos serviços com qualidade para grandes nomes do setor privado e também para o segmento público. A missão da nossa empresa é criar soluções em engenharia elétrica que garantam excelência, apoiadas na competência e no aprimoramento tecnológico buscando sempre agir de forma ética e sustentável.</p>
<h2>A melhor manutenção em quadro elétrico do mercado</h2>
<p>Prestamos a manutenção em quadro elétrico tanto para produtos fabricados por nossa empresa quanto para outras marcas. Caso você precise realizar um orçamento basta preencher os campos disponíveis na aba “contato” com o máximo de informações sobre sua situação para que nossa equipe faça contato já com a ciência da sua necessidade e uma alternativa já proposta através da análise das informações prestadas por você. Atuamos seguindo todas as normas técnicas e ambientais compreendendo que uma de nossas principais funções é atuar de forma sustentável preservando o ambiente para as futuras gerações. </p>
<h3>Solicite aqui sua manutenção em quadro elétrico</h3>
<p>Para saber mais sobre nossa manutenção em quadro elétrico ou quaisquer outros serviços ou produtos disponibilizados por nossa empresa entre em contato e seja atendido por um especialista para te auxiliar da melhor maneira possível. Trabalhamos com a visão de elevar nossa importância nos próximos anos, nos tornando destaque dentre as maiores fornecedoras de serviços de engenharia e montagens industriais no cenário nacional. Não comprometa seu quadro elétrico e realize manutenções periódicas para manter o melhor funcionamento possível e a segurança de seu equipamento. </p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>