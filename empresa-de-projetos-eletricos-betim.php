<?php
    $title       = "empresa de Projetos Elétricos Betim";
    $description = "O melhor custo benefício em serviços para projetos de sistemas elétricos do Brasil. Contrate a melhor empresa de projetos elétricos Betim, MS Projetos.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>A MS Projetos é uma<strong> empresa de projetos elétricos Betim</strong>. Trabalhamos com serviços especializados para projetos de sistemas elétricos industriais. Entre as diversas opções que podem ser encontradas aqui,tenha custo benefício e excelência em serviços realizados por mão de obra especializada, experiente e qualificada.</p>
<p>Com trabalhos completos para segmentos de energia solar, painéis e quadros elétricos, montagens industriais, eficiência energética e muito mais, nossa <strong>empresa de projetos elétricos Betim </strong>oferece preço justo para serviços de alta qualidade. Conheça mais sobre projetos elétricos disponíveis com a MS Projetos e fale conosco para orçamentos e mais informações.</p>
<p>Nossa <strong>empresa de projetos elétricos Betim</strong> oferece serviços especializados para montagem industrial. Conheça nossos serviços: montagem e instalação de cabeamento estruturado, CFTV, subestação de média e alta tensão, adequação CEMIG, aterramento e barramentos blindados, subestações, geradores, nobreak, instalações elétricas, adequações NR10 e NR12, entre outros serviços.</p>
<p>Trabalhando também com painéis e quadros elétricos, obtenha suporte completo para sistemas elétricos industriais. Segurança e eficiência elétrica para equipamentos industriais como motores, compressores, transformadores e muito mais produtos. Consulte nossa <strong>empresa de projetos elétricos Betim</strong> para mais informações e solicite orçamentos.</p>
<h2><strong>Empresa de projetos elétricos Betim especializada em sistemas elétricos</strong></h2>
<p>Atendendo todo o território nacional, fale conosco e solicite seu orçamento por e-mail, telefone ou WhatsApp. O melhor custo benefício em serviços para projetos de sistemas elétricos do Brasil. Contrate a melhor<strong> empresa de projetos elétricos Betim</strong>, Minas Gerais, MS Projetos. Desde 1992 no mercado mineiro e nacional, atendemos empresas privadas e órgãos públicos oferecendo o melhor da engenharia elétrica. Com mão de obra especializada, oferecemos excelência em serviços especializados em montagem industrial, projetos elétricos, quadros e painéis e muito mais.</p>
<h3><strong>MS Projetos Industriais, a melhor empresa de projetos elétricos Betim</strong></h3>
<p>Conheça mais sobre a MS Projetos Industriais. Nossa<strong> empresa de projetos elétricos Betim</strong> oferece serviços para sistemas elétricos e indústrias com soluções inteligentes, sustentáveis e integradas.</p>
<p>Também com serviços na área de eficiência energética e sustentabilidade, prestamos serviços de medições, análise, gestão de projetos, diagnóstico, monitoramento e controle, medição e verificação, e muito mais. Com o melhor custo benefício do mercado, garante segurança e segurança para dispositivos industriais e sistemas elétricos com a MS.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>