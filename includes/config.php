<?php

        // Principais Dados do Cliente
    $nome_empresa = "MS Projetos Industriais";
    $emailContato = "comercial@msprojetosindustriais.com.br";

    // Parâmetros de Unidade
    $unidades = array(
    1 => array(
        "nome" => "MS Projetos Industriais",
        "rua" => "Av.São João, 114",
        "bairro" => "Bandeirantes",
        "cidade" => "Contagem",
        "estado" => "Minas Gerais",
        "uf" => "MG",
        "cep" => "32240-520",
        "latitude_longitude" => "-19.962699781849235, -44.05018377380358", // Consultar no maps.google.com
        "ddd" => "31",
        "telefone" => "99437-4888",
        "telefone2" => "3046-5432",
        "whatsapp" => "99783-1243",
        "whatsapp-link" => "https://api.whatsapp.com/send?phone=5531997831243",
        "link_maps" => "https://www.google.com.br/maps/place/Maestec+Soluções+em+Automação/@-19.9629468,-44.0523885,17z/data=!3m1!4b1!4m5!3m4!1s0xa6bffa191ebd9d:0x494a6739fc67b57d!8m2!3d-19.9629462!4d-44.0502028?hl=pt-BR&authuser=0" // Incorporar link do maps.google.com
    ),
        2 => array(
            "nome" => "",
            "rua" => "",
            "bairro" => "",
            "cidade" => "",
            "estado" => "",
            "uf" => "",
            "cep" => "",
            "ddd" => "",
            "telefone" => ""
        )
    );
    
    // Parâmetros para URL
    $padrao = new classPadrao(array(
        // URL local
        "http://localhost/informacoes.msprojetosindustriais.com/",
        // URL online
        "https://www.informacoes.msprojetosindustriais.com/"
    ));
    
    // Variáveis da head.php
    $url = $padrao->url;
    $canonical = $padrao->canonical;
	
    // Parâmetros para Formulário de Contato
    $smtp_contato            = ""; // Solicitar ao líder do dpto técnico, 177.85.98.119
    $email_remetente         = ""; // Criar no painel de hospedagem, admin@...
    $senha_remetente         = "c0B1S3vH5eCvAO";

    // Contato Genérico (para sites que não se hospedam os e-mails)
    // $smtp_contato            = "111.111.111.111";
    // $email_remetente         = "formulario@temporario-clientes.com.br";
    // $senha_remetente         = "4567FGHJK";

    // Recaptcha Google
    $captcha                 = false; // https://www.google.com/recaptcha/
    $captcha_key_client_side = "";
    $captcha_key_server_side = "";

    // CSS default
    $padrao->css_files_default = array(
        "default/reset",
        "default/grid-system",
        "default/main",
        "default/slicknav-menu",
        "_main-style"
    );
    
    // JS Default
    $padrao->js_files_default = array(
        "default/jquery-1.9.1.min",
        "default/modernizr",
        "default/jquery.slicknav.min",
        "jquery.padrao.main"
    );
        
    // Listas de Palavras Chave
    $palavras_chave = array(
        "Adequação NR10",
        "Adequação NR12",
        "Análise de Energia",
        "Condomínio Solar",
        "Coordenograma de Proteção",
        "Coordenograma de Proteção Cemig",
        "Diagrama Multifilar",
        "Diagrama Trifilar",
        "Diagrama Unifilar",
        "empresa de Energia Fotovoltaica",
        "empresa de Energia Fotovoltaica Contagem",
        "empresa de Energia Fotovoltaica Lagoa Santa",
        "empresa de Energia Fotovoltaica Nova Lima",
        "empresa de Energia Fotovoltaica BH",
        "empresa de Energia Fotovoltaica Belo Horizonte",
        "empresa de Energia Fotovoltaica Betim",
        "empresa de Energia Fotovoltaica Juiz de Fora",
        "empresa de Energia Fotovoltaica Rio de Janeiro",
        "empresa de Energia Solar Belo Horizonte",
        "empresa de Energia Solar Betim",
        "empresa de Energia Solar BH",
        "empresa de Energia Solar Contagem",
        "empresa de Energia Solar Juiz de Fora",
        "empresa de Energia Solar Lagoa Santa",
        "empresa de Energia Solar Nova Lima",
        "empresa de Energia Solar Rio de Janeiro",
        "empresa de Painéis Elétricos Belo Horizonte",
        "empresa de Painéis Elétricos Betim",
        "empresa de Painéis Elétricos Contagem",
        "empresa de Projetos Elétricos",
        "empresa de Projetos Elétricos Belo Horizonte",
        "empresa de Projetos Elétricos Betim",
        "empresa de Projetos Elétricos Contagem",
        "empresa de Quadros Elétricos Belo Horizonte",
        "empresa de Quadros Elétricos Betim",
        "empresa de Quadros Elétricos Contagem",
        "Energia Fotovoltaica",
        "Energia Solar",
        "Energia Solar Fotovoltaica",
        "Energia Solar Residencial",
        "Estudo de Curto Circuito e Seletividade",
        "Estudo de Seletividade",
        "Fabricação de Painéis Elétricos",
        "Fabricação de Quadro Elétricos",
        "Fabricação de Quadros Elétricos",
        "Geração de Energia Solar",
        "Geração de Energia Solar Fotovoltaica",
        "Gerenciamento de Energia",
        "Gestão de Monitoramento de Energia",
        "Inspeção Termografica em Painéis Elétricos",
        "Inspeção Termografica em Quadros Elétricos",
        "Instalação de CCM",
        "Instalação de Painéis Elétricos",
        "Instalação de Quadros Elétricos",
        "Instalações Eletromecânica",
        "Instalações Elétricas Industriais",
        "Instalações Elétricas de Baixa Tensão",
        "Instalações Elétricas de Media Tensão",
        "Laudo NR10",
        "Laudo NR12",
        "Laudo de Aterramento e SPDA",
        "Laudo de Instalações Elétricas",
        "Manutenção em Quadro Elétrico",
        "Manutenção em Subestação",
        "Manutenção em Transformadores",
        "Montagem Eletromecânica",
        "Montagem Elétrica Industrial",
        "Montagem de CCM",
        "Montagem de Centro de Comando de Motores",
        "Montagem de PLC",
        "Montagem de PLC de Segurança",
        "Montagem de Painéis Elétricos",
        "Montagem de QGBT",
        "Montagem de Quadro de Distribuição",
        "Montagem e Instalações de Painéis Elétricos",
        "Painel Elétrico",
        "Painel Fotovoltaico",
        "Painéis Elétricos",
        "Painéis Elétricos BH",
        "Painéis Elétricos Belo Horizonte",
        "Painéis Elétricos Betim",
        "Painéis Elétricos Contagem",
        "Painéis Elétricos MG",
        "Painéis Elétricos Minas Gerais",
        "Parametrização de Inversores",
        "Parametrização de Relé",
        "Parametrização de Relé Pextrom",
        "Parametrização de Relé SEPAM",
        "Parametrização de Soft-starter",
        "Placa de Energia Solar",
        "Projeto CEMIG",
        "Projeto Luminotécnico",
        "Projeto Subestação CEMIG",
        "Projeto de Aterramento e SPDA",
        "Projeto de Automação Industrial",
        "Projeto de Iluminação",
        "Projeto de Painéis Elétricos",
        "Projeto de Quadros Elétricos",
        "Projeto de Subestação",
        "Projeto e Fabricação de Eletrocentro",
        "Quadro Elétrico",
        "Quadro de Segurança",
        "Quadro para Usina Solar",
        "Quadros Elétricos BH",
        "Quadros Elétricos Belo Horizonte",
        "Quadros Elétricos Betim",
        "Quadros Elétricos Contagem",
        "Quadros Elétricos MG",
        "Quadros Elétricos Minas Gerais",
        "Quadros Elétricos",
        "Quadros Elétricos Média Tensão",
        "Quadros Elétricos de Baixa Tensão",
        "Quadros Elétricos de Média Tensão",
        "Quadros Elétricos para Comando Predial",
        "Quadros de Distribuição de Iluminação e Tomadas",
        "Termografia em Painéis Elétricos",
        "Termografia em Quadros Elétricos",
        "Termografia em Subestação",
        "Usina Fotovoltaica",
        "Usina Solar",
        "Usina Solar Compartilhada"

    );
   
    $palavras_chave_com_descricao = array(
        "Item 1" => "Lorem ipsum dolor sit amet.",
        "Item 2" => "Laudem dissentiunt ut per.",
        "Item 3" => "Solum repudiare dissentiunt at qui.",
        "Item 4" => "His at nobis placerat.",
        "Item 5" => "Ei justo lucilius nominati vim."
    );
    
     /**
     * Submenu
     * 
     * $opcoes = array(
     * "id" => "",
     * "class" => "",
     * "limit" => 9999,
     * "random" => false
     * );
     * 
     * $padrao->subMenu($palavras_chave, $opcoes);
     * 
     */

    /**
     * Breadcrumb
     * 
     * -> Propriedades
     * 
     * Altera a url da Home no breadcrumb
     * $padrao->breadcrumb_url_home = "";
     * 
     * Altera o texto que antecede a Home
     * $padrao->breadcrumb_text_before_home = "";
     * 
     * Altera o texto da Home no breadcrumb
     * $padrao->breadcrumb_text_home = "Home";
     * 
     * Altera o divisor de níveis do breadcrumb
     * $padrao->breadcrumb_spacer = " » ";
     * 
     * -> Função
     * 
     * Cria o breadcrumb
     * $padrao->breadcrumb(array("Informações", $h1));
     * 
     */

    /**
     * Lista Thumbs
     * 
     * $opcoes = array(
     * "id" => "",
     * "class_div" => "col-md-3",
     * "class_section" => "",
     * "class_img" => "img-responsive",
     * "title_tag" => "h2",
     * "folder_img" => "imagens/thumbs/",
     * "extension" => "jpg",
     * "limit" => 9999,
     * "type" => 1,
     * "random" => false,
     * "text" => "",
     * "headline_text" => "Veja Mais"
     * );
     * 
     * $padrao->listaThumbs($palavras_chave, $opcoes);
     * 
     */
    
    /**
     * Funções Extras
     * 
     * $padrao->formatStringToURL();
     * Reescreve um texto em uma URL válida
     * 
     */