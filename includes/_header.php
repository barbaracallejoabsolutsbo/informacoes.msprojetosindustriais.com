<header itemscope itemtype="http://schema.org/Organization">
    
    <div class="container header-container-main">
        <div class="col-md-4">
            <div class="logo">
                <a href="https://www.msprojetosindustriais.com" title="<?php echo $h1 . " - " . $nome_empresa; ?>">
                    <span itemprop="image">
                        <img src="imagens/logo.png" alt="<?php echo $nome_empresa; ?>" title="<?php echo $nome_empresa; ?>" class="img-responsive">
                    </span>
                </a>
            </div>
        </div>
        
        <div class="col-md-8">
            <div class="tel">
                <a title="Clique e ligue" href="tel:<?php echo $unidades[1]["ddd"].$unidades[1]["telefone"]; ?>">
                    <i class="fas fa-phone-alt"></i><span itemprop="telephone"> (<?php echo $unidades[1]["ddd"]; ?>) <?php echo $unidades[1]["telefone"]; ?></span>
                </a> 
                |
                <a title="whatsApp" href="<?php echo $unidades[1]["whatsapp-link"]; ?>">
                    <i class="fab fa-whatsapp"></i><span itemprop="telephone"> (<?php echo $unidades[1]["ddd"]; ?>) <?php echo $unidades[1]["whatsapp"]; ?></span>
                </a> 
                |
                <a title="E-mail" href="mailto:<?php echo $emailContato; ?>">
                    <br><i class="fas fa-envelope-open-text"></i><span itemprop="telephone"> <?php echo $emailContato; ?></span>
                </a> 
            </div>
        </div>
    </div>
    <nav class="menu">
                <ul class="menu-list">
                    <li><a href="https://www.msprojetosindustriais.com" title="Página inicial">HOME</a></li>
                    <li><a href="https://www.msprojetosindustriais.com/montagens-eletricas" title="MS MONTAGENS">MS MONTAGENS</a>
                        <ul class="sub-menu">
                            <li><a href="https://www.msprojetosindustriais.com/manutencao-subestacao" title="MANUTENÇÃO EM SUBESTAÇÃO">MANUTENÇÃO EM SUBESTAÇÃO</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="https://www.msprojetosindustriais.com/projetoseletricos" title="MS PROJETOS">MS PROJETOS</a>
                        <ul class="sub-menu">
                            <li><a href="https://www.msprojetosindustriais.com/projeto-conceitual-basico-detalhado" title="PROJETO CONCEITUAL, BÁSICO E EXECUTIVO">PROJETO CONCEITUAL, BÁSICO E EXECUTIVO</a></li>
                            <li><a href="https://www.msprojetosindustriais.com/coordenacao-seletividade" title="COORDENAÇÃO E SELETIVIDADE">COORDENAÇÃO E SELETIVIDADE</a></li>
                            <li><a href="https://www.msprojetosindustriais.com/montante-jusante" title="MONTANTE E JUSTANTE">MONTANTE E JUSTANTE</a></li>
                            <li><a href="https://www.msprojetosindustriais.com/curto-circuito" title="CURTO CIRCUITO">CURTO CIRCUITO</a></li>
                            <li><a href="https://www.msprojetosindustriais.com/projeto-cemig" title="PROJETO CEMIG">PROJETO CEMIG</a></li>
                            <li><a href="https://www.msprojetosindustriais.com/termografia-paineis-eletricos" title="TERMOGRAFIA ELÉTRICA">TERMOGRAFIA ELÉTRICA</a></li>
                            <li><a href="https://www.msprojetosindustriais.com/laudo-aterramento-spda" title="LAUDO DE ATERRAMENTO">LAUDO DE ATERRAMENTO</a></li>
                            <li><a href="https://www.msprojetosindustriais.com/laudo-nr10" title="LAUDO NR-10">LAUDO NR-10</a></li>
                            <li><a href="https://www.msprojetosindustriais.com/laudo-nr12" title="LAUDO NR-12">LAUDO NR-12</a></li>
                            <li><a href="https://www.msprojetosindustriais.com/laudo-credito-icms" title="LAUDO ICMS">LAUDO ICMS</a></li>
                        </ul>
                    </li>
                    <li><a href="https://www.msprojetosindustriais.com/energia-solar" title="MS SOLAR">MS SOLAR</a></li>
                    <li><a href="https://www.msprojetosindustriais.com/ms-eficiencia" title="MS EFICIÊNCIA">MS EFICIÊNCIA</a>
                        <ul class="sub-menu">
                            <li><a href="" title="GESTÃO INTEGRAL DE ENERGIA">GESTÃO INTEGRAL DE ENERGIA</a></li>
                            <li><a href="" title="DIAGNÓSTICO ENERGÉTICO">DIAGNÓSTICO ENERGÉTICO</a></li>
                            <li><a href="" title="ENGENHARIA E MEDIÇÕES">ENGENHARIA E MEDIÇÕES</a></li>
                            <li><a href="" title="MELHORIAS">MELHORIAS</a></li>
                            <li><a href="" title="PROJETO DE EFICIÊNCIA ENERGÉTICA">PROJETO DE EFICIÊNCIA ENERGÉTICA</a></li>
                            <li><a href="" title="MONITORAMENTO DE ENERGIA">MONITORAMENTO DE ENERGIA</a></li>
                        </ul>
                    </li>
                    <li><a href="https://www.msprojetosindustriais.com/ms-paineis" title="MS PAINÉIS">MS PAINÉIS</a></li>
                    <li><a href="https://www.msprojetosindustriais.com/portifolio" title="PORTFÓLIO">PORTFÓLIO</a></li>
                    <li><a href="index.php" title="Informações">INFORMAÇÕES</a></li>
                    <li><a href="https://www.msprojetosindustriais.com" title="CONTATO">CONTATO</a></li>
                </ul>
            </nav>
</header>