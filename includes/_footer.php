<footer>
    <?php include "includes/btn-fixos.php"; ?>
    <div class="container">
        <h2 class="text-center">Localização Matriz</h2>
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3750.083310542856!2d-44.05280424961937!3d-19.96299834442545!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xa6be30fed8a4ff%3A0x2c5361d2a7d23d70!2sAv.%20S%C3%A3o%20Jo%C3%A3o%2C%20114%20-%20Bandeirantes%2C%20Contagem%20-%20MG%2C%2032240-520!5e0!3m2!1spt-BR!2sbr!4v1643381526782!5m2!1spt-BR!2sbr" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
        <br>
        <h5>MS Projetos e Montagens Industriais Ltda</h5>
        <p>Matriz:  Av.São João, 114 - Bandeirantes - Contagem - MG - CEP: 32240-520 - <b>Telefone (31) 99437-4888/(31) 3046-5432</b></p>
        <p>Filial: Avenida das Américas, 19005 | Torre 2 - Sala 914 Absolutto Business Towers - Recreio dos Bandeirantes, Rio de Janeiro - RJ, 22790-703 - <b>Telefone  (21) 98895-7090</b></p>
        <a href="https://www.facebook.com/msprojetosindustriais" title="Facebook"><i class="fab fa-facebook-square"></i></a><a href="https://www.linkedin.com/company/ms-projetos-e-montagens/" title="Linkedin"><i class="fab fa-linkedin"></i></a><a href="https://www.instagram.com/msprojetosindustriais/" title="Instagram"><i class="fab fa-instagram"></i></a>
    </div>
        <div class="footer-copyright">
            <div class="container">
                <hr>
                <div class="row">
                    <div class="col-md-4">
                        <a href="<?php echo $url; ?>mapa-site" title="Mapa do Site">
                            <img src="<?php echo $url; ?>assets/img/icons/sitemap.png" alt="Sitemap"></a>
                        <a rel="nofollow" href="http://validator.w3.org/check?uri=<?php echo $canonical; ?>" target="_blank" title="HTML 5 - Site Desenvolvido nos padrões W3C">
                            <img src="<?php echo $url; ?>assets/img/icons/selo-html5.png" alt="HTML 5 - Site Desenvolvido nos padrões W3C">
                        </a>
                        <a rel="nofollow" href="http://jigsaw.w3.org/css-validator/validator?uri=<?php echo $canonical; ?>" target="_blank" title="CSS 3 - Site Desenvolvido nos padrões W3C">
                            <img src="<?php echo $url; ?>assets/img/icons/selo-css3.png" alt="CSS 3 - Site Desenvolvido nos padrões W3C">
                        </a>
                        <a rel="nofollow" href="http://www.absolutsbo.com.br/" target="_blank" title="CSS 3 - Site Desenvolvido nos padrões W3C">
                            <img src="<?php echo $url; ?>assets/img/icons/logo-footer.png" alt="Absolut SBO">
                        </a>
                    </div>
                    <div class="col-md-4 text-center">
                        <div class="baixa-absolut"></div>
                        <p class="absolutsbo"><a href="https://www.absolutsbo.com.br/" target="_blank" title="Absolut SBO">Desenvolvido por <strong>Absolut SBO</strong></a></p>
                    </div>
                    <div class="col-md-4 text-right">
                        <a href="<?php echo $url; ?>mapa-site" title="Mapa do Site">
                            <img src="<?php echo $url; ?>assets/img/icons/sitemap.png" alt="Sitemap"></a>
                        <a rel="nofollow" href="http://validator.w3.org/check?uri=<?php echo $canonical; ?>" target="_blank" title="HTML 5 - Site Desenvolvido nos padrões W3C">
                            <img src="<?php echo $url; ?>assets/img/icons/selo-html5.png" alt="HTML 5 - Site Desenvolvido nos padrões W3C">
                        </a>
                        <a rel="nofollow" href="http://jigsaw.w3.org/css-validator/validator?uri=<?php echo $canonical; ?>" target="_blank" title="CSS 3 - Site Desenvolvido nos padrões W3C">
                            <img src="<?php echo $url; ?>assets/img/icons/selo-css3.png" alt="CSS 3 - Site Desenvolvido nos padrões W3C">
                        </a>
                        <a rel="nofollow" href="http://www.absolutsbo.com.br/" target="_blank" title="CSS 3 - Site Desenvolvido nos padrões W3C">
                            <img src="<?php echo $url; ?>assets/img/icons/logo-footer.png" alt="Absolut SBO">
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <ul class="menu-footer-mobile">
            <li><a href="tel:<?php echo $unidades[1]["ddd"].$unidades[1]["telefone"]; ?>" class="mm-call" title="Ligue"><i class="fas fa-phone-alt"></i></a></li>
            <li><a href="whatsapp://send?text=<?php echo $canonical; ?>" class="mm-whatsapp" title="Whats App"><i class="fab fa-whatsapp"></i></a></li>
            <li><a href="mailto:<?php echo $emailContato; ?>" class="mm-email" title="E-mail"><i class="fas fa-envelope-open-text"></i></a></li>
            <li><button type="button" class="mm-up-to-top" title="Volte ao Topo"><i class="fas fa-arrow-up"></i></button></li>
        </ul>
    </footer>
    <?php if($_SERVER["SERVER_NAME"] != "clientes" && $_SERVER["SERVER_NAME"] != "localhost"){ ?>
        <!-- Código do Analytics aqui! -->
        <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-1Y68NR1N0Z"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-1Y68NR1N0Z');
</script>
        <?php } ?>