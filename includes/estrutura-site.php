<ul>
    <li><a href="https://www.msprojetosindustriais.com" title="Home">Home</a></li>
    <li><a href="https://www.msprojetosindustriais.com/montagens-eletricas" title="MS MONTAGENS">MS MONTAGENS</a>
        <ul class="sub-menu">
            <li><a href="https://www.msprojetosindustriais.com/manutencao-subestacao" title="MANUTENÇÃO EM SUBESTAÇÃO">MANUTENÇÃO EM SUBESTAÇÃO</a></li>
        </ul>
    </li>
    <li>
        <a href="https://www.msprojetosindustriais.com/projetoseletricos" title="MS PROJETOS">MS PROJETOS</a>
        <ul class="sub-menu">
            <li><a href="https://www.msprojetosindustriais.com/projeto-conceitual-basico-detalhado" title="PROJETO CONCEITUAL, BÁSICO E EXECUTIVO">PROJETO CONCEITUAL, BÁSICO E EXECUTIVO</a></li>
            <li><a href="https://www.msprojetosindustriais.com/coordenacao-seletividade" title="COORDENAÇÃO E SELETIVIDADE">COORDENAÇÃO E SELETIVIDADE</a></li>
            <li><a href="https://www.msprojetosindustriais.com/montante-jusante" title="MONTANTE E JUSTANTE">MONTANTE E JUSTANTE</a></li>
            <li><a href="https://www.msprojetosindustriais.com/curto-circuito" title="CURTO CIRCUITO">CURTO CIRCUITO</a></li>
            <li><a href="https://www.msprojetosindustriais.com/projeto-cemig" title="PROJETO CEMIG">PROJETO CEMIG</a></li>
            <li><a href="https://www.msprojetosindustriais.com/termografia-paineis-eletricos" title="TERMOGRAFIA ELÉTRICA">TERMOGRAFIA ELÉTRICA</a></li>
            <li><a href="https://www.msprojetosindustriais.com/laudo-aterramento-spda" title="LAUDO DE ATERRAMENTO">LAUDO DE ATERRAMENTO</a></li>
            <li><a href="https://www.msprojetosindustriais.com/laudo-nr10" title="LAUDO NR-10">LAUDO NR-10</a></li>
            <li><a href="https://www.msprojetosindustriais.com/laudo-nr12" title="LAUDO NR-12">LAUDO NR-12</a></li>
            <li><a href="https://www.msprojetosindustriais.com/laudo-credito-icms" title="LAUDO ICMS">LAUDO ICMS</a></li>
        </ul>
    </li>
    <li><a href="https://www.msprojetosindustriais.com/energia-solar" title="MS SOLAR">MS SOLAR</a></li>
    <li><a href="https://www.msprojetosindustriais.com/ms-eficiencia" title="MS EFICIÊNCIA">MS EFICIÊNCIA</a>
        <ul class="sub-menu">
            <li><a href="" title="GESTÃO INTEGRAL DE ENERGIA">GESTÃO INTEGRAL DE ENERGIA</a></li>
            <li><a href="" title="DIAGNÓSTICO ENERGÉTICO">DIAGNÓSTICO ENERGÉTICO</a></li>
            <li><a href="" title="ENGENHARIA E MEDIÇÕES">ENGENHARIA E MEDIÇÕES</a></li>
            <li><a href="" title="MELHORIAS">MELHORIAS</a></li>
            <li><a href="" title="PROJETO DE EFICIÊNCIA ENERGÉTICA">PROJETO DE EFICIÊNCIA ENERGÉTICA</a></li>
            <li><a href="" title="MONITORAMENTO DE ENERGIA">MONITORAMENTO DE ENERGIA</a></li>
        </ul>
    </li>
    <li><a href="https://www.msprojetosindustriais.com/ms-paineis" title="MS PAINÉIS">MS PAINÉIS</a></li>
    <li><a href="https://www.msprojetosindustriais.com/portifolio" title="PORTFÓLIO">PORTFÓLIO</a></li>
    <li><a href="https://www.msprojetosindustriais.com" title="Contato">Contato</a></li>
    <li><a href="<?php echo $url; ?>informacoes" title="Informações">Informações</a>
        <ul>
            <?php echo $padrao->subMenu($palavras_chave); ?>
        </ul>
    </li>
</ul>