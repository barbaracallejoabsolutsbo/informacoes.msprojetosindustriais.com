<?php
    $title       = "Coordenograma de Proteção";
    $description = "O coordenograma de proteção é a criação de um gráfico que representa e possui as curvas de seletividade e de proteção de um equipamento.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Conheça a MS Projetos e nossos serviços especializados em Engenharia Elétrica. Promovendo diversos serviços para soluções em sistemas elétricos, a MS Projetos oferece serviços desde a produção de quadros elétricos para shoppings, aeroportos, hotéis, entre outros, até caixas sob medida e armários ou quadros customizados de acordo com a sua necessidade. Conheça mais sobre nossos serviços como o de<strong> coordenograma de proteção.</strong></p>
<p>Mas o que é<strong> coordenograma de proteção?</strong> Este serviço é a criação de um gráfico que representa e possui as curvas de seletividade e de proteção de um equipamento. Este estudo deve ser apresentado às concessionárias de luz, contendo alguns dados como: curvas da atuação das proteções, memória de cálculo do ajuste das proteções, diagrama unifilar completo, coordenograma de atuação da proteção, ajustes indicados, parâmetros para programação, entre outros dados.</p>
<p>Determinando as configurações para os dispositivos de proteção a partir do<strong> coordenograma de proteção, </strong>são realizados estudos completos em transformadores, geradores, sistemas elétricos industriais, linhas de transmissão, usinas, geração de emergência, entre outros.</p>
<p>Ao realizar seu<strong> coordenograma de proteção </strong>é possível usufruir de uma instalação elétrica confiável, com segurança para os operadores e garantindo proteção para os dispositivos. Outros estudos também podem ser feitos como parametrização de relés, estudo de curto circuito e montagem e comissionamento de equipamentos.</p>
<h2><strong>Como é produzido o coordenograma de proteção?</strong></h2>
<p>Feito por um profissional habilitado de Engenharia Elétrica da MS Projetos, o <strong>coordenograma de proteção </strong>é um estudo feito para gerar gráficos para análise de curtos e proteção em cabines de baixa tensão. Fale com nosso atendimento agora mesmo para orçamentos e mais informações sobre serviços para instalações elétricas industriais.</p>
<h3><strong>Contrate nosso serviço para estudo de coordenograma de proteção</strong></h3>
<p>Ao contratar nosso estudo e elaboração de<strong> coordenograma de proteção </strong>MS Projetos, você tem garantia da verificação dos ajustes previstos e diagnóstico de alterações e ajustes a serem feitos. Durante o processo de elaboração de projetos elétricos, estes ajustes e produção de novos estudos e versões do gráfico podem ser solicitados a fim de otimizar os vários níveis de proteção.</p>
<p>Este serviço demanda de grande responsabilidade e é um estudo de grande complexidade, por este motivo, para obtenção de dados confiáveis e precisos, a MS Projetos trabalha com mão de obra especializada e profissionalizada de Engenharia Elétrica, junto a equipamentos de alto nível do ramo.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>