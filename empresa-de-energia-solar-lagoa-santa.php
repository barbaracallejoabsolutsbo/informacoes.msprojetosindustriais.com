<?php
    $title       = "empresa de Energia Solar Lagoa Santa";
    $description = "Desde a implementação do sistema e instalação até a distribuição e compartilhamento de energia solar com nossa empresa de energia solar Juiz de Fora, MS Solar.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>São diversos motivos que podem levar a empresas, indústrias, condomínios e até mesmo residências investir em energia solar. Além de ecológica, a energia solar é sustentável e renovável. Com retorno financeiro do investimento inicial em até 7 anos, os sistemas de captação de energia fotovoltaica podem durar por pelo menos 25 anos, sem perda de eficiência significativa se sempre manter a manutenção. Pensando nisso, nossa <strong>empresa de energia solar Lagoa Santa</strong> procura oferecer todos os serviços envolvidos em todas as etapas para a obtenção de sistemas de painéis solares, sejam integrados à rede elétrica ou não.</p>
<p>Investir em serviços de nossa <strong>empresa de energia solar Juiz de Fora</strong>, MS Solar,é o melhor negócio. Desde 1992 no ramo de sistemas elétricos como MS Eletric, prestamos serviços a diversas empresas privadas e órgãos públicos. Oferecemos também não só serviços para o ramo de energia solar, mas também para toda área de Engenharia Elétrica.</p>
<p>Também serviços disponíveis em nossa <strong>empresa de energia solar Juiz de Fora </strong>são:  projetos para personalização e montagem de quadros elétricos, quadros de comando e quadros de sinalização, seguindo todas normas nacionais e internacionais, além de critérios essenciais da engenharia. Com fornecedores das melhores procedências, oferecemos equipamentos aprovados pelo Inmetro e homologados pela companhia de distribuição.</p>
<p>Faça serviços completos desde a implementação do sistema e instalação até a distribuição e compartilhamento de energia solar com nossa <strong>empresa de energia solar Juiz de Fora</strong>, MS Solar.</p>
<h2><strong>Que serviços encontro com a MS Solar, empresa de energia solar Juiz de Fora</strong></h2>
<p>Encontre serviços especializados em Engenharia Elétrica e painéis solares com nossa<strong> empresa de energia solar Juiz de Fora</strong>. Aqui você pode fazer prospecção de terras para instalação de sistemas completos de geração de energia fotovoltaica, projetos, estudos de viabilidade, dimensionamento de sistema, análise de dados, comercialização de energia, manutenção, entre outros, com todo acompanhamento pós venda. Contrate a MS Solar para serviços com excelência.</p>
<h3><strong>Benefícios garantidos por nossa empresa de energia solar Juiz de Fora</strong></h3>
<p><strong>Com a MS Solar, empresa de energia solar Juiz de Fora</strong>, é possível encontrar uma diversidade de benefícios na obtenção de sistemas fotovoltaicos. Entre os mais comuns, se destacam a economia, valorização do imóvel, sustentabilidade, retorno financeiro garantido, entre outros.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>