<?php
    $title       = "Energia Solar Residencial";
    $description = "A MS Solar, do grupo MS, é uma empresa especializada em energia solar residencial, industrial e comercial. Conheça mais acessando nosso site.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>A<strong> energia solar residencial </strong>pode ser uma ótima escolha para reduzir sua conta de energia elétrica em até 95% do que atualmente. Desde a instalação completa, visitas técnicas, projetos, homologação, entre outros processos para obtenção de um sistema de captação de energia solar, a MS Solar oferece todo suporte profissional de uma empresa especializada em Engenharia Elétrica.</p>
<p>O futuro da energia limpa é a energia solar fotovoltaica. Garantindo diversos benefícios tanto para empresas e indústrias quanto para uso da<strong> energia solar residencial, </strong>é uma fonte de energia ecológica e não poluente.</p>
<p>Nossos serviços especializados em energia fotovoltaica da MS Solar abrangem todo o processo burocrático. Obtenha todo suporte profissional para sistemas, manutenção e instalação completa com homologação das distribuidoras de energia da sua região.</p>
<p>Podendo ser instalado em condomínios, casas, sítios e chácaras para nível domiciliar, a<strong> energia solar residencial </strong>pode ser distribuída de forma compartilhada ou para uso particular. Conheça os tipos de sistemas de energia solar que podem ser utilizados para uso residencial.</p>
<p>Os sistemas para captação de <strong>energia solar residencial</strong> podem constar em 3 modelos de adaptação ao sistema elétrico da sua residência: o off-grid, o on-grid e o sistema híbrido. O sistema off-grid é independente e trabalha com o armazenamento da energia gerada em baterias especiais para este tipo de sistema. O sistema on-grid, com custo benefício excelente, trabalha em conjunto com a distribuidora de energia para gerar créditos que podem ser utilizados no futuro para abater valores em contas de energia elétrica. O sistema híbrido também se comunica com a rede elétrica da sua distribuidora, porém também armazenando energia em baterias para o caso de quedas de energia.</p>
<h2><strong>Conheça mais sobre a maior empresa de energia solar residencial, MS Solar</strong></h2>
<p>A MS Solar, do grupo MS, é uma empresa especializada em<strong> energia solar residencial</strong>, industrial e comercial. Oferecendo todos os serviços para este tipo de segmento, conosco é possível fazer desde a instalação até a manutenção de sistemas fotovoltaicos. Conheça mais acessando nosso site.</p>
<h3><strong>Energia solar residencial é o futuro da energia ecológica</strong></h3>
<p>Debatido em diversos estudos científicos, aenergia solarpode se tornar a energia mais utilizada a partir de 2035, ultrapassando o uso de energia eólica, a carvão e hidrelétricas, por exemplo. Por possuir baixo custo de produção, investimento justo e alto retorno benéfico, a <strong>energia solar residencial</strong> pode ser uma ótima escolha para economizar dinheiro e colaborar com o meio ambiente.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>