<?php
    $title       = "empresa de Energia Fotovoltaica Juiz de Fora";
    $description = "A MS Solar é uma empresa de energia fotovoltaica Juiz de Fora que oferece diversos serviços para os vários segmentos da Engenharia Elétrica.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Se você está procurando uma<strong> empresa de energia fotovoltaica Juiz de Fora, </strong>conheça os serviços especializados para toda área de energia solar realizados pela MS Solar. Do grupo MS Projetos, profissionais em Engenheira Elétrica, trabalhamos com projetos que vão desde projetos e montagens industriais até a implementação de sistemas de captação de energia fotovoltaica.</p>
<p>O principal objetivo de nossa <strong>empresa de energia fotovoltaica Juiz de Fora </strong>é proporcionar soluções inteligentes e eficientes para geração, projetar, consumir e compartilhar energia de fonte renovável do sol. A energia solar é uma tecnologia moderna e ecológica que explora a fonte de energia solar. Esta fonte de energia não emite poluentes, é renovável e altamente econômica, com reduções de até 95% do valor das contas de energia elétrica.</p>
<p>A MS Solar,<strong> empresa de energia fotovoltaica Juiz de Fora</strong>, é um dos segmentos do grupo MS. Especializada em serviços para o ramo de geração de energia fotovoltaica, trabalhamos desde a prospecção de imóveis e terras rurais para implementação de sistemas de captação energia, até todo o processo de regulamentação e instalação dos equipamentos que compõem todo o sistema. Contrate agora mesmo os serviços da MS Solar.</p>
<p>Com nossa <strong>empresa de energia fotovoltaica Juiz de Fora, </strong>você pode usufruir de fonte de energia limpa e renovável, com possibilidade de consumir muito mais energia gastando muito menos dinheiro na conta de energia elétrica. Energia fotovoltaica é uma solução inteligente para geração de energia sustentável, econômica e um investimento que garante retorno do dinheiro investido em até 7 anos no máximo.</p>
<p></p>
<h2><strong>Conheça mais a MS Solar, empresa de energia fotovoltaica Juiz de Fora</strong></h2>
<p>A MS Solar é uma <strong>empresa de energia fotovoltaica Juiz de Fora </strong>que oferece diversos serviços para os vários segmentos da Engenharia Elétrica. Com soluções modernas, tecnologias ecológicas e sustentáveis, a MS Solar oferece todo suporte para você que procura instalação, manutenção e regulamentação de sistemas de painéis solares para todo Brasil. Para empresas, indústrias, condomínios e residências, solicite nossos serviços e tenha suporte profissional.</p>
<h3><strong>Serviços especializados de empresa de energia fotovoltaica Juiz de Fora</strong></h3>
<p>Nossa <strong>empresa de energia fotovoltaica Juiz de Fora </strong>oferece serviçoscom qualidade para você que usa ou procura usar energia sustentável de fonte solar. Conheça nossos serviços completos para todo segmento de energia solar.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>