<?php
    $title       = "empresa de Energia Fotovoltaica Nova Lima";
    $description = "A MS Projetos Industriais é uma empresa de energia fotovoltaica Nova Lima, com sede em Contagem, Minas Gerais, oferecemos serviços de Engenharia Elétrica.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>A energia fotovoltaica é proveniente dos raios solares, e pode ser captada por painéis solares para serem convertidos em um sistema com inversor que a transforma em energia elétrica para utilização em indústrias, casas e empresas. A MS Projetos é uma<strong> empresa de energia fotovoltaica Nova Lima</strong> que oferece serviços completos para implementação de sistemas de captação de energia fotovoltaica.</p>
<p>Com nossa<strong> empresa de energia fotovoltaica Nova Lima </strong>você encontra serviços para regulamentação e implementação de painéis fotovoltaicos com sistemas híbridos, off-grid ou on-grid. Conheça mais sobre os tipos de sistemas de captação de energia fotovoltaica, seus funcionamentos e tudo que é necessário para regularizar com sua concessionária de energia elétrica.</p>
<p>Os sistemas encontrados com nossa<strong> empresa de energia fotovoltaica Nova Lima </strong>podem ser para uso comercial, residencial ou industrial. Produzida a partir de uma fonte de energia renovável e ecológica, a energia solar proporciona economia de, em alguns casos, até 95% em contas de energia elétrica. Dependendo do seu investimento, entre 3 e 7 anos você obtém todo o retorno do capital investido. Os sistemas possuem durabilidade de 25 anos, com alguns relatos de funcionamento por mais de 30 anos.</p>
<p>A MS Projetos,<strong> empresa de energia fotovoltaica Nova Lima</strong>, oferece também outras soluções para a área de Engenharia Elétrica. Com serviços para sistemas elétricos a nível industrial, comercial ou residencial, oferecemos suporte para projetos de quadros de energia, quadros de comando, quadros de iluminação, painéis de controle, caixas sob medida, armários autoportantes, entre outros serviços completos.</p>
<h2><strong>Conheça mais sobre nossa empresa de energia fotovoltaica Nova Lima</strong></h2>
<p>A MS Projetos Industriais é uma<strong> empresa de energia fotovoltaica Nova Lima</strong> em Contagem, Minas Gerais, que oferece serviços de Engenharia Elétrica para o país todo. Especializados e experientes, ano após ano vem conquistando o cenário mineiro e nacional por meio de diversas oportunidades de negócio desenvolvidas com nossos projetos.</p>
<h3><strong>Outros serviços disponíveis em nossa empresa de energia fotovoltaica Nova Lima</strong></h3>
<p>Com a<strong> empresa de energia fotovoltaica Nova Lima</strong>, MS Projetos, é possível encontrar serviços para todo ramo de Engenharia Elétrica. Desde diagramas, projetos, implementação de sistemas de captação de energia solar, todo acompanhamento pós venda, manutenção e instalação de sistemas elétricos, até painéis de comandos, quadros de energia, entre outros. Consulte-nos e faça já o seu orçamento.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>